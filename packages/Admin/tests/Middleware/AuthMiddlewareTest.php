<?php

namespace WeCare\Admin\Test\Middleware;

use WeCare\User\Model\User;
use WeCare\User\Repository\UserRepository;
use Zend\Session\SessionManager;

class AuthMiddlewareTest extends \PHPUnit\Framework\TestCase
{
    public function testInvokingAuthAsGuestShouldReturnRedirect()
    {
        $_SESSION = [];
        $storage = new \Zend\Session\Storage\ArrayStorage();
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['offsetGet', 'getStorage'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(1))
            ->method('getStorage')
            ->will(static::returnValue($storage));
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $configMock = $this->getMockBuilder(\Zend\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMockForAbstractClass();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMockForAbstractClass();
        $request = new \GuzzleHttp\Psr7\ServerRequest(
//            'POST', '/login/login', [], '{"username":"test@example.com","password":"123123"}'
//            '', '', [], '{"username":"test@example.com","password":"123123"}'
            '', '', [], ''
        );
        $aclData = require __DIR__ . '/../../../../config/acl.php';
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new \WeCare\Admin\Middleware\AuthMiddleware($sessionMock, $configMock, $aclData, $flashMock, $userRepoMock);
        $response = $controller($request, $response);
        $this->assertSame(302, $response->getStatusCode());
        $this->assertEquals('/user/loginForm/', $response->getHeader('Location')[0]);
    }

    public function testInvokingAuthAsGuestOnAllowedPathShouldReturnNextCallableInChain()
    {
        $_SESSION = [];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $configMock = $this->getMockBuilder(\Zend\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMockForAbstractClass();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMockForAbstractClass();
        $request = new \GuzzleHttp\Psr7\ServerRequest(
            '', '/user/loginForm/', [], ''
        );
        $aclData = require __DIR__ . '/../../../../config/acl.php';
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new \WeCare\Admin\Middleware\AuthMiddleware($sessionMock, $configMock, $aclData, $flashMock, $userRepoMock);
        $response = $controller($request, $response, function () {return 'test';});
        $this->assertSame('test', $response);
    }

    public function testInvokingAuthAsUserOnNotAllowedPathShouldReturnRedirectAndSetMessage()
    {
        $_SESSION = [
            'flash_messages' => [
                'error' => []
            ],
        ];
        $storage = new \Zend\Session\Storage\ArrayStorage();
        $storage->offsetSet('loggedIn', 2);
        $storage->offsetSet('loggedInRole', 2);
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['offsetGet', 'getStorage'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(3))
            ->method('getStorage')
            ->will(static::returnValue($storage));
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['getById'])
            ->getMockForAbstractClass();
        $userRepoMock->expects(static::once())
            ->method('getById')
            ->willReturn(new UserStub());
        $configMock = $this->getMockBuilder(\Zend\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMockForAbstractClass();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMockForAbstractClass();
        $request = new \GuzzleHttp\Psr7\ServerRequest(
            '', '/user/view/', [], ''
        );
        $aclData = require __DIR__ . '/../../../../config/acl.php';
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new \WeCare\Admin\Middleware\AuthMiddleware($sessionMock, $configMock, $aclData, $flashMock, $userRepoMock);
        $response = $controller($request, $response);
        $this->assertSame(302, $response->getStatusCode());
        $this->assertEquals('/report/view/2/', $response->getHeader('Location')[0]);
        $this->assertSame('You do not have permissions to access /user/view/.', $_SESSION['flash_messages']['error'][0]);
    }

    public function testInvokingAuthAsUserOnAllowedPathShouldReturnRedirectNextMiddlewareInChain()
    {
        $_SESSION = [
            'flash_messages' => [
                'error' => []
            ],
        ];
        $storage = new \Zend\Session\Storage\ArrayStorage();
        $storage->offsetSet('loggedIn', 2);
        $storage->offsetSet('loggedInRole', 2);
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['offsetGet', 'getStorage'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(2))
            ->method('getStorage')
            ->will(static::returnValue($storage));
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['getById'])
            ->getMockForAbstractClass();
        $userRepoMock->expects(static::once())
            ->method('getById')
            ->willReturn(new UserStub());
        $configMock = $this->getMockBuilder(\Zend\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMockForAbstractClass();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMockForAbstractClass();
        $request = new \GuzzleHttp\Psr7\ServerRequest(
            '', '/report/view/2/', [], ''
        );
        $aclData = require __DIR__ . '/../../../../config/acl.php';
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new \WeCare\Admin\Middleware\AuthMiddleware($sessionMock, $configMock, $aclData, $flashMock, $userRepoMock);
        $response = $controller($request, $response, function () {return 'test';});
        $this->assertSame('test', $response);
    }
}

class UserStub extends User
{
    public function __construct()
    {
        parent::__construct('2', '', '', '', '', '2', 1, '', '');
    }

    public function getEmail(): string
    {
        return 'testEmail';
    }

    public function getPassword(): string
    {
        return password_hash('testPassword', PASSWORD_BCRYPT);
    }
}