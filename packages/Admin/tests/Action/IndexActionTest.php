<?php
namespace WeCare\Admin\Test\Action;

use Monolog\Logger;
use WeCare\Admin\Action\FetchCityStatusAction;

class IndexActionTest extends \PHPUnit\Framework\TestCase
{
    function testInvokingIndexActionShouldReturnRedirect()
    {
        $configMock = $this->getMockBuilder(\Zend\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMockForAbstractClass();
        $loggerMock = $this->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $indexAction = new FetchCityStatusAction($loggerMock, $configMock);
        $request = new \GuzzleHttp\Psr7\ServerRequest(
            '', '', [], ''
        );
        $response = $indexAction($request, new \GuzzleHttp\Psr7\Response());
        $this->assertSame(302, $response->getStatusCode());
        $this->assertEquals('/user/loginForm/', $response->getHeader('Location')[0]);
    }
}