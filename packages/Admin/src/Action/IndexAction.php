<?php
declare(strict_types=1);

namespace WeCare\Admin\Action;

use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;

class IndexAction
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Config
     */
    private $config;

    /**
     * ParseReportAction constructor.
     * @param Logger $logger
     * @param Config $config
     */
    public function __construct(
        Logger $logger, Config $config
    ) {
        $this->logger = $logger;
        $this->config = $config;
    }

    /**
     * Parses data for provided merchantId
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request request
     * @param \Psr\Http\Message\ResponseInterface $response response
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function __invoke(
        \Psr\Http\Message\ServerRequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response
    ) {
        $url = $this->config->offsetGet('baseUrl') . '/admin/user/loginForm/';

        return $response->withStatus(302)->withHeader('Location', $url);
    }
}