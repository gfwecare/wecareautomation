<?php

namespace WeCare\Admin\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use WeCare\User\Repository\UserRepository;
use Laminas\Session\SessionManager;
use Tamtamchik\SimpleFlash\Flash;
use Laminas\Config\Config;
use Skeletor\Acl\Acl;

class AuthMiddleware
{
    /**
     * @var SessionManager
     */
    private $sessionManager;

    /**
     * @var Acl
     */
    private $acl;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Flash
     */
    private $flash;

    const GUEST_LEVEL = 0;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * AuthMiddleware constructor.
     *
     * @param SessionManager $sessionManager session manager
     * @param UserRepository $userRepository
     * @param Config $config
     * @param $aclData
     * @param Flash $flash
     */
    public function __construct(SessionManager $sessionManager, Config $config, Flash $flash, UserRepository $userRepository, Acl $acl)
    {
        $this->sessionManager = $sessionManager;
        $this->config = $config;
        $this->acl = $acl;
        $this->flash = $flash;
        $this->userRepository = $userRepository;
    }

    /**
     * Checks if user can access resource.
     *
     * @param ServerRequestInterface $request request
     * @param ResponseInterface $response response
     * @param null|callable $next next
     *
     * @return ResponseInterface
     * @throws \Exception
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        $next = null
    )
    {
        $path = $request->getUri()->getPath();
        //skip pages that do not require auth
        if ($this->acl->isGuestPath($path)) {
            return $next($request, $response);
        }

        if (!$this->sessionManager->getStorage()->offsetGet('loggedIn')) {
            $url = $this->config->offsetGet('baseUrl') . '/admin/user/loginForm/';
            $this->flash->error('You must be logged in to access private area.');

            return $response->withStatus(302)->withHeader('Location', $url);
        }
        $user = $this->userRepository->getById($this->sessionManager->getStorage()->offsetGet('loggedIn'));
        if (!$this->acl->canAccess($user, $request->getUri()->getPath())) {
            $url = $this->config->offsetGet('baseUrl') . '/admin/user/form/' . $this->sessionManager->getStorage()->offsetGet('loggedIn') . '/';
            $this->flash->error(sprintf('You do not have permissions to access %s.', $path));

            return $response->withStatus(302)->withHeader('Location', $url);
        }

        return $next($request, $response);
    }
}
