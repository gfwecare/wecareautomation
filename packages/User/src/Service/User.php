<?php
declare(strict_types=1);
namespace WeCare\User\Service;

use \WeCare\User\Repository\UserRepository;
use Laminas\Session\SessionManager as Session;

class User
{
    const LOGIN_ERROR_INVALID = 'Invalid credentials provided.';

    const LOGIN_ERROR_NO_EMAIL = 'Email not found in system.';

    const LOGIN_SUCCESS = 'You have successfully logged in.';

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var Session
     */
    private $session;

    private $messages;

    public function __construct(UserRepository $userRepository, Session $session)
    {
        $this->userRepository = $userRepository;
        $this->session = $session;
    }

    public function login($data)
    {
        try {
            $user = $this->userRepository->getByEmail($data['email']);
            if (!password_verify($data['password'], $user->getPassword())) {
                $this->messages[] = self::LOGIN_ERROR_INVALID;
                return false;
            }
        } catch (\Exception $e) {
            $this->messages[] = self::LOGIN_ERROR_NO_EMAIL;
            return false;
        }
        $this->messages[] = self::LOGIN_SUCCESS;
        $this->session->getStorage()->offsetSet('loggedIn', $user->getId());
        $this->session->getStorage()->offsetSet('loggedInRole', $user->getRole());
        $this->session->getStorage()->offsetSet('loggedInEmail', $user->getEmail());
        $this->session->getStorage()->offsetSet('redirectPath', $user->getRedirectPath());

        return true;
    }

    public function getRepository()
    {
        return $this->userRepository;
    }

    public function getMessages()
    {
        return $this->messages;
    }
}