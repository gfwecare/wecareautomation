<?php

namespace WeCare\User\Validator;

use Tamtamchik\SimpleFlash\Flash;
use WeCare\User\Repository\UserRepository;

/**
 * Class User.
 * User validator.
 *
 * @package WeCare\Merchant\Validator
 */
class User
{
    /**
     * @var Flash
     */
    private $flash;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * User constructor.
     *
     * @param Flash $flash
     * @param UserRepository $userRepo
     */
    public function __construct(Flash $flash, UserRepository $userRepo)
    {
        $this->flash = $flash;
        $this->userRepo = $userRepo;
    }

    /**
     * Validates provided data, and sets errors with Flash in session.
     *
     * @param $data
     *
     * @return bool
     */
    public function isValid($data): bool
    {
        $emailValidator = new \Laminas\Validator\EmailAddress();
        $valid = true;

        if (!$emailValidator->isValid($data['email'])) {
            $this->flash->error('Email you entered is not valid.');
            $valid = false;
        }

        if ($this->userRepo->emailExists($data['email'])) {
            $this->flash->error('Email you entered already exists in system.');
            $valid = false;
        }

        if ($data['password'] !== $data['password2']) {
            $this->flash->error('Passwords you entered do not match.');
            $valid = false;
        }

        if (strlen($data['password']) < 6) {
            $this->flash->error('Password must be at least 6 characters long.');
            $valid = false;
        }
        if (strlen($data['displayName']) < 3) {
            $this->flash->error('First name must be at least 3 characters long.');
            $valid = false;
        }

        return $valid;
    }
}
