<?php
declare(strict_types=1);

namespace WeCare\User\Repository;

use Skeletor\Mapper\NotFoundException;
use WeCare\User\Mapper\User as Mapper;
use WeCare\User\Model\Client;
use WeCare\User\Model\User;
use Laminas\Config\Config;
use WeCare\User\Model\Admin;

/**
 * Class UserRepository.
 *
 */
class UserRepository
{
    /**
     * @var Mapper
     */
    private $userMapper;

    /**
     * @var \DateTime
     */
    private $dt;

    /**
     * @var Config
     */
    private $config;

    /**
     * userRepository constructor.
     *
     * @param Mapper $userMapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(Mapper $userMapper, \DateTime $dt, Config $config)
    {
        $this->userMapper = $userMapper;
        $this->dt = $dt;
        $this->config = $config;
    }

    /**
     * Fetches a list of UserEntity models.
     *
     * @param array $params
     *
     * @return array
     */
    public function fetchAll($params = array()): array
    {
        $users = [];
        foreach ($this->userMapper->fetchAll($params) as $userData) {
            $users[] = $this->make($userData);
        }

        return $users;
    }


    /**
     * @param $userId
     * @return User
     * @throws NotFoundException
     */
    public function getById($userId): User
    {
        return $this->make($this->userMapper->fetchById($userId));
    }

    /**
     * @param $email
     *
     * @return User
     * @throws \Exception
     */
    public function getByEmail($email): User
    {
        if ($email === '') {
            throw new \InvalidArgumentException('Wrong param provided. ');
        }

        $data = $this->userMapper->fetchAll(['email' => $email], 1);
        if (empty($data)) {
            throw new \Exception('User entity not found. ' . $email);
        }

        return $this->make($data[0]);
    }

    /**
     * Creates user model.
     *
     * @param array $data
     *
     * @return int
     * @throws \Exception
     */
    public function create($data): int
    {
        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        $user = $this->make($data);

        return $this->userMapper->insert($user);
    }

    /**
     * Updates user model.
     *
     * @param $data
     * @return User
     * @throws \Exception
     *
     */
    public function update($data): User
    {
        if ($data['password'] !== '') {
            $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        } else {
            $oldUser = $this->make($this->userMapper->fetchById($data['userId']));
            $data['password'] = $oldUser->getPassword();
        }
        $user = $this->make($data);
        $this->userMapper->update($user);

        return $this->getById($user->getId());
    }

    /**
     * Factory method
     *
     * @param $userData
     * @return User
     */
    private function make($userData): User
    {
        $data = [];
        $banks = null;
        foreach ($userData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }

        switch ($data['role'])
        {
            case User::ROLE_ADMIN:
                $user = Admin::class;
                break;

            case User::ROLE_CLIENT:
                $user = Client::class;
                break;

            default:
                throw new \InvalidArgumentException('Invalid role provided');
                break;
        }

        return new $user(
            $data['userId'],
            $data['password'],
            $data['email'],
            $data['createdAt'],
            $data['updatedAt'],
            (int) $data['isActive'],
            $data['displayName']
        );
    }

    /**
     * Deletes a single user entity.
     *
     * @param User $user
     *
     * @return bool
     */
    public function deleteUser(User $user): bool
    {
        return $this->userMapper->delete($user);
    }

    public function emailExists($email): bool
    {
        try {
            $this->getByEmail($email);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }
}
