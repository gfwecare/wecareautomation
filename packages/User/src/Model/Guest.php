<?php
declare(strict_types=1);

namespace WeCare\User\Model;

/**
 * Class Client.
 * Represents client user dto.
 *
 * @package WeCare\User\Model
 */
class Guest extends User
{
    public function __construct()
    {
        parent::__construct('', '', '', '', '', self::ROLE_GUEST, '', '');
    }
}
