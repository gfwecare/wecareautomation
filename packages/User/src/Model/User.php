<?php
declare(strict_types = 1);
namespace WeCare\User\Model;

use Skeletor\Model\Model;
use Skeletor\Acl\AclInterface;


/**
 * Class User.
 * Base user model.
 *
 * @package SNF\User\Model
 */
class User extends Model implements AclInterface
{
    const ROLE_GUEST = 0;

    const ROLE_ADMIN = 1;

    const ROLE_CLIENT = 2;

    const STATUS_INACTIVE = 0;

    const STATUS_ACTIVE = 1;

    /**
     * @var
     */
    private $userId;

    /**
     * @var
     */
    private $password;

    /**
     * @var
     */
    private $email;

    /**
     * @var
     */
    private $displayName;

    /**
     * @var
     */
    private $role;

    /**
     * @var int
     */
    private $isActive;

    /**
     * Redirect path after login/
     * @TODO move to config or other storage
     *
     * @var string
     */
    private $redirectPath = '/admin/user/form/';

    /**
     * User constructor.
     * @param $userId
     * @param $password
     * @param $email
     * @param $createdAt
     * @param $updatedAt
     * @param $role
     * @param int $isActive
     * @param $displayName
     * @param Bank[] $banks
     */
    public function __construct($userId, $password, $email, $createdAt, $updatedAt, $role, $isActive, $displayName)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->userId = $userId;
        $this->password = $password;
        $this->email = $email;
        $this->role = $role;
        $this->isActive = $isActive;
        $this->displayName = $displayName;
    }

    public function getRedirectPath()
    {
        return $this->redirectPath . $this->userId .'/';
    }

    /**
     * @return array
     */
    public static function hrLevels(): array
    {
        return array(
            self::ROLE_ADMIN => 'Admin',
            self::ROLE_CLIENT => 'Client',
        );
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return int
     */
    public function getRole(): int
    {
        return (int) $this->role;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return (bool) $this->isActive;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int) $this->userId;
    }
}
