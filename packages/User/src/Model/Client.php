<?php
declare(strict_types=1);

namespace WeCare\User\Model;

/**
 * Class Client.
 * Represents client user dto.
 *
 * @package WeCare\User\Model
 */
class Client extends User
{
    public function __construct($userId, $password, $email, $createdAt, $updatedAt, $isActive, $displayName)
    {
        $role = self::ROLE_CLIENT;
        parent::__construct($userId, $password, $email, $createdAt, $updatedAt, $role, $isActive, $displayName);
    }
}
