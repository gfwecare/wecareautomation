<?php
declare(strict_types=1);

namespace WeCare\User\Model;

/**
 * Class Admin.
 * Represents admin user dto.
 *
 * @package WeCare\User\Model
 */
class Admin extends User
{
    private $redirectTo = '/user/view/';

    public function __construct($userId, $password, $email, $createdAt, $updatedAt, $isActive, $displayName)
    {
        $role = self::ROLE_ADMIN;
        parent::__construct($userId, $password, $email, $createdAt, $updatedAt, $role, $isActive, $displayName);
    }
}