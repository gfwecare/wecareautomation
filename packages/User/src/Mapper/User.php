<?php
declare(strict_types=1);
namespace WeCare\User\Mapper;

use Skeletor\Mapper\NotFoundException;
use WeCare\User\Model\User as Model;

class User
{
    /**
     * @var \PDO
     */
    private $driver;

    public function __construct(\PDO $pdo)
    {
        $this->driver = $pdo;
    }

    /**
     * @param Model $model
     *
     * @throws \Exception if not able to execute query
     *
     * @return int
     */
    public function insert(Model $model): int
    {
        $this->driver->beginTransaction();

        $sql = <<<HEREDOC
INSERT INTO `user` (`userId`, `email`, `password`, `role`, `isActive`, `displayName`)
VALUES (null, :email, :password, :role, :isActive, :displayName);
HEREDOC;

        $statement = $this->driver->prepare($sql);
        $statement->bindValue(':email', $model->getEmail());
        $statement->bindValue(':password', $model->getPassword());
        $statement->bindValue(':role', $model->getRole());
        $statement->bindValue(':isActive', $model->getIsActive());
        $statement->bindValue(':displayName', $model->getDisplayName());
        if (!$statement->execute()) {
            $this->driver->rollBack();
            throw new \Exception('Sql: ' . $sql . ' generated error:' . print_r($statement->errorInfo(), true));
        }
        $id = $this->driver->lastInsertId();
        $this->driver->commit();
        return (int)$id;
    }

    /**
     * @param Model $model
     * @return int
     * @throws \Exception
     */
    public function update(Model $model): int
    {
        $this->driver->beginTransaction();

        $sql = <<<HEREDOC
UPDATE `user` SET `email` = :email, `password` = :password, `role` = :role, `isActive` = :isActive, `displayName` = :displayName 
WHERE `userId` = :userId;
HEREDOC;

        $statement = $this->driver->prepare($sql);
        $statement->bindValue(':userId', $model->getId(), \PDO::PARAM_INT);
        $statement->bindValue(':email', $model->getEmail());
        $statement->bindValue(':password', $model->getPassword());
        $statement->bindValue(':role', $model->getRole());
        $statement->bindValue(':isActive', $model->getIsActive());
        $statement->bindValue(':displayName', $model->getDisplayName());
        if (!$statement->execute()) {
            throw new \Exception('Sql: ' . $sql . ' generated error:' . print_r($statement->errorInfo(), true));
        }
        $this->driver->commit();

        return $model->getId();
    }

    /**
     * @param array $params
     * @param null $limit
     * @return array
     */
    public function fetchAll($params = array(), $limit = null): array
    {
        $sql = "SELECT * FROM `user` ";
        $i = 0;
        foreach ($params as $name => $value) {
            if ($i === 0) {
                $sql .= " WHERE `{$name}` = '{$value}' ";
            } else {
                $sql .= " AND `{$name}` = '{$value}' ";
            }
            $i++;
        }
        if (isset($limit)) {
            $sql .= " LIMIT " . (int)$limit;
        }
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $userId
     * @return array
     * @throws NotFoundException
     */
    public function fetchById($userId): array
    {
        $sql = "SELECT * FROM `user` WHERE `userId` = :userId";
        $stmt = $this->driver->prepare($sql);
        $stmt->execute([':userId' => (int) $userId]);
        if ($stmt->rowCount() === 0) {
            throw new NotFoundException('User entity not found. ' . $userId);
        }

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * @param Model $user
     * @return bool
     */
    public function delete(Model $user): bool
    {
        $this->driver->beginTransaction();
        $sql = "DELETE FROM `user` WHERE `userId` = {$user->getUserId()}";
        $this->driver->prepare($sql)->execute();

        return $this->driver->commit();
    }

}
