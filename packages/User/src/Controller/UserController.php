<?php
declare(strict_types=1);

namespace WeCare\User\Controller;

use GuzzleHttp\Psr7\Response;
use WeCare\User\Service\User as UserService;
use WeCare\User\Validator\User;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use WeCare\User\Repository\UserRepository as UserRepo;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Controller\Controller;


class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var User
     */
    private $validator;

    /**
     * LoginController constructor.
     *
     * @param UserRepo $userRepo
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param \Twig\Environment $twig
     * @param User $validator
     */
    public function __construct(UserService $userService, Session $session, Config $config, Flash $flash, \Twig\Environment $twig, User $validator)
    {
        parent::__construct($twig, $config, $session, $flash);

        $this->userService = $userService;
        $this->validator = $validator;
    }

    /**
     * @return Response
     */
    public function loginForm(): Response
    {
        if ($this->getSession()->getStorage()->offsetGet('loggedIn')) {
            return $this->redirect('/admin/user/form/'.$this->getSession()->getStorage()->offsetGet('loggedIn').'/');
        }
        $this->setGlobalVariable('pageTitle', 'Login');

        return $this->respond('loginForm');
    }

    /**
     * User list & view
     *
     * @return Response
     */
    public function view(): Response
    {
        $users = $this->userService->getRepository()->fetchAll();
        $this->setGlobalVariable('pageTitle', 'View Users');
        return $this->respond('view', ['users' => $users]);
    }

    /**
     * Login action.
     *
     * @return Response
     */
    public function login(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        if (!$this->userService->login($data)) {
            $this->getFlash()->error($this->userService->getMessages()[0]);

            return $this->redirect('/admin/user/loginForm/');
        }
        $this->getFlash()->success($this->userService->getMessages()[0]);

        return $this->redirect($this->getSession()->getStorage()->offsetGet('redirectPath'));
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function logOut(): Response
    {
        $this->getSession()->getStorage()->offsetUnset('loggedIn');
        $this->getSession()->getStorage()->offsetUnset('loggedInRole');
        $this->getSession()->getStorage()->offsetUnset('loggedInEmail');
        $this->getSession()->forgetMe();
        $this->getFlash()->success('You have successfully logged out.');

        return $this->redirect('/admin/user/loginForm/');
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('userId');
        if ($this->getSession()->getStorage()->offsetGet('loggedInRole') !== \WeCare\User\Model\User::ROLE_ADMIN) {
            if ($id !== $this->getSession()->getStorage()->offsetGet('loggedIn')) {
                $this->getFlash()->error('Can only edit owned profile.');

                return $this->redirect(sprintf('/admin/user/form/%s/', $this->getSession()->getStorage()->offsetGet('loggedIn')));
            }
        }

        $user = false;
        $this->setGlobalVariable('pageTitle', 'Create user');
        if ($id) {
            $user = $this->userService->getRepository()->getById($id);
            $this->setGlobalVariable('pageTitle', 'Edit user: ' . $user->getDisplayName());
        }
        return $this->respond('form', ['user' => $user]);
    }

    /**
     * @return Response
     */
    public function update(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        //@TODO validate data
        try {
            $this->userService->getRepository()->update($data);
            $this->getFlash()->success('User successfully updated');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        return $this->redirect('/admin/user/view/'.$data['userId'].'/');
    }

    /**
     * @return Response
     */
    public function create(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        if ($this->validator->isValid($data)) {
            try {
                $this->userService->getRepository()->create($data);
                $this->getFlash()->success('User successfully created');
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                die;
            }
        }
        return $this->redirect('/admin/user/view/');
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function delete(): Response
    {
        $user = false;
        $id = $this->getRequest()->getAttribute('userId');
        if (!empty($id)) {
            $user = $this->userService->getRepository()->getById($id);
        }
        try {
            $this->userService->getRepository()->deleteUser($user);
            $this->getFlash()->success('User successfully deleted');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        return $this->redirect('/admin/user/view/');
    }
}