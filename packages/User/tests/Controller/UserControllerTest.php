<?php

namespace Test\WeCare\User\Controller;


use WeCare\Bank\Repository\BankRepository;
use WeCare\User\Controller\UserController;
use WeCare\User\Model\User;
use WeCare\User\Repository\UserRepository;
use WeCare\User\Validator\User as Validator;
use Laminas\Session\SessionManager;

class UserControllerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Action attribute is not set.
     */
    public function testLoginFormWithoutProperUriShouldThrowException()
    {
        $_SESSION = [];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->getMock();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMock();
        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
        $validatorMock = $this->getMockBuilder(Validator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $request = new \GuzzleHttp\Psr7\ServerRequest(
            '', '', [], ''
        );
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new UserController($userRepoMock, $bankRepoMock, $sessionMock, $configMock, $flashMock, $twigMock, $validatorMock);
        $controller($request, $response);
    }

    public function testLoginFormWithoutSessionShouldReturnFormPage()
    {
        $_SESSION = [];
        $storage = new \Laminas\Session\Storage\ArrayStorage();
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['offsetGet', 'getStorage'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(4))
            ->method('getStorage')
            ->willReturn($storage);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMock();
        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
        $validatorMock = $this->getMockBuilder(Validator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods()
            ->getMock();
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $request = new \GuzzleHttp\Psr7\ServerRequest(
            '', '', [], ''
        );
        $request = $request->withAttribute('action', 'loginForm');
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new UserController($userRepoMock, $bankRepoMock, $sessionMock, $configMock, $flashMock, $twigMock, $validatorMock);
        /*  @var \GuzzleHttp\Psr7\Response $response */
        $response = $controller($request, $response);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('<h2 class="panel-title">Please Login</h2>', $response->getBody()->getContents());
    }

    public function testLoginFormWithSessionShouldReturnRedirect()
    {
        $_SESSION = [];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['getStorage', 'offsetGet'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(6))
            ->method('getStorage')
            ->will(static::returnSelf());
        $sessionMock->expects(static::exactly(6))
            ->method('offsetGet')
            ->willReturn(true);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMock();
        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
        $validatorMock = $this->getMockBuilder(Validator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $request = new \GuzzleHttp\Psr7\ServerRequest(
            'GET', '/user/loginForm/', [], ''
        );

        $request = $request->withAttribute('action', 'loginForm');
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new UserController($userRepoMock, $bankRepoMock, $sessionMock, $configMock, $flashMock, $twigMock, $validatorMock);
        $response = $controller($request, $response);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals('/user/form/1/', $response->getHeader('Location')[0]);
    }

    public function testViewShouldReturnUserList()
    {
        $_SESSION = [
        ];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['getStorage', 'offsetGet'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(4))
            ->method('getStorage')
            ->will(static::returnSelf());
        $sessionMock->expects(static::exactly(4))
            ->method('offsetGet')
            ->willReturn(true);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMock();
        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
        $validatorMock = $this->getMockBuilder(Validator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->setMethods(['fetchAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock->expects(static::once())
            ->method('fetchAll')
            ->willReturn([]);
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $request = new \GuzzleHttp\Psr7\ServerRequest(
            'GET', '/user/view/', [], ''
        );
        $request = $request->withAttribute('action', 'view');
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new UserController($userRepoMock, $bankRepoMock, $sessionMock, $configMock, $flashMock, $twigMock, $validatorMock);
        $response = $controller($request, $response);
        $htmlBody = $response->getBody()->getContents();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('<h1 class="page-header">User list:</h1>', $htmlBody);
        $this->assertContains('<title>View Users</title>', $htmlBody);
    }

    public function testLoginActionShouldFailForInvalidCredentials()
    {
        $_SESSION = [
            'flash_messages' => [
                'error' => []
            ]
        ];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['getStorage', 'offsetGet'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(3))
            ->method('getStorage')
            ->will(static::returnSelf());
        $sessionMock->expects(static::exactly(3))
            ->method('offsetGet')
            ->willReturn(false);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMock();
        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
        $validatorMock = $this->getMockBuilder(Validator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userStub = new UserStub();
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->setMethods(['getByEmail'])
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock->expects(static::once())
            ->method('getByEmail')
            ->willReturn($userStub);
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $request = new \GuzzleHttp\Psr7\ServerRequest(
            'POST', '/user/view/', [], 'password=&email='
        );
        $request = $request->withAttribute('action', 'login');
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new UserController($userRepoMock, $bankRepoMock, $sessionMock, $configMock, $flashMock, $twigMock, $validatorMock);
        $response = $controller($request, $response);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertContains('Invalid credentials provided.', $_SESSION['flash_messages']['error'][0]);
    }

    public function testLoginActionShouldFailForNotFoundEmailAddress()
    {
        $_SESSION = [
            'flash_messages' => [
                'error' => []
            ]
        ];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['getStorage', 'offsetGet'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(3))
            ->method('getStorage')
            ->will(static::returnSelf());
        $sessionMock->expects(static::exactly(3))
            ->method('offsetGet')
            ->willReturn(false);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMock();
        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
        $validatorMock = $this->getMockBuilder(Validator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->setMethods(['getByEmail'])
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock->expects(static::once())
            ->method('getByEmail')
            ->willThrowException(new \Exception(''));
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $request = new \GuzzleHttp\Psr7\ServerRequest(
            'POST', '/user/view/', [], 'password=&email='
        );
        $request = $request->withAttribute('action', 'login');
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new UserController($userRepoMock, $bankRepoMock, $sessionMock, $configMock, $flashMock, $twigMock, $validatorMock);
        $response = $controller($request, $response);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertContains('Email not found in system.', $_SESSION['flash_messages']['error'][0]);
    }

    public function testLoginActionShouldLoginProperUser()
    {
        $_SESSION = [
            'flash_messages' => [
                'success' => []
            ]
        ];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['getStorage', 'offsetGet', 'offsetSet'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(6))
            ->method('getStorage')
            ->will(static::returnSelf());
        $sessionMock->expects(static::exactly(3))
            ->method('offsetGet')
            ->willReturn(false);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMock();
        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
        $validatorMock = $this->getMockBuilder(Validator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userStub = new UserStub();
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->setMethods(['getByEmail'])
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock->expects(static::once())
            ->method('getByEmail')
            ->willReturn($userStub);
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $request = new \GuzzleHttp\Psr7\ServerRequest(
            'POST', '/user/view/', [], 'password=testPassword&email=testEmail'
        );
        $request = $request->withAttribute('action', 'login');
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new UserController($userRepoMock, $bankRepoMock, $sessionMock, $configMock, $flashMock, $twigMock, $validatorMock);
        $response = $controller($request, $response);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertContains('You have successfully logged in.', $_SESSION['flash_messages']['success'][0]);
    }

    public function testLogoutShouldLogOutUser()
    {
        $_SESSION = [
            'flash_messages' => [
                'success' => []
            ],
            'loggedIn' => 1,
            'loggedInEmail' => 'test@example.com'
        ];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['getStorage', 'offsetGet', 'offsetUnset', 'forgetMe'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(6))
            ->method('getStorage')
            ->will(static::returnSelf());
        $sessionMock->expects(static::exactly(3))
            ->method('offsetGet')
            ->willReturn(false);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMock();
        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $validatorMock = $this->getMockBuilder(Validator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $request = new \GuzzleHttp\Psr7\ServerRequest(
            'get', '/user/logout/', [], ''
        );
        $request = $request->withAttribute('action', 'logout');
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new UserController($userRepoMock, $bankRepoMock, $sessionMock, $configMock, $flashMock, $twigMock, $validatorMock);
        $response = $controller($request, $response);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertContains('You have successfully logged out.', $_SESSION['flash_messages']['success'][0]);
    }

    public function testFormWithoutUserIdShouldReturnCreateForm()
    {
        $_SESSION = [
            'flash_messages' => [
                'errors' => []
            ],
        ];
        $storage = new \Laminas\Session\Storage\ArrayStorage();
        $storage->offsetSet('loggedIn', 1);
        $storage->offsetSet('loggedInRole', 1);
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['offsetGet', 'getStorage'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(5))
            ->method('getStorage')
            ->willReturn($storage);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMock();
        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
        $validatorMock = $this->getMockBuilder(Validator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods()
            ->getMock();
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $request = new \GuzzleHttp\Psr7\ServerRequest(
            '', '/user/form/', [], ''
        );
        $request = $request->withAttribute('action', 'form');
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new UserController($userRepoMock, $bankRepoMock, $sessionMock, $configMock, $flashMock, $twigMock, $validatorMock);

        $response = $controller($request, $response);
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertContains('<h1 class="page-header">Create new user</h1>', $response->getBody()->getContents());
    }

    public function testFormWithUserIdShouldReturnUpdateFormForProperUser()
    {
        $_SESSION = [
            'flash_messages' => [
                'success' => []
            ],
        ];
        $storage = new \Laminas\Session\Storage\ArrayStorage();
        $storage->offsetSet('loggedIn', 1);
        $storage->offsetSet('loggedInRole', 1);
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['offsetGet', 'getStorage'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(5))
            ->method('getStorage')
            ->willReturn($storage);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMock();
        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
        $validatorMock = $this->getMockBuilder(Validator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['getById'])
            ->getMock();
        $userRepoMock->expects(self::once())
            ->method('getById')
            ->willReturn(new UserStub());
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $request = new \GuzzleHttp\Psr7\ServerRequest(
            '', '/user/form/', [], ''
        );
        $request = $request
            ->withAttribute('action', 'form')
            ->withAttribute('userId', 1);
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new UserController($userRepoMock, $bankRepoMock, $sessionMock, $configMock, $flashMock, $twigMock, $validatorMock);

        $response = $controller($request, $response);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('<h1 class="page-header">Update user: </h1>', $response->getBody()->getContents());
    }

    public function testUpdateShouldUpdateUser()
    {
        $_SESSION = [
                'flash_messages' => [
                    'success' => []
                ],
        ];
        $storage = new \Laminas\Session\Storage\ArrayStorage();
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['offsetGet', 'getStorage'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(3))
            ->method('getStorage')
            ->willReturn($storage);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMock();
        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
        $validatorMock = $this->getMockBuilder(Validator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['update'])
            ->getMock();
        $userRepoMock->expects(self::once())
            ->method('update')
            ->willReturn(new UserStub());
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $request = new \GuzzleHttp\Psr7\ServerRequest(
            '', '/user/update/', [], ''
        );
        $request = $request
            ->withAttribute('action', 'update')
            ->withAttribute('userId', 1);
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new UserController($userRepoMock, $bankRepoMock, $sessionMock, $configMock, $flashMock, $twigMock, $validatorMock);

        $response = $controller($request, $response);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertContains('User successfully updated', $_SESSION['flash_messages']['success'][0]);

    }

    public function testCreateShouldCreateUser()
    {
        $_SESSION = [
            'flash_messages' => [
                'success' => []
            ],
        ];
        $storage = new \Laminas\Session\Storage\ArrayStorage();
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['offsetGet', 'getStorage'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(3))
            ->method('getStorage')
            ->willReturn($storage);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMock();
        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
        $validatorMock = $this->getMockBuilder(Validator::class)
            ->disableOriginalConstructor()
            ->setMethods(['isValid'])
            ->getMock();
        $validatorMock->expects(self::once())
            ->method('isValid')
            ->willReturn('true');
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $userRepoMock->expects(self::once())
            ->method('create')
            ->willReturn(1);
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $request = new \GuzzleHttp\Psr7\ServerRequest(
            '', '/user/create/', [], ''
        );
        $request = $request
            ->withAttribute('action', 'create')
            ->withParsedBody(['']);
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new UserController($userRepoMock, $bankRepoMock, $sessionMock, $configMock, $flashMock, $twigMock, $validatorMock);

        $response = $controller($request, $response);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertContains('User successfully created', $_SESSION['flash_messages']['success'][0]);

    }

    public function testDeleteUserShouldDeleteUser()
    {
        $_SESSION = [
            'flash_messages' => [
                'success' => []
            ],
        ];
        $storage = new \Laminas\Session\Storage\ArrayStorage();
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['offsetGet', 'getStorage'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(3))
            ->method('getStorage')
            ->willReturn($storage);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->setMethods()
            ->getMock();
        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
        $validatorMock = $this->getMockBuilder(Validator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userRepoMock = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['deleteUser','getById'])
            ->getMock();
        $userRepoMock->expects(self::once())
            ->method('deleteUser')
            ->willReturn(true);
        $userRepoMock->expects(self::once())
            ->method('getById')
            ->willReturn(new UserStub());
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $request = new \GuzzleHttp\Psr7\ServerRequest(
            '', '/user/delete/', [], ''
        );
        $request = $request
            ->withAttribute('action', 'delete')->withAttribute('userId',1);
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new UserController($userRepoMock, $bankRepoMock, $sessionMock, $configMock, $flashMock, $twigMock, $validatorMock);

        $response = $controller($request, $response);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertContains('User successfully deleted', $_SESSION['flash_messages']['success'][0]);

    }
}

class UserStub extends User
{
    public function __construct()
    {
        parent::__construct('1', '', '', '', '', '1', 1, '', '');
    }

    public function getEmail(): string
    {
        return 'testEmail';
    }

    public function getPassword(): string
    {
        return password_hash('testPassword', PASSWORD_BCRYPT);
    }
}