<?php

namespace Test\WeCare\User\Repository;

use DateTime;
use phpDocumentor\Reflection\Types\Static_;
use WeCare\Bank\Repository\BankRepository;
use WeCare\User\Mapper\User;
use WeCare\User\Repository\UserRepository;
use PHPUnit\Framework\TestCase;

class UserRepositoryTest extends TestCase
{
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Wrong param provided.
     */
    public function testGetByEmailWithEmptyEmailShouldReturnException()
    {
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userMapperMock = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->getMock();
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->getMock();
        $dt = new DateTime();

        $userRepository = new UserRepository($userMapperMock,$dt, $configMock,$bankRepoMock);
        $email = '';
        $userRepository->getByEmail($email);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage User entity not found.
     */
    public function testGetByEmailEmailNotFoundShouldReturnException()
    {
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userMapperMock = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->setMethods(['fetchAll'])
            ->getMock();
        $userMapperMock->expects(static::once())
            ->method('fetchAll')
            ->willReturn(array());
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->getMock();
        $dt = new DateTime();

        $userRepository = new UserRepository($userMapperMock, $dt, $configMock,$bankRepoMock);
        $email = 'test@example.com';
        $userRepository->getByEmail($email);
    }

    public function testGetByEmailWithValidEmailShouldReturnUserModel()
    {
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userMapperMock = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->setMethods(['fetchAll'])
            ->getMock();
        $userMapperMock->expects(static::once())
            ->method('fetchAll')
            ->willReturn([
                [
                    'email' => 'test@example.com',
                    'userId' => '1',
                    'password' => 'test123',
                    'displayName' => 'test',
                    'isActive' => 1,
                    'role' => 1
                ]
            ]);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->getMock();
        $dt = new DateTime();
        $userRepository = new UserRepository($userMapperMock, $dt, $configMock,$bankRepoMock);
        $email = 'test@example.com';
        $this->assertInstanceOf('\WeCare\User\Model\User', $userRepository->getByEmail($email));
    }

    public function testGetByIdShouldReturnUserModel()
    {
        $userMapperMock = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->setMethods(['fetchById', 'fetchAll'])
            ->getMock();
        $userMapperMock->expects(static::exactly(1))
            ->method('fetchById')
            ->willReturn(
                [
                    'email' => 'test@example.com',
                    'userId' => '1',
                    'password' => 'test123',
                    'displayName' => 'test',
                    'isActive' => 1,
                    'role' => 1,
                    'createdAt' =>'',
                    'updatedAt' => '',
                ]
            );
        $bankRepoMock = $this->getMockBuilder(BankRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->getMock();
        $dt = new DateTime();
        $userRepository = new UserRepository($userMapperMock, $dt, $configMock, $bankRepoMock);
        $this->assertInstanceOf('\WeCare\User\Model\User', $userRepository->getById(1));
    }
}
