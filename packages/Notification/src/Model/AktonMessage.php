<?php
declare(strict_types=1);
namespace WeCare\Notification\Model;

class AktonMessage
{
    /**
     * @var string message id
     */
    private $cmId;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var int [106 => 'one way, text', 108 => 'one way, text image button', 210 => 'two way, text image button']
     */
    private $messageType;

    /**
     * @var string
     */
    private $messageText;

    /**
     * @var string
     */
    private $buttonCaption;

    /**
     * @var string
     */
    private $buttonUrl;

    /**
     * @var string
     */
    private $imageUrl;

    /**
     * @var bool
     */
    private $smsSend;

    /**
     * @var string
     */
    private $smsText;

    /**
     * @var string 'WeCare', // AKTON
     */
    private $smsDisplay;

    /**
     * @var int [5 => 'undelivered', 6 => 'viber message expired', 8 => 'user blocked sender on viber'
     */
    private $smsRule;

    private $validity = 345600;

    /**
     * AktonMessage constructor.
     * @param string $cmId
     * @param string $phoneNumber
     * @param int $messageType
     * @param string $messageText
     * @param string $buttonCaption
     * @param string $buttonUrl
     * @param string $imageUrl
     * @param bool $smsSend
     * @param string $smsText
     * @param string $smsDisplay
     * @param string $smsRule
     */
    public function __construct(
        string $cmId, string $phoneNumber, string $messageText, string $buttonCaption, string $buttonUrl, string $imageUrl,
        string $smsText, bool $smsSend = true, int $messageType = 106, string $smsDisplay = 'WeCare', string $smsRule = '5, 6, 8')
    {
        $this->cmId = $cmId;
        $this->phoneNumber = $phoneNumber;
        $this->messageText = $messageText;
        $this->buttonCaption = $buttonCaption;
        $this->buttonUrl = $buttonUrl;
        $this->imageUrl = $imageUrl;
        $this->smsText = $smsText;
        $this->smsDisplay = $smsDisplay;
        $this->smsRule = $smsRule;
        $this->messageType = $messageType;
        $this->smsSend = $smsSend;
    }

    public function toArray()
    {
        $data = [];
        foreach (get_object_vars($this) as $prop => $value) {
            $data[$prop] = $value;
        }

        return $data;
    }
}