<?php
declare(strict_types=1);
namespace WeCare\Notification\Service;

use Psr\Log\LoggerInterface as Logger;
use GuzzleHttp\Client;
use WeCare\Notification\Model\AktonMessage;


class Akton
{
    /**
     * @var Client
     */
    private $httpClient;

    private $logger;


    /**
     * Akton constructor.
     * @param \GuzzleHttp\Client $httpClient
     */
    public function __construct(Client $httpClient, Logger $logger)
    {
        $this->httpClient = $httpClient;
        $this->logger = $logger;
    }

    public function send(AktonMessage $message)
    {
        $endpoint = 'https://viber1.akton.net:4000';
        $params = [
            'clientId' => '1757aaed167ab036219ba2795b6223',
            'displayName' => 'We Care',
            'message' => $message->toArray()
        ];

        try {
            echo 'sending POST to ' . $endpoint . '/message' . PHP_EOL;
            echo "sending data: " . json_encode($params);

            $res = $this->httpClient->request('POST', '/message', [
                'body' => json_encode($params),
                'headers' => [
                    'Content-Type' => 'application/json; charset=UTF-8'
                ]
            ]);
        } catch(\GuzzleHttp\Exception\GuzzleException $e) {
            var_dump($e->getMessage());
            die();
        }

        var_dump($res->getStatusCode());
        var_dump($res->getBody());
    }
}