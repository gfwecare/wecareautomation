<?php
declare(strict_types=1);
namespace WeCare\Notification\Controller;

use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Controller\Controller;
use WeCare\Notification\Model\AktonMessage;
use WeCare\Notification\Service\Akton;
use Psr\Log\LoggerInterface as Logger;

class NotificationController extends Controller
{
    /**
     * @var Akton
     */
    private $akton;


    private $logger;

    public function __construct(Akton $akton, Session $session, Config $config, Flash $flash, \Twig\Environment $twig, Logger $logger)
    {
        parent::__construct($twig, $config, $session, $flash);

        $this->logger = $logger;
        $this->akton = $akton;
    }

    public function testAkton()
    {
        $messageText = 'test';

        $data['cmId'] = '1234';
        $data['phoneNumber'] = '381641384708';
        $data['messageText'] = $messageText;
        $data['smsText'] = $messageText;
        $data['buttonCaption'] = 'klikni me';
        $data['buttonUrl'] = 'http://www.google.com';
        $data['imageUrl'] = 'https://5.imimg.com/data5/CK/AS/MY-60212530/9-ply-duplex-box-500x500.jpg';

        $message = new AktonMessage($data['cmId'], $data['phoneNumber'], $data['messageText'], $data['buttonCaption'],
            $data['buttonUrl'], $data['imageUrl'], $data['smsText']);

        $this->akton->send($message);
    }
}