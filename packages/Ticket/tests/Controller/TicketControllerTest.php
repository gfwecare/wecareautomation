<?php
namespace Test\WeCare\Ticket\Controller;

use Laminas\Session\SessionManager;
use Tamtamchik\SimpleFlash\Flash;
use WeCare\CityExpress\Model\StatusListStub;
use WeCare\CityExpress\Service\Api;
use WeCare\Ticket\Controller\TicketController;
use WeCare\Ticket\Service\Ticket;

class TicketControllerTest extends \PHPUnit\Framework\TestCase
{
    private $cityConfigMock;
    private $twigMock;

    public function setUp(): void
    {
        $this->cityConfigMock = json_decode(json_encode(include(__DIR__ . '/../fixtures/config-city.php')));


        $loader = new \Twig\Loader\FilesystemLoader(
            __DIR__ . '/../../../../themes/sbadmin/'
        );
        $this->twigMock = new \Twig\Environment($loader, array(
            'debug' => true,
        ));
    }

    public function testCheckShipmentStatusWillUpdateTicketDataWhenDeliveredToService()
    {
        $kyTicketMock = $this->getMockBuilder(\kyTicket::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getId', 'getStatus', 'getType', 'getCustomField'])
            ->addMethods(['getTitle', 'setValue', 'setTimestamp'])
            ->getMockForAbstractClass();
        $kyTicketMock->expects($this->any())
            ->method('getId')
            ->will(static::returnValue('test-ticket-id'));
        $kyTicketMock->expects(static::any())
            ->method('getStatus')
            ->will(static::returnValue(new StatusMock(190)));
        $kyTicketMock->expects(static::exactly(1))
            ->method('getType')
            ->will(static::returnSelf());
        $kyTicketMock->expects(static::any())
            ->method('getCustomField')
            ->willReturnSelf();
        $kyTicketMock->expects(static::exactly(1))
            ->method('getTitle')
            ->willReturn('Adapter');

        $ticketServiceMock = $this->getMockBuilder(Ticket::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getTicketFromKayako'])
            ->getMockForAbstractClass();
        $ticketServiceMock->method('getTicketFromKayako')
            ->willReturn($kyTicketMock);
        $cityServiceMock = $this->getMockBuilder(Api::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getShipmentStatusByRef'])
            ->getMockForAbstractClass();
        $dt = new \DateTime();
        $cityServiceMock->expects(static::once())
            ->method('getShipmentStatusByRef')
            ->willReturn(new StatusListStub(true, false, $dt));

        $_SESSION = [];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getStorage'])
            ->addMethods(['offsetGet'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(4))
            ->method('getStorage')
            ->will(static::returnSelf());
        $sessionMock->expects(static::exactly(4))
            ->method('offsetGet')
            ->willReturn(true);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['get'])
            ->getMock();
        $configMock->expects(static::once())
            ->method('get')
            ->willReturn($this->cityConfigMock);
        $request = new \GuzzleHttp\Psr7\ServerRequest(
            'get', '', [], ''
        );
        $request = $request->withAttribute('action', 'checkShipmentStatus');

        $controller = new TicketController($ticketServiceMock, $cityServiceMock, $sessionMock, $configMock, new Flash(), $this->twigMock);
        $response = $controller($request, new \GuzzleHttp\Psr7\Response());
        $response->getBody()->rewind();
        $body = $response->getBody()->getContents();
        $this->assertStringContainsString('Shipment status check complete.', $body);
        $this->assertStringContainsString('Ticket test-ticket-id delivered to service at ' . $dt->format('m-d-Y'), $body);
    }

    public function testCheckShipmentStatusWillUpdateTicketDataWhenDeliveredToMpo()
    {
        $kyTicketMock = $this->getMockBuilder(\kyTicket::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getId', 'getStatus', 'getType', 'getCustomField'])
            ->addMethods(['getTitle', 'setValue', 'setTimestamp'])
            ->getMockForAbstractClass();
        $kyTicketMock->expects($this->any())
            ->method('getId')
            ->will(static::returnValue('test-ticket-id'));
        $kyTicketMock->expects(static::any())
            ->method('getStatus')
            ->will(static::returnValue(new StatusMock(191)));
        $kyTicketMock->expects(static::any())
            ->method('getStatus')
            ->will(static::returnSelf());
        $kyTicketMock->expects(static::exactly(1))
            ->method('getType')
            ->will(static::returnSelf());
        $kyTicketMock->expects(static::any())
            ->method('getCustomField')
            ->willReturnSelf();
        $kyTicketMock->expects(static::exactly(1))
            ->method('getTitle')
            ->willReturn('Adapter');

        $ticketServiceMock = $this->getMockBuilder(Ticket::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getTicketFromKayako'])
            ->getMockForAbstractClass();
        $ticketServiceMock->method('getTicketFromKayako')
            ->willReturn($kyTicketMock);
        $cityServiceMock = $this->getMockBuilder(Api::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getShipmentStatusByRef'])
            ->getMockForAbstractClass();
        $dt = new \DateTime();
        $cityServiceMock->expects(static::once())
            ->method('getShipmentStatusByRef')
            ->willReturn(new StatusListStub(true, false, $dt));

        $_SESSION = [];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getStorage'])
            ->addMethods(['offsetGet'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(4))
            ->method('getStorage')
            ->will(static::returnSelf());
        $sessionMock->expects(static::exactly(4))
            ->method('offsetGet')
            ->willReturn(true);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['get'])
            ->getMock();
        $configMock->expects(static::once())
            ->method('get')
            ->willReturn($this->cityConfigMock);
        $request = new \GuzzleHttp\Psr7\ServerRequest(
            'get', '', [], ''
        );
        $request = $request->withAttribute('action', 'checkShipmentStatus');

        $controller = new TicketController($ticketServiceMock, $cityServiceMock, $sessionMock, $configMock, new Flash(), $this->twigMock);
        $response = $controller($request, new \GuzzleHttp\Psr7\Response());
        $response->getBody()->rewind();
        $body = $response->getBody()->getContents();
        $this->assertStringContainsString('Shipment status check complete.', $body);
        $this->assertStringContainsString('Ticket test-ticket-id delivered to MPO at ' . $dt->format('m-d-Y'), $body);
    }

    public function testCheckShipmentStatusWillUpdateTicketStatusWhenInTransitToService()
    {
        $kyTicketMock = $this->getMockBuilder(\kyTicket::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getId', 'getStatus', 'getType', 'update']) //, 'setStatusId', 'update'
            ->addMethods(['getTitle'])
            ->getMockForAbstractClass();
        $kyTicketMock->expects($this->any())
            ->method('getId')
            ->will(static::returnValue('test-ticket-id'));
        $kyTicketMock->expects(static::any())
            ->method('getStatus')
            ->will(static::returnValue(new StatusMock(1)));

        $kyTicketMock->expects(static::exactly(1))
            ->method('getType')
            ->will(static::returnSelf());
        $kyTicketMock->expects(static::exactly(1))
            ->method('getTitle')
            ->willReturn('Adapter');

        $ticketServiceMock = $this->getMockBuilder(Ticket::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getTicketFromKayako'])
            ->getMockForAbstractClass();
        $ticketServiceMock->method('getTicketFromKayako')
            ->willReturn($kyTicketMock);
        $cityServiceMock = $this->getMockBuilder(Api::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getShipmentStatusByRef'])
            ->getMockForAbstractClass();
        $dt = new \DateTime();
        $cityServiceMock->expects(static::once())
            ->method('getShipmentStatusByRef')
            ->willReturn(new StatusListStub(false, true, $dt));

        $_SESSION = [];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getStorage'])
            ->addMethods(['offsetGet'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(4))
            ->method('getStorage')
            ->will(static::returnSelf());
        $sessionMock->expects(static::exactly(4))
            ->method('offsetGet')
            ->willReturn(true);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['get'])
            ->getMock();
        $configMock->expects(static::once())
            ->method('get')
            ->willReturn($this->cityConfigMock);
        $request = new \GuzzleHttp\Psr7\ServerRequest(
            'get', '', [], ''
        );
        $request = $request->withAttribute('action', 'checkShipmentStatus');

        $controller = new TicketController($ticketServiceMock, $cityServiceMock, $sessionMock, $configMock, new Flash(), $this->twigMock);
        $response = $controller($request, new \GuzzleHttp\Psr7\Response());
        $response->getBody()->rewind();
        $body = $response->getBody()->getContents();
        $this->assertStringContainsString('Shipment status check complete.', $body);
        $this->assertStringContainsString('Ticket test-ticket-id: status changed to "Poslat iz MPO u servis" at ' . $dt->format('m-d-Y'), $body);
    }

    public function testCheckShipmentStatusWillUpdateTicketStatusWhenInTransitToMpo()
    {
        $kyTicketMock = $this->getMockBuilder(\kyTicket::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getId', 'getStatus', 'getType', 'update']) //, 'setStatusId', 'update'
            ->addMethods(['getTitle'])
            ->getMockForAbstractClass();
        $kyTicketMock->expects($this->any())
            ->method('getId')
            ->will(static::returnValue('test-ticket-id'));
        $kyTicketMock->expects(static::any())
            ->method('getStatus')
            ->will(static::returnValue(new StatusMock(252)));

        $kyTicketMock->expects(static::exactly(1))
            ->method('getType')
            ->will(static::returnSelf());
        $kyTicketMock->expects(static::exactly(1))
            ->method('getTitle')
            ->willReturn('Adapter');

        $ticketServiceMock = $this->getMockBuilder(Ticket::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getTicketFromKayako'])
            ->getMockForAbstractClass();
        $ticketServiceMock->method('getTicketFromKayako')
            ->willReturn($kyTicketMock);
        $cityServiceMock = $this->getMockBuilder(Api::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getShipmentStatusByRef'])
            ->getMockForAbstractClass();
        $dt = new \DateTime();
        $cityServiceMock->expects(static::once())
            ->method('getShipmentStatusByRef')
            ->willReturn(new StatusListStub(false, true, $dt));

        $_SESSION = [];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getStorage'])
            ->addMethods(['offsetGet'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(4))
            ->method('getStorage')
            ->will(static::returnSelf());
        $sessionMock->expects(static::exactly(4))
            ->method('offsetGet')
            ->willReturn(true);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['get'])
            ->getMock();
        $configMock->expects(static::once())
            ->method('get')
            ->willReturn($this->cityConfigMock);
        $request = new \GuzzleHttp\Psr7\ServerRequest(
            'get', '', [], ''
        );
        $request = $request->withAttribute('action', 'checkShipmentStatus');

        $controller = new TicketController($ticketServiceMock, $cityServiceMock, $sessionMock, $configMock, new Flash(), $this->twigMock);
        $response = $controller($request, new \GuzzleHttp\Psr7\Response());
        $response->getBody()->rewind();
        $body = $response->getBody()->getContents();
        $this->assertStringContainsString('Shipment status check complete.', $body);
        $this->assertStringContainsString('Ticket test-ticket-id: status changed to "Poslat iz servisa u MPO" at ' . $dt->format('m-d-Y'), $body);
    }

    public function testCheckShipmentStatusWillFailAndReturnError()
    {
        $kyTicketMock = $this->getMockBuilder(\kyTicket::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getId', 'getStatus', 'getType', 'update']) //, 'setStatusId', 'update'
            ->addMethods(['getTitle'])
            ->getMockForAbstractClass();
        $kyTicketMock->expects($this->any())
            ->method('getId')
            ->will(static::returnValue('test-ticket-id'));
//        $kyTicketMock->expects(static::any())
//            ->method('getStatus')
//            ->will(static::returnValue(new StatusMock(252)));

        $kyTicketMock->expects(static::exactly(1))
            ->method('getType')
            ->will(static::returnSelf());
        $kyTicketMock->expects(static::exactly(1))
            ->method('getTitle')
            ->willReturn('Adapter');

        $ticketServiceMock = $this->getMockBuilder(Ticket::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getTicketFromKayako'])
            ->getMockForAbstractClass();
        $ticketServiceMock->method('getTicketFromKayako')
            ->willReturn($kyTicketMock);
        $cityServiceMock = $this->getMockBuilder(Api::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getShipmentStatusByRef'])
            ->getMockForAbstractClass();
        $dt = new \DateTime();
        $cityServiceMock->expects(static::once())
            ->method('getShipmentStatusByRef')
            ->willThrowException(new \Exception('Shipment not found'));

        $_SESSION = [];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getStorage'])
            ->addMethods(['offsetGet'])
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(4))
            ->method('getStorage')
            ->will(static::returnSelf());
        $sessionMock->expects(static::exactly(4))
            ->method('offsetGet')
            ->willReturn(true);
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['get'])
            ->getMock();
        $configMock->expects(static::once())
            ->method('get')
            ->willReturn($this->cityConfigMock);
        $request = new \GuzzleHttp\Psr7\ServerRequest(
            'get', '', [], ''
        );
        $request = $request->withAttribute('action', 'checkShipmentStatus');

        $controller = new TicketController($ticketServiceMock, $cityServiceMock, $sessionMock, $configMock, new Flash(), $this->twigMock);
        $response = $controller($request, new \GuzzleHttp\Psr7\Response());
        $response->getBody()->rewind();
        $body = $response->getBody()->getContents();
        $this->assertStringContainsString('Shipment status check complete.', $body);
        $this->assertStringContainsString('Ticket test-ticket-id: could not be found @ courier service.', $body);
    }
}

class StatusMock
{
    public $status;

    public function __construct($status)
    {
        $this->status = $status;
    }

    public function getId()
    {
        return $this->status;
    }
}