<?php

return [
    'city' => [
        'endpoint' => 'some.endpoint',
        'apiKeys' => [
            'Mobilni telefon' => [
                'apiKey' => 'api-key-mobilni',
                'mapping' => ['Mobilni telefon', 'Tablet']
            ],
            'MKA' => [
                'apiKey' => 'api-key-mka',
                'mapping' => ['MKA']
            ],
            'TV' => [
                'apiKey' => 'api-key-tv',
                'mapping' => ['TV']
            ],
            'LAPTOP' => [
                'apiKey' => 'api-key-laptop',
                'mapping' => ['Notebook', 'Netbook']
            ],
            'DESKTOP' => [
                'apiKey' => 'api-key-dekstop',
                'mapping' => ['DesktopPC']
            ],
            'MONITOR' => [
                'apiKey' => 'api-key-monitor',
                'mapping' => ['Monitor']
            ],
            'OSTALO' => [
                'apiKey' => 'api-key-ostalo',
            ]

        ]
    ]
];