<?php
declare(strict_types=1);
namespace WeCare\Ticket\Service;

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class XlsParser
{
    private $mpoSource;

    private $externalSource;

    private $reader;

    public function __construct(Xlsx $reader, $mpoSource, $externalSource)
    {
        $this->mpoSource = $mpoSource;
        $this->externalSource = $externalSource;
        $this->reader = $reader;
    }

    public function getExternalServiceList()
    {
        $spreadsheet = $this->reader->load($this->externalSource);
        $worksheet = $spreadsheet->getActiveSheet();
        $list = [];
        foreach ($worksheet->getRowIterator() as $row) {
            if ($row->getRowIndex() < 2) {
                continue;
            }
            if ($worksheet->getCell('A'. $row->getRowIndex())->getValue() === '') {
                break;
            }
            foreach ($row->getCellIterator() as $cell) {
                if ($cell->getColumn() === 'A') {
                    $name = $cell->getValue();
                }
                if ($cell->getColumn() === 'B') {
                    $address = $cell->getValue();
                }
                if ($cell->getColumn() === 'C') {
                    $city = $cell->getValue();
                }
                if ($cell->getColumn() === 'D') {
                    $zip = $cell->getValue();
                }
                if ($cell->getColumn() === 'E') {
                    $phone = $cell->getValue();
                }
                if ($cell->getColumn() === 'F') {
                    $vendor = $cell->getValue();
                }
                if ($cell->getColumn() === 'G') {
                    $type = (string) $cell->getValue();
                }
                if ($cell->getColumn() === 'H') {
                    $ignoredType = (string) $cell->getValue();
                }
                if ($cell->getColumn() === 'I') {
                    $kayakoId = $cell->getValue();
                }
            }
            $list[] = [
                'name' => $name,
                'address' => $address,
                'city' => $city,
                'zip' => $zip,
                'phone' => $phone,
                'manufacturer' => $vendor,
                'type' => trim($type),
                'ignoredType' => trim($ignoredType),
                'kayakoId' => $kayakoId,
            ];
        }

        return $list;
    }

    public function getMpoList()
    {
        $spreadsheet = $this->reader->load($this->mpoSource);
        $worksheet = $spreadsheet->getActiveSheet();
        $mpoList = [];
        foreach ($worksheet->getRowIterator() as $row) {
            if ($row->getRowIndex() < 1) {
                continue;
            }
            foreach ($row->getCellIterator() as $cell) {
                if ($cell->getColumn() === 'B') {
                    $name = $cell->getValue();
                }
                if ($cell->getColumn() === 'C') {
                    $address = $cell->getValue();
                }
                if ($cell->getColumn() === 'D') {
                    $city = $cell->getValue();
                }
                if ($cell->getColumn() === 'E') {
                    $zip = $cell->getValue();
                }
                if ($cell->getColumn() === 'F') {
                    $phone = $cell->getValue();
                }
                if ($cell->getColumn() === 'G') {
                    $email = $cell->getValue();
                }
                if ($cell->getColumn() === 'A') {
                    $apiId = $cell->getValue();
                }
            }
            $mpoList[] = [
                'name' => $name,
                'address' => $address,
                'city' => $city,
                'zip' => $zip,
                'phone' => $phone,
                'email' => $email,
                'apiId' => $apiId,
            ];
        }

        return $mpoList;
    }

}