<?php
declare(strict_types=1);
namespace WeCare\Ticket\Service;

use WeCare\CityExpress\Service\ApiKeyParser;
use WeCare\Ticket\Model\Ticket as TicketModel;
use WeCare\Ticket\Service\Kayako;
use Monolog\Logger;
use WeCare\CityExpress\Service\Api;
use WeCare\Ticket\Entity\Ticket as TicketEntity;

class Ticket
{
    /**
     * @var Api
     */
    private $cityApi;

    private $logger;

    /**
     * @var Kayako
     */
    private $kayakoApi;

    public function __construct(Api $cityApi, Kayako $kayako, Logger $logger)
    {
        $this->cityApi = $cityApi;
        $this->kayakoApi = $kayako;
        $this->logger = $logger;
    }

    /**
     * @param $ticketId
     * @return \kyTicket
     * @throws \Exception
     */
    public function getTicketFromKayako($ticketId)
    {
        return $this->kayakoApi->getTicketById($ticketId);
    }

    /**
     * @param $ticketId
     * @param $apiKey
     * @return \WeCare\CityExpress\Model\StatusList
     * @throws \Exception
     */
//    public function getTicketShipmentStatusFromCity($ticketId, $apiKey)
//    {
//        try {
//            $status = $this->cityApi->getShipmentStatusByRef($ticketId, $apiKey);
//        } catch (\Exception $e) {
//            if (false !== strpos($e->getMessage(), 'Shipment not found')) {
//                $msg = sprintf('Ticket %s: not found @ courier service with apiKey: %s.', $ticketId, $apiKey);
//                throw new \Exception($msg);
//            }
//            echo $e->getMessage();
//            die();
//        }
//
//        return $status;
//    }

    public function getCityReports($apiKey, $name)
    {
        $log = [];
        try {
            $labels = $this->cityApi->getShippingLabels($apiKey);
            $fileData = base64_decode($labels->PdfDocument);
            if (strlen($fileData) > 0) {
                $filePath = sprintf('%s/data/labels-%s-%s.pdf', APP_PATH, $name, md5($fileData));
                file_put_contents($filePath, $fileData);
                $log['labels'] = $filePath;
            }
        } catch (\Exception $e) {
//            if ($e->getMessage() !== 'NoShipmentsForPickupFound') {
//                echo $e->getMessage() . PHP_EOL;
//            }
        }
        try {
            $pickupResponse = $this->cityApi->requestPickup($apiKey); // change shipment status from 1 -> 11;
            $fileData = base64_decode($pickupResponse->PickupListDocument);
            if (strlen($fileData) > 0) {
                $filePath = sprintf('%s/data/reports-%s-%s.pdf', APP_PATH, $name, md5($fileData));
                file_put_contents($filePath, $fileData);
                $log['reports'] = $filePath;
            }
        } catch (\Exception $e) {
//            if ($e->getMessage() !== 'NoShipmentsForPickupFound') {
//                echo $e->getMessage() . PHP_EOL;
//            }
        }
        return $log;
    }

    public function sendShipmentToServiceDepartment(TicketEntity $ticket, $mpoData, $apiKey, $manufacturer = '', $externalService = null)
    {
        $res = $this->cityApi->createShipmentPlain(
            $apiKey, $ticket->getId(), $ticket->getType()->getTitle(), $mpoData, $manufacturer, $externalService
        );
        $ticket->setFieldValue(TicketModel::FIELD_RECEIVING_TYPE, serialize(['3386',[3386 => '3387']]), '');
        usleep(100);

        // Broj primljene pošiljke
        $ticket->setFieldValue(TicketModel::FIELD_RECEIVED_SHIPMENT_CODE, $res->getCreatedShipmentId(), '');
        usleep(100);

        // Shipment ID iz MPO
        $ticket->setFieldValue(TicketModel::FIELD_RECEIVED_SHIPMENT_ID, $res->getCreatedShipmentId(), '');

        return $res->getCreatedShipmentId();
    }

    public function sendShipmentToRetailDepartment(TicketEntity $ticket, $mpoData, $apiKey, $manufacturer = '')
    {
        $res = $this->cityApi->createShipment(
            $apiKey, $ticket->getId(), $ticket->getType()->getTitle(), $mpoData, $manufacturer
        );
        $ticket->setFieldValue(TicketModel::FIELD_SENT_SHIPMENT_ID, $res->getCreatedShipmentId(), '');
        usleep(100);

//        $sentShipmentId = $ticket->getFieldValue(TicketModel::FIELD_SENT_SHIPMENT_ID);
//        $this->logger->log(Logger::INFO, '**** sentShipmentId verification, sent : ' . $sentShipmentId . '; received: ' . $res->getCreatedShipmentId());
//        if (!$sentShipmentId || strlen($sentShipmentId) === 0) {
//            $this->logger->log(Logger::INFO, '**** sentShipmentId missing !? : ' . $sentShipmentId);
//            $ticket->setFieldValue(TicketModel::FIELD_SENT_SHIPMENT_ID, $res->getCreatedShipmentId(), '');
//            usleep(100);
//            $sentShipmentId = $ticket->getFieldValue(TicketModel::FIELD_SENT_SHIPMENT_ID);
//            $this->logger->log(Logger::INFO, '**** sentShipmentId verification X2, sent : ' . $sentShipmentId . '; received: ' . $res->getCreatedShipmentId());
//            if (!$sentShipmentId || strlen($sentShipmentId) === 0) {
//                $this->logger->log(Logger::INFO, '**** sentShipmentId missing X2 !? : ' . $sentShipmentId);
//                $ticket->setFieldValue(TicketModel::FIELD_SENT_SHIPMENT_ID, $res->getCreatedShipmentId(), '');
//            }
//        }

        usleep(100);
        //Broj poslate pošiljke
        $ticket->setFieldValue(TicketModel::FIELD_SENT_SHIPMENT_CODE, $res->getCreatedShipmentId(), '');
        usleep(100);
        $ticket->setFieldValue(TicketModel::FIELD_SENDING_TYPE, serialize(['3416',[3416 => '3417']]), '');

        return $res->getCreatedShipmentId();
    }

    public function saveCityBarcodeToTicket(TicketEntity $ticket, $apiKey)
    {
//        $receivedShipmentId = $ticket->getField(TicketModel::FIELD_RECEIVED_SHIPMENT_ID)->getRawValue();
//        $receivedBarcode = $ticket->getField(TicketModel::FIELD_RECEIVED_SHIPMENT_CODE)->getRawValue();
//        $sentShipmentId = $ticket->getField(TicketModel::FIELD_SENT_SHIPMENT_ID)->getRawValue();
//        $sentBarcode = $ticket->getField(TicketModel::FIELD_SENT_SHIPMENT_CODE)->getRawValue();

        $receivedShipmentId = (string) $ticket->getFieldValue(TicketModel::FIELD_RECEIVED_SHIPMENT_ID);
        $receivedBarcode = $ticket->getFieldValue(TicketModel::FIELD_RECEIVED_SHIPMENT_CODE);
        $sentShipmentId = (string) $ticket->getFieldValue(TicketModel::FIELD_SENT_SHIPMENT_ID);
        $sentBarcode = $ticket->getFieldValue(TicketModel::FIELD_SENT_SHIPMENT_CODE);

        $receivedShipment = false;
        if (strlen($receivedShipmentId) !== 0 && substr($receivedBarcode, 0, 2) !== "00") {
            if ($receivedBarcode == $receivedShipmentId) {
                try {
                    $status = $this->cityApi->getShipmentApiStatus($receivedShipmentId, $apiKey);
                    $receivedBarcode = $status->getBarcode();
                    if (strlen($receivedBarcode) > 0) {
                        // save incoming data
                        $ticket->setFieldValue(TicketModel::FIELD_RECEIVED_SHIPMENT_CODE, $receivedBarcode, $receivedShipmentId);
//                    $ticket->getField(TicketModel::FIELD_RECEIVED_SHIPMENT_CODE)->setValue($receivedBarcode);
//                        $ticket->updateFields();
                        $receivedShipment['code'] = $receivedBarcode;
                        $receivedShipment['date'] = $status->getDeliveryDate();
//                        var_dump('updated ticket ' . $ticket->getId());
                    }
                } catch (\Exception $e) {
                    throw $e;
                }
            }
        }

        $sentShipment = false;
        if ($sentShipmentId) {
            if ($sentBarcode == $sentShipmentId) {
                try {
                    $status = $this->cityApi->getShipmentApiStatus($sentShipmentId, $apiKey);
                    $sentBarcode = $status->getBarcode();
                    if (strlen($sentBarcode) > 0) {
                        // @TODO log saved tickets
                        $ticket->setFieldValue(TicketModel::FIELD_SENT_SHIPMENT_CODE, $sentBarcode, $sentShipmentId);
//                        $ticket->getField(TicketModel::FIELD_SENT_SHIPMENT_CODE)->setValue($sentBarcode);
//                        $ticket->updateFields();
                        $sentShipment['code'] = $sentBarcode;
                        $sentShipment['date'] = $status->getDeliveryDate();
                    }
                } catch (\Exception $e) {
                    throw $e;
                }
            }
        }

        return [
            'receivedBarcode' => $receivedShipment, 'sentBarcode' => $sentShipment,
        ];
    }

    // @TODO might need to retry these actions
    /**
     * Syncing kayako with city data.
     *
     * @param \kyTicket $ticket
     */
    public function packagePickedUpFromMpo(\kyTicket $ticket)
    {
        $ticket->setStatusId(TicketModel::STATUS_MPO_SENT_TO_SERVICE);
        $ticket->update();
    }

    /**
     * Syncing kayako with city data.
     *
     * @param \kyTicket $ticket
     */
    public function packagePickedUpFromService(\kyTicket $ticket)
    {
        $ticket->setDepartmentId(TicketModel::DEPARTMENT_RETAIL);
        $ticket->setStatusId(TicketModel::STATUS_SERVICE_SENT_TO_MPO);
        $ticket->update();
    }

    public function deliveredToServiceLocation(TicketEntity $ticket, $date)
    {
        // @TODO move to config
        $wecareId = 8169;  // prod
//        $wecareId = 8040; // dev
        $gorenjeId = 6407;
        $systemOneId = 6501;
        $localServices = [$wecareId, $gorenjeId, $systemOneId];
        $migrationDate = strtotime('2021-01-11 00:00:00');
        $ticketDate = strtotime($ticket->getTicket()->getCreationTime());
        if ($ticketDate < $migrationDate) { // logic for tickets created before new changes were applied
            $ticket->setFieldValue(TicketModel::FIELD_DATE_FROM_MPO, $date, '');
            $ticket->setDepartment(TicketModel::DEPARTMENT_EXTERNAL_SERVICE);
            $ticket->setStatus(TicketModel::STATUS_DELIVERED_TO_SERVICE);
        } else {
            // care & repair and gorenje locations
            if (in_array($ticket->getFieldValue(TicketModel::FIELD_EXTERNAL_SERVICE), $localServices)) {
                $ticket->setFieldValue(TicketModel::FIELD_DATE_FROM_MPO, $date, '');
                $ticket->setDepartment(TicketModel::DEPARTMENT_EXTERNAL_SERVICE);
                $ticket->setStatus(TicketModel::STATUS_DELIVERED_TO_SERVICE);
            } else {
                $ticket->setFieldValue(TicketModel::FIELD_DATE_TO_EXTERNAL_SERVICE, $date, '');
                $ticket->setDepartment(TicketModel::DEPARTMENT_EXTERNAL_SERVICE);
                $ticket->setStatus(TicketModel::STATUS_DEVICE_IN_ES);
            }
        }
        $ticket->update();
    }

    public function deliveredToMpoLocation(TicketEntity $ticket, $date)
    {
        $ticket->setFieldValue(TicketModel::FIELD_DATE_TO_MPO, $date, '');
        $ticket->setStatus(TicketModel::STATUS_RETURNED_TO_MPO);
        $ticket->update();
//        $ticket->getCustomField('u4n3mf4hfyha')->setValue($shipmentId);
    }
}

// syurmnnuryqi => Datum isporuke iz MPO u logistiku
// u4n3mf4hfyha => Datum isporuke u MPO  iz logistike
// ar65fcdxf2am => Broj primljene pošiljke
// t7d1iu4nb0qu => Broj poslate pošiljke
// 4x30sgxkr2nm => Prijemno mesto reklamacije
// 3qnk0banthtn => Prijem uređaja => City express