<?php
declare(strict_types=1);
namespace WeCare\Ticket\Service;


class Kayako
{
    private $counter;

    public function __construct(\kyConfig $config)
    {
        \kyConfig::set($config);
        $this->counter = 0;
        \kyConfig::get()->setDebugEnabled(false); // fetch from config..
    }

    public function getDepartments()
    {
        return \kyDepartment::getAll();
    }

    /**
     * @param $ticketId
     * @return \kyTicket
     */
    public function getTicketById($ticketId)
    {
        try {
            $ticket = \kyTicket::get($ticketId);
            $this->counter++;
        } catch (\Exception $e) {
            if (false !== strpos($e->getMessage(), 'timed out after ')) {
                var_dump('retrying ticket ' . $ticketId);
                if ($this->counter < 5) {
                    usleep(200);
                    $ticket = $this->getTicketById($ticketId);
                } else {
                    throw new \Exception(sprintf('Couldn\'t fetch ticket %s after 5 retries', $ticketId));
                }
            } else {
                var_dump('error fetching ticket from kayako ' . $ticketId);
                var_dump($e->getMessage());
                die();
            }
        }

        return $ticket;
    }

    /**
     * @return \kyResultSet
     */
    public function getStatusList()
    {
        return \kyTicketStatus::getAll();
    }
}