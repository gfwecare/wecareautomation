<?php

namespace WeCare\Ticket\Service;

use Laminas\Mail\Transport\TransportInterface;
use Laminas\Mail\Message;
use Laminas\Mime\Message as MimeMessage;
use Laminas\Mime\Part;
use Laminas\Mime\Mime;
use \Monolog\Logger;

class Mailer
{
    const FROM = 'errorReporting+noreply@wecare.rs';


    /**
     * @var TransportInterface
     */
    private $mail;

    /**
     * @var Logger
     */
    private $log;

    public function __construct(TransportInterface $mail, Logger $log)
    {
        $this->mail = $mail;
        $this->log = $log;
    }

    public function handleApplicationError(array $record)
    {
        $message = new Message();
        $message
            ->addTo('djavolak@mail.ru')
            ->setFrom(static::FROM)
            ->setSubject('WeCare Application error !')
            ->setBody(print_r($record, true));
        $this->send($message);
    }

    public function sendStatusDatesReport($logData, $prependBody = '', $subject)
    {
        $mimeMessage = new MimeMessage();
        $text = new Part();
        $text->type = Mime::TYPE_TEXT;
        $text->charset = 'utf-8';
        $text->setContent($prependBody);
        $mimeMessage->addPart($text);

        $general = 'Multiple status change:' . PHP_EOL;
        if (@count($logData['multipleStatuses'])) {
            foreach ($logData['multipleStatuses'] as $status => $ids) {
                if (@count($ids)) {
                    $general .= PHP_EOL . $status . PHP_EOL;
                    foreach ($ids as $id) {
                        $general .= $id . PHP_EOL;
                    }
                }
            }
            $mimeMessage->addPart($this->createAttachment($general, 'Multiple status change tickets.txt'));
        }

        $general = 'Successful tickets:' . PHP_EOL;
        if (@count($logData['success'])) {
            foreach ($logData['success'] as $id) {
                $general .= PHP_EOL . 'success' . PHP_EOL;
                if (is_array($id)) {
                    foreach ($id as $value) {
                        $general .= $value . PHP_EOL;
                    }
                } else {
                    $general .= $id . PHP_EOL;
                }
            }
            $mimeMessage->addPart($this->createAttachment($general, 'Successful tickets.txt'));
        }

        $general = 'Errors:' . PHP_EOL;
        if (@count($logData['error'])) {
            foreach ($logData['error'] as $id) {
                $general .= PHP_EOL . 'Errors' . PHP_EOL;
                if (is_array($id)) {
                    foreach ($id as $value) {
                        $general .= $value . PHP_EOL;
                    }
                } else {
                    $general .= $id . PHP_EOL;
                }
            }
            $mimeMessage->addPart($this->createAttachment($general, 'Errors.txt'));
        }

        $message = new Message();
        $message
//            ->addTo('djavolak@mail.ru')
            ->setFrom(static::FROM)
            ->setSubject($subject)
            ->setBody($mimeMessage);
//        $this->send($message);
    }

    public function sendFixDateLogReport($logData, $prependBody = '')
    {
        $mimeMessage = new MimeMessage();
        $text = new Part();
        $text->type = Mime::TYPE_TEXT;
        $text->charset = 'utf-8';
        $text->setContent($prependBody);
        $mimeMessage->addPart($text);
        $subject = 'Fix date and status for tickets sent to MPO report: ';

        $errors = 'Failed tickets:' . PHP_EOL;
        if (@count($logData['errors'])) {
            foreach ($logData['errors'] as $msg) {
                $errors .= $msg . PHP_EOL;
            }
            $mimeMessage->addPart($this->createAttachment($errors, 'Failed tickets.txt'));
        }

        $updated = 'Date updated for tickets:' . PHP_EOL;
        if (@count($logData['date'])) {
            foreach ($logData['date'] as $msg) {
                $updated .= $msg . PHP_EOL;
            }
            $mimeMessage->addPart($this->createAttachment($updated, 'Tickets - dates.txt'));
        }

        $updated = 'Updated tickets:' . PHP_EOL;
        if (@count($logData['success'])) {
            foreach ($logData['success'] as $msg) {
                $updated .= $msg . PHP_EOL;
            }
            $mimeMessage->addPart($this->createAttachment($updated, 'Tickets - status & dates updated.txt'));
        }

        $cancelled = 'Status updated for tikets:' . PHP_EOL;
        if (@count($logData['status'])) {
            foreach ($logData['status'] as $msg) {
                $cancelled .= $msg . PHP_EOL;
            }
            $mimeMessage->addPart($this->createAttachment($cancelled, 'Tickets - status.txt'));
        }

        $general = 'Tickets with no shipmentid yet:' . PHP_EOL;
        if (@count($logData['general'])) {
            foreach ($logData['general'] as $msg) {
                $general .= $msg . PHP_EOL;
            }
            $mimeMessage->addPart($this->createAttachment($general, 'No shipmentid tickets.txt'));
        }

        $message = new Message();
        $message
//            ->addTo('djavolak@mail.ru')
            ->setFrom(static::FROM)
            ->setSubject($subject)
            ->setBody($mimeMessage);
//        $this->send($message);
    }

    public function sendLogReport($logData, $prependBody = '', $action = 'fromService')
    {
        $mimeMessage = new MimeMessage();
        $text = new Part();
        $text->type = Mime::TYPE_TEXT;
        $text->charset = 'utf-8';
        $text->setContent($prependBody);
        $mimeMessage->addPart($text);
        $direction = '';
        if ($action === 'fromService') {
            $subject = 'Tickets status sync process reports: ';
            $direction = 'smer ka maloprodajama';
        } elseif ($action === 'toService') {
            $subject = 'Tickets status sync process reports: ';
            $direction = 'smer ka Care&Repair';
        } elseif ($action === 'ES') {
            $subject = 'Tickets ES write process report: ';
        } elseif ($action === 'CF') {
            $subject = 'Tickets CF write process report: ';
        }
        $subject .= $direction;

        $errors = 'Failed tickets:' . PHP_EOL;
        if (@count($logData['errors'])) {
            foreach ($logData['errors'] as $msg) {
                $errors .= $msg . PHP_EOL;
            }
            $mimeMessage->addPart($this->createAttachment($errors, 'Failed tickets.txt'));
        }

        $updated = 'Successful tickets:' . PHP_EOL;
        if (@count($logData['success'])) {
            foreach ($logData['success'] as $msg) {
                $updated .= $msg . PHP_EOL;
            }
            $mimeMessage->addPart($this->createAttachment($updated, 'Successful tickets.txt'));
        }

        $cancelled = 'Nepreuzeti tiketi:' . PHP_EOL;
        if (@count($logData['cancelled'])) {
            foreach ($logData['cancelled'] as $msg) {
                $cancelled .= $msg . PHP_EOL;
            }
            $mimeMessage->addPart($this->createAttachment($cancelled, 'Nepreuzeti tiketi.txt'));
        }

        $general = 'General notify tickets:' . PHP_EOL;
        if (@count($logData['notify'])) {
            foreach ($logData['notify'] as $msg) {
                $general .= $msg . PHP_EOL;
            }
            $mimeMessage->addPart($this->createAttachment($general, 'General notify tickets.txt'));
        }

        $message = new Message();
        $message
//            ->addTo('failreports@wecare.rs')
//            ->addTo('nemanja.djajic@wecare.rs')
//            ->addTo('djavolak@mail.ru')
            ->setFrom(static::FROM)
            ->setSubject($subject)
            ->setBody($mimeMessage);
//        if ($action === 'CF') {
//            $message->addTo('djavolak@mail.ru');
//        }
//        $this->send($message);
    }

    public function sendShipmentReports($logs)
    {
        $mimeMessage = new MimeMessage();
        $text = new Part();
        $text->type = Mime::TYPE_TEXT;
        $text->charset = 'utf-8';
        $text->setContent('This mail contains reports that courier requires @ pickup.');
        $mimeMessage->addPart($text);
        // TODO add all statuses even if empty

        foreach ($logs as $log) {
            if (isset($log['labels'])) {
                $mimeMessage->addPart($this->createAttachmentFromFile($log['labels']));
            }
            if (isset($log['reports'])) {
                $mimeMessage->addPart($this->createAttachmentFromFile($log['reports']));
            }
        }
        $message = new Message();
        $message
//            ->addTo('nalepnice@wecare.rs')
            ->addTo('servis@tehnomanija.rs')
            ->addTo('sasa.milosevic@wecare.rs')
            ->addTo('vladimir.jankovic@wecare.rs')
            ->setFrom(static::FROM)
            ->setSubject('Automatic shipment creation report - city express')
            ->setBody($mimeMessage);
        $this->send($message);
    }

    private function send(Message $message)
    {
        try {
            $this->mail->send($message);
            echo 'mail sent';
        } catch (\Exception $e) {
            $this->log->log(
                \Monolog\Logger::ERROR,
                sprintf('Could not send report mail for %s, reason: %s', $message->getSubject(), $e->getMessage())
            );
        }
    }

    private function createAttachment($text, $name)
    {
        $attachment = new Part($text);
        $attachment->type = Mime::TYPE_TEXT;
        $attachment->filename = $name;
        $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;
        return $attachment;
    }

    private function createAttachmentFromFile($path)
    {
        $attachment = new Part(fopen($path, 'r'));
        $attachment->type = Mime::TYPE_OCTETSTREAM;
        $attachment->filename = basename($path);
        $attachment->encoding = Mime::ENCODING_BASE64;
        $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;
        return $attachment;
    }
}