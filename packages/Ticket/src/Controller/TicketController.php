<?php
declare(strict_types=1);

namespace WeCare\Ticket\Controller;

use Laminas\Validator\File\Count;
use WeCare\CityExpress\Service\ApiKeyParser;
use WeCare\ExternalService\Model\ExternalService;
use WeCare\Ticket\Service\Ticket as TicketService;
use WeCare\Ticket\Model\Ticket;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Controller\Controller;
use WeCare\CityExpress\Service\Api as City;
//use Psr\Log\LoggerInterface as Logger;
use Monolog\Logger;
use WeCare\Ticket\Service\Mailer;
use WeCare\User\Repository\UserRepository as UserRepo;
use WeCare\Ticket\Service\XlsParser;
use WeCare\ExternalService\Repository\ExternalServiceRepository;

class TicketController extends Controller
{
    /**
     * @var TicketService
     */
    private $ticketService;

    /**
     * @var City
     */
    private $cityService;

    private $logger;

    private $timer;

    private $esRepo;

    /**
     * @var Mailer
     */
    private $mail;

    private $httpClient;

    private $xlsParser;

    /**
     * LoginController constructor.
     *
     * @param UserRepo $userRepo
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param \Twig\Environment $twig
     * @param User $validator
     */
    public function __construct(
        TicketService $ticketService, City $cityService, Session $session, Config $config, Flash $flash,
        \Twig\Environment $twig, Logger $logger, Mailer $mail, \GuzzleHttp\Client $httpClient, XlsParser $xlsParser,
        ExternalServiceRepository $esRepo)
    {
        parent::__construct($twig, $config, $session, $flash);

        $this->ticketService = $ticketService;
        $this->cityService = $cityService;
        $this->logger = $logger;
        $this->xlsParser = $xlsParser;
        $this->mail = $mail;
        $this->timer = microtime(true);
        $this->httpClient = $httpClient;
        $this->esRepo = $esRepo;
    }

    public function saveBarcodeFromMpo()
    {
        $departments = [Ticket::DEPARTMENT_RETAIL];
        $statuses = [Ticket::STATUS_READY_SEND_TO_SERVICE];

        return $this->saveCityBarcodes(\kyTicket::getAll($departments, $statuses));
    }

    public function testBarcode()
    {
        $departments = [Ticket::DEPARTMENT_RETAIL];
        $statuses = [Ticket::STATUS_MPO_SENT_TO_SERVICE];
//        $tickets = \kyTicket::getAll($departments, $statuses, [], [], 50, 1);
        //62003762
        $tickets = [\kyTicket::get(1210035510)];

        return $this->saveCityBarcodes($tickets);
    }

    private function saveCityBarcodes($tickets)
    {
        $counter = 0;
        $mpoList = $this->xlsParser->getMpoList();
        $apiKeyParser = new ApiKeyParser($this->getConfig()->get('services')->city->apiKeys);
        $data = [];
        $this->logger->log(Logger::INFO, 'updating barcodes, total candidates: ' . count($tickets));
        foreach ($tickets as $ticket) {
            $ticket = new \WeCare\Ticket\Entity\Ticket($ticket, $this->httpClient, $this->cityService, $this->logger, $this->getConfig());
            try {
                // random error appearing from api, incomplete ticket ? appears to be ok on the next loop
                if (null === $ticket->getFieldValue(Ticket::FIELD_RECEIVING_LOCATION)) {
//                if (null === $ticket->getField(Ticket::FIELD_RECEIVING_LOCATION)) {
                    continue;
                }
                // prijemno mesto reklamacija
                $ticketMpoId = $ticket->getFieldValue(Ticket::FIELD_RECEIVING_LOCATION);
//                $ticketMpoName = $ticket->getField(Ticket::FIELD_RECEIVING_LOCATION)->getRawValue();
                $key = array_search($ticketMpoId, array_column($mpoList, 'apiId'));
                // look for existing locations only
                if (false === $key) {
                    continue;
                }
                $apiKey = $apiKeyParser($ticket->getType()->getTitle());
                $barcodes = $this->ticketService->saveCityBarcodeToTicket($ticket, $apiKey);
                if ($barcodes['receivedBarcode'] || $barcodes['sentBarcode']) {
                    $data[] = [
                        'ticketId' => $ticket->getId(),
                        'barcodes' => $barcodes,
                    ];
//                    var_dump($data[count($data)-1]);
//                    die();
                    $counter++;
                }
            } catch (\Exception $e) {
                $error = sprintf('Could not save data for ticket %s, continuing operation. %s', $ticket->getId(), $e->getMessage());
                $this->logger->log(Logger::INFO, $error);
                continue;
            }
        }
        $timeEnd = microtime(true);
        $msg = 'Shipments updated. Time took: ' . ($timeEnd - $this->timer) / 60 . PHP_EOL;
        $msg .= ' Total shipments updated: ' . $counter;
        foreach ($data as $datum) {
            $msg .= $datum['ticketId'] .','. $datum['barcodes']['receivedBarcode']['code'] .','. $datum['barcodes']['sentBarcode']['date'] . PHP_EOL;
//            echo $datum['ticketId'] .','. $datum['barcodes']['receivedBarcode'] .','. $datum['barcodes']['sentBarcode'] . PHP_EOL;
        }
        $this->logger->log(Logger::INFO, $msg);
        $this->getResponse()->getBody()->write($msg);

        return $this->getResponse();
    }

    /**
     * Create shipments from Logistic department (back) to Retail (end the loop)
     * need to print reports when process is done
     */
    public function createShipmentsForMpo()
    {
        $departments = [Ticket::DEPARTMENT_LOGISTIC_DEVICE];
        $statuses = [Ticket::STATUS_READY_FOR_MPO];
        $tickets = \kyTicket::getAll($departments, $statuses);

        $response = $this->createShipments($tickets);
        $this->saveCityBarcodes($tickets);

        return $response;
    }

    /**
     * Create shipments from Retail department to Logistic (start the loop)
     */
    public function createShipmentsFromMpo()
    {
        $departments = [Ticket::DEPARTMENT_RETAIL];
        $statuses = [Ticket::STATUS_READY_SEND_TO_SERVICE];
        $tickets = \kyTicket::getAll($departments, $statuses);
        $response = $this->createShipments($tickets);
        $this->saveCityBarcodes($tickets);

        return $response;
    }

    public function checkEs()
    {
        $ticket = \kyTicket::get(1210126504);
        $es = $this->getExternalService($ticket);
        var_dump($es);
    }

    public function shipmentsTest()
    {
        ini_set('display_errors', '1');
        error_reporting(E_ALL);
        $departments = [Ticket::DEPARTMENT_RETAIL];
        $statuses = [Ticket::STATUS_READY_SEND_TO_SERVICE];

        /* @var \kyResultSet $tickets */
//        $tickets = \kyTicket::getAll($departments, $statuses, [], [], 20, 1);
//        $tickets = \kyTicket::getAll($departments, $statuses);
        $tickets = [\kyTicket::get(1210025852)];

        die('evo me');

        return $this->createShipments($tickets);
    }

    public function writeStatusUpdateToCustomField()
    {
        ini_set('display_errors', '1');
        error_reporting(E_ALL);

//        $messages = [];

        $id = (int) @$_GET['id'];
        $cfId = (int) @$_GET['cfId'];
        $cfValue = @$_GET['cfValue'];
        $return = '0';

        if (!$id) {
            $this->getResponse()->getBody()->write($return);
            $this->getResponse()->getBody()->rewind();

            return $this->getResponse();
        }
        try {
            $tickets = [\kyTicket::get($id)];
        } catch (\Exception $e) {
//            $messages['errors'][] = $e->getMessage();
            $this->getResponse()->getBody()->write($return);
            $this->getResponse()->getBody()->rewind();

            return $this->getResponse();
        }

        /** @var \kyTicket $ticket */
        foreach ($tickets as $ticket) {
            try {
                $ticket = new \WeCare\Ticket\Entity\Ticket($ticket, $this->httpClient, $this->cityService, $this->logger, $this->getConfig());
                $ticket->setFieldValue($cfId, $cfValue, '');
                $msg = 'Saved CF for ticket ' . $ticket->getId();
                $this->logger->log(Logger::INFO, $msg);
//                $messages['success'][] = $msg;
                $return = '1';
            } catch (\Exception $e) {
                $error = sprintf('Could not fetch data for ticket %s, reason given: %s, continuing operation.', $ticket->getId(), $e->getMessage());
//                $messages['errors'][] = $error;
                $this->logger->log(Logger::INFO, $error);
                continue;
            }
        }
        $this->getResponse()->getBody()->write($return);
        $this->getResponse()->getBody()->rewind();

        return $this->getResponse();
    }

    public function writeServiceDataToTicket()
    {
        ini_set('display_errors', '1');
        error_reporting(E_ALL);

        $messages = [];

        $id = (int) @$_GET['id'];
        if (!$id) {
            return $this->getResponse();
        }
        try {
            $tickets = [\kyTicket::get($id)];
        } catch (\Exception $e) {
            $messages['errors'][] = $e->getMessage();
            return $this->getResponse();
        }

        /** @var \kyTicket $ticket */
        foreach ($tickets as $ticket) {
            try {
                $externalService = $this->getExternalService($ticket);
                if (!$externalService) {
                    $error = sprintf('External service not found for Ticket %s with type %s', $ticket->getId(), $ticket->getType()->getTitle());
                    $messages['errors'][] = $error;
                    $this->logger->log(Logger::INFO, $error);
                    continue;
                }
                $ticket = new \WeCare\Ticket\Entity\Ticket($ticket, $this->httpClient, $this->cityService, $this->logger, $this->getConfig());
                $ticket->setFieldValue(Ticket::FIELD_EXTERNAL_SERVICE, $externalService->getKayakoId(), '');
                $this->logger->log(Logger::INFO, 'Saved ES for ticket ' . $ticket->getId());
            } catch (\Exception $e) {
                $error = sprintf('Could not fetch data for ticket %s, reason given: %s, continuing operation.', $ticket->getId(), $e->getMessage());
                $messages['errors'][] = $error;
                $this->logger->log(Logger::INFO, $error);
                continue;
            }
        }
        if (count($messages)) {
            $mailBody = 'External Service write to ticket failed:';
            $this->mail->sendLogReport($messages, $mailBody, 'ES');
        }

        return $this->getResponse();
    }

    /**
     * @param \kyTicket $ticket
     * @return ExternalService
     * @throws \Exception
     * @throws \Skeletor\Mapper\NotFoundException
     */
    private function getExternalService(\kyTicket $ticket)
    {
        try {
            $manufacturer = $ticket->getCustomField('zcut43ahiib9')->getRawValue();
        } catch (\Exception $e) {
            throw new \Exception(sprintf('Could not retrieve custom field for ES for ticket %s, reason given %s.',
                $ticket->getId(), $e->getMessage()
            ));
        }
        $typeId = $ticket->getType()->getId();
        $es = $this->esRepo->findEsForFixedRule($manufacturer, $typeId);
        if (!count($es)) {
            $es = $this->esRepo->findEsForGeneralRule($manufacturer, $typeId);
            if (!count($es)) {
                $msg = sprintf('no rules defined for %s and %s.', $manufacturer, $ticket->getType()->getTitle());
                throw new \Exception($msg);
            }
        }
        if (count($es) > 1) {
            throw new \Exception('more than one rule detected: ' . print_r($es, true));
        }

        return $this->esRepo->getById($es[0]['externalServiceId']);
    }

//    private function createShipments(\kyResultSet $tickets)
    private function createShipments($tickets)
    {
        $mpoList = $this->xlsParser->getMpoList();
        $apiKeyParser = new ApiKeyParser($this->getConfig()->get('services')->city->apiKeys);
        $counter = 0;
        $this->logger->log(Logger::INFO, 'checking tickets for creating shipments, total: ' . count($tickets));
        /** @var \kyTicket $kyTicket */
        foreach ($tickets as $kyTicket) {
            try {
                //can sometimes fail, but it is handy to fetch this data like this
                $ref3CustomField = $kyTicket->getCustomField('zcut43ahiib9');
                if (!$ref3CustomField) {
                    continue;
                }
                $manufacturer = $ref3CustomField->getRawValue(); // ref3
                $ticket = new \WeCare\Ticket\Entity\Ticket($kyTicket, $this->httpClient, $this->cityService, $this->logger, $this->getConfig());
                // prijemno mesto reklamacije
                $ticketMpoId = $ticket->getFieldValue(Ticket::FIELD_RECEIVING_LOCATION);
                $key = array_search($ticketMpoId, array_column($mpoList, 'apiId'));
                // look for existing locations only
                if (false === $key) {
                    continue;
                }
                $mpoData = $mpoList[$key];
                $apiKey = $apiKeyParser($ticket->getType()->getTitle());
            } catch (\Exception $e) {
                $error = sprintf('Could not fetch data for ticket %s because of: %s, continuing operation.', $ticket->getId(), $e->getMessage());
                $this->logger->log(Logger::INFO, $error);
                continue;
            }

            try {
                if ($ticket->getStatus()->getId() === Ticket::STATUS_READY_SEND_TO_SERVICE) {
                    if (strlen($ticket->getFieldValue(Ticket::FIELD_RECEIVED_SHIPMENT_CODE)) > 0) {
                        continue;
                    }
                    // make sure ticket does not have a shipment headed that way already
                    $receivedShipmentId = $ticket->getFieldValue(Ticket::FIELD_RECEIVED_SHIPMENT_ID);
                    if ($receivedShipmentId && strlen($receivedShipmentId) > 0) {
                        continue;
                    }
                    // check if rules are existing, this will trigger errors if not
                    $es = $this->getExternalService($kyTicket);
                    $kayakoEsId = $ticket->getFieldValue(Ticket::FIELD_EXTERNAL_SERVICE);
                    // fetch ES via kayako field (which should be written via cron trigger)
                    if ($kayakoEsId === '6335' || $kayakoEsId != $es->getKayakoId()) {
                        $this->logger->log(Logger::INFO, sprintf('No ES is saved yet to ticket %s.', $ticket->getId()));
                        continue;
                    }
                    $externalService = $this->esRepo->getByKayakoId($kayakoEsId)->toArray();
                    $counter++;
                    $shipmentId = $this->ticketService->sendShipmentToServiceDepartment($ticket, $mpoData, $apiKey, $manufacturer, $externalService);
                    $this->logger->log(Logger::INFO, sprintf(
                        'Ticket %s with apiKey %s and shipmentId %s sent FROM %s TO %s',
                        $ticket->getId(), $apiKey, $shipmentId, $mpoData['name'], $externalService['name'])
                    );
                } elseif ($ticket->getStatus()->getId() === Ticket::STATUS_READY_FOR_MPO) {
                    if (strlen($ticket->getFieldValue(Ticket::FIELD_SENT_SHIPMENT_CODE)) > 0) {
                        continue;
                    }
                    $sentShipmentId = $ticket->getFieldValue(Ticket::FIELD_SENT_SHIPMENT_ID);
                    if ($sentShipmentId && strlen($sentShipmentId) > 0) {
                        continue;
                    }
                    $counter++;
                    $shipmentId = $this->ticketService->sendShipmentToRetailDepartment($ticket, $mpoData, $apiKey, $manufacturer);
                    $this->logger->log(Logger::INFO, sprintf(
                        'Ticket %s with apiKey %s and shipmentId %s sent TO %s',
                        $ticket->getId(), $apiKey, $shipmentId, $mpoData['name'])
                    );
                }
            } catch (\Exception $e) {
                $error = sprintf('Could not fetch data for ticket %s beacuse of: %s, continuing operation.', $ticket->getId(), $e->getMessage());
                $this->logger->log(Logger::INFO, $error);
                continue;
            }
        }
        $timeEnd = microtime(true);
        $msg = 'Shipments created. Time took: ' . ($timeEnd - $this->timer) / 60 . PHP_EOL;
        $msg .= ' Total shipments created: ' . $counter;
        $this->logger->log(Logger::INFO, $msg);
        $this->getResponse()->getBody()->write($msg);

        return $this->getResponse();
    }

    public function getReportsFromCity()
    {
        $logs = [];
        $this->logger->log(Logger::INFO, 'get reports from city process started.');
        foreach ($this->getConfig()->get('services')->city->apiKeys as $name => $keyData) {
            $key = $keyData->apiKey;
            $this->logger->log(Logger::INFO, sprintf('fetching reports for key %s.', $key));
            $log = array_merge($logs, $this->ticketService->getCityReports($key, $name));
            if (count($log) > 0) {
                $logs[] = $log;
            }
        }
        if (count($logs) > 0) {
            $this->mail->sendShipmentReports($logs);
            $this->logger->log(Logger::INFO, 'get reports from city mails sent.');
        }
        $this->logger->log(Logger::INFO, 'get reports from city process finished.');

        return $this->getResponse();
    }

    /**
     *
     * @return \GuzzleHttp\Psr7\Response
     */
    public function checkBarcodesForShipments()
    {
//        ini_set('memory_limit', '1.4G');
        $apiKeyParser = new ApiKeyParser($this->getConfig()->get('services')->city->apiKeys);
//        $department = Ticket::DEPARTMENT_RETAIL;
//        $status = Ticket::STATUS_READY_SEND_TO_SERVICE;
//        if (date('H') % 2 === 0) {
//            $department = Ticket::DEPARTMENT_LOGISTIC_DEVICE;
//            $status = Ticket::STATUS_READY_FOR_MPO;
//        }
        $tickets = \kyTicket::getAll([Ticket::DEPARTMENT_RETAIL, Ticket::DEPARTMENT_LOGISTIC_DEVICE], [Ticket::STATUS_READY_SEND_TO_SERVICE, Ticket::STATUS_READY_FOR_MPO]);
//        $tickets = \kyTicket::getAll([Ticket::DEPARTMENT_RETAIL], [Ticket::STATUS_READY_SEND_TO_SERVICE]);
//        $tickets = [\kyTicket::get(1210038639)];

        $timeEnd = microtime(true);
        $msg = 'starting shipment barcodes sync operation - fetched %s tickets in %s:';
        $this->logger->log(Logger::INFO, sprintf($msg, count($tickets), ($timeEnd - $this->timer) / 60));
        /* @var \kyTicket $ticket */
        foreach ($tickets as $key => $ticket) {
            try {
                if (null === $ticket->getCustomField('4x30sgxkr2nm')) { // does not have "Prijemno mesto reklamacije" field
                    continue;
                }
                // not qualified for couriers operations
                if (false !== strpos($ticket->getCustomField('4x30sgxkr2nm')->getRawValue(), 'Beograd')) {
                    continue;
                }
                $ticketEntity = new \WeCare\Ticket\Entity\Ticket($ticket, $this->httpClient, $this->cityService, $this->logger, $this->getConfig());
                $apiKey = $apiKeyParser($ticket->getType()->getTitle());
                if ($ticketEntity->getStatus()->getId() === Ticket::STATUS_READY_SEND_TO_SERVICE) {
                    $shipmentCode = $ticketEntity->getFieldValue(Ticket::FIELD_RECEIVED_SHIPMENT_CODE);
                } else if ($ticketEntity->getStatus()->getId() === Ticket::STATUS_READY_FOR_MPO) {
                    $shipmentCode = $ticketEntity->getFieldValue(Ticket::FIELD_SENT_SHIPMENT_CODE);
                }
                // skip operation if already has barcode
                if (strlen($shipmentCode) > 7) {
                    continue;
                }
//                $incidentStart = \DateTime::createFromFormat('Y-m-d', '2021-2-1');
//                $incidentEnd = \DateTime::createFromFormat('Y-m-d', '2021-2-21');
//                $ticketDate = new \DateTime($ticketEntity->getTicket()->getCreationTime());
                // pickupdate, only for problematic period and From mpo direction
//                if ($incidentStart < $ticketDate && $incidentEnd > $ticketDate && $ticketEntity->getStatus()->getId() === Ticket::STATUS_READY_FOR_MPO) {
//                    $status = $this->cityService->getShipmentStatusByRef($ticket->getId(), $apiKey);
//                } else {
                    $status = $ticketEntity->getShipmentStatus($apiKey);
//                }
            } catch (\Exception $e) {
//                var_dump($e->getMessage());
//                continue;
                if ($e->getMessage() === 'No shipmentId saved yet.') {
                    continue;
                }
                $error = $e->getMessage();
                if (false !== strpos($e->getMessage(), 'Shipment not found')) {
                    $error = sprintf('Ticket %s: could not be found @ courier service.', $ticket->getId());
                }
                $this->logger->log(Logger::INFO, $error);
                continue;
            }
//            $ids[$ticket->getId()] = $status->getBarcode();
//            var_dump($shipmentCode);
//            var_dump($ticket->getStatus()->getTitle());
//            var_dump($status);
//            var_dump($status->getBarcode());
//            die();

            if ($status->getBarcode()) {
                try {
                    $ticketEntity->saveBarcode($status);
                } catch (\Exception $e) {
                    $this->logger->log(Logger::INFO, $e->getMessage());
                }
            }
            $ticketEntity = [];
        }
        $timeEnd = microtime(true);
        $msg = 'shipment barcodes sync operation - complete in %s:';
        $this->logger->log(Logger::INFO, sprintf($msg, ($timeEnd - $this->timer) / 60));
        $this->getResponse()->getBody()->write($msg);

        return $this->getResponse();
    }

    public function checkShipmentStatusForMpo()
    {
        $this->timer = microtime(true);
        $departments = [Ticket::DEPARTMENT_LOGISTIC_DEVICE, Ticket::DEPARTMENT_RETAIL];
        $statuses = [Ticket::STATUS_READY_FOR_MPO, Ticket::STATUS_SERVICE_SENT_TO_MPO];
        $tickets = \kyTicket::getAll($departments, $statuses);
//        $tickets = [\kyTicket::get(1210003997)];

        $timeEnd = microtime(true);
        $msg = 'starting shipment status sync operation for statuses [spreman za slanje u mpo i poslat iz servisa u mpo] - fetched %s tickets in %s:';
        $this->logger->log(Logger::INFO, sprintf($msg, count($tickets), ($timeEnd - $this->timer) / 60));
        $this->getResponse()->getBody()->write($this->updateTicketData($tickets));

        return $this->getResponse();
    }

    public function checkShipmentStatusFromMpo()
    {
        $this->timer = microtime(true);
        /* @var \kyResultSet $tickets */
//        $statusesFromMpo = [Ticket::STATUS_READY_SEND_TO_SERVICE, Ticket::STATUS_MPO_SENT_TO_SERVICE, Ticket::STATUS_MPO_RECEIVED];
        $statusesFromMpo = [Ticket::STATUS_READY_SEND_TO_SERVICE, Ticket::STATUS_MPO_SENT_TO_SERVICE];
        $department = [Ticket::DEPARTMENT_RETAIL];
        $tickets = \kyTicket::getAll($department, $statusesFromMpo);

//        $tickets = [\kyTicket::get(1210033151)];
//        $tickets = [\kyTicket::get(1210050014)];

        $timeEnd = microtime(true);
        $msg = 'starting shipment status sync operation for statuses [Primljen u mpo, Poslat iz mpo i Pripremljen za slanje]:  - fetched %s tickets in %s:';
        $this->logger->log(Logger::INFO, sprintf($msg, count($tickets), ($timeEnd - $this->timer) / 60));
        $this->getResponse()->getBody()->write($this->updateTicketData($tickets));

        return $this->getResponse();
    }

    private function updateTicketData($tickets)
    {
        $apiKeyParser = new ApiKeyParser($this->getConfig()->get('services')->city->apiKeys);
        $logData = ['errors' => [], 'success' => [], 'notify' => [], 'cancelled' => []];
        $now = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));
        $statusesFromMpo = [Ticket::STATUS_READY_SEND_TO_SERVICE, Ticket::STATUS_MPO_SENT_TO_SERVICE, Ticket::STATUS_MPO_RECEIVED];
        $statusesForMpo = [Ticket::STATUS_READY_FOR_MPO, Ticket::STATUS_SERVICE_SENT_TO_MPO];
        $totalQualifiedTickets = 0;
        foreach ($tickets as $key => $kyTicket) {
            try {
                $action = 'fromService';
                $ticket = new \WeCare\Ticket\Entity\Ticket($kyTicket, $this->httpClient, $this->cityService, $this->logger, $this->getConfig());
                $apiKey = $apiKeyParser($ticket->getType()->getTitle());
                if (in_array($ticket->getStatus()->getId(), [Ticket::STATUS_READY_SEND_TO_SERVICE, Ticket::STATUS_MPO_SENT_TO_SERVICE, Ticket::STATUS_MPO_RECEIVED])) {
                    $action = 'toService';
                    if (!$ticket->getFieldValue(Ticket::FIELD_RECEIVED_SHIPMENT_ID)) {
                        continue;
                    }
//                    $shipmentId = $ticket->getFieldValue(Ticket::FIELD_RECEIVED_SHIPMENT_ID);
//                    $shipmentCode = $ticket->getFieldValue(Ticket::FIELD_RECEIVED_SHIPMENT_CODE);
                } else if (in_array($ticket->getStatus()->getId(), $statusesForMpo)) {
                    if (!$ticket->getFieldValue(Ticket::FIELD_SENT_SHIPMENT_ID)) {
                        continue;
                    }
//                    $shipmentId = $ticket->getFieldValue(Ticket::FIELD_SENT_SHIPMENT_ID);
//                    $shipmentCode = $ticket->getFieldValue(Ticket::FIELD_SENT_SHIPMENT_CODE);
                } else {
                    continue;
                }
                // @TODO implement this status return when cityService is in test mode
//                    $dt = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));
//                    $status = new \WeCare\CityExpress\Model\StatusListStub(false, true, false, $dt);
                $totalQualifiedTickets++;

//                $status = $this->cityService->getShipmentApiStatus($shipmentId, $apiKey);
//                $status = $this->cityService->getShipmentStatusByRef($ticket->getId(), $apiKey, $shipmentCode);
                $status = $ticket->getShipmentStatus($apiKey);
                if (!$status->hasStatuses()) {
                    $logData['notify'][] = sprintf('Ticket %s has no status list.', $ticket->getId());
//                    continue;
                }
                if ($status->isCancelled()) {
                    $logData['cancelled'][] = sprintf('Ticket %s with barcode %s has been cancelled.', $ticket->getId(), $ticket->getFieldValue(Ticket::FIELD_SENT_SHIPMENT_CODE));
                }
                //log tickets
                if ($status->shouldLog()) {
                    $logData['notify'][] = sprintf('Ticket %s has strange status.', $ticket->getId());
                }
                if ($status->isInTransit()) {
                    if (in_array($ticket->getStatus()->getId(), [Ticket::STATUS_READY_SEND_TO_SERVICE, Ticket::STATUS_MPO_RECEIVED])) {
                        $this->ticketService->packagePickedUpFromMpo($kyTicket);
                        $log = 'Ticket %s: status changed to "Poslat iz MPO u servis" at %s';
                        $this->logger->log(Logger::INFO, sprintf($log, $ticket->getId(), $now->format('m-d-Y')));
                        $logData['success'][] = sprintf($log, $ticket->getId(), $now->format('m-d-Y'));
                    } else if ($ticket->getStatus()->getId() == Ticket::STATUS_READY_FOR_MPO) {
                        $this->ticketService->packagePickedUpFromService($kyTicket);
                        $log = 'Ticket %s: status changed to "Poslat iz servisa u MPO" at %s';
                        $this->logger->log(Logger::INFO, sprintf($log, $ticket->getId(), $now->format('m-d-Y')));
                        $logData['success'][] = sprintf($log, $ticket->getId(), $now->format('m-d-Y'));
                    }
                }

                if ($status->isDelivered()) {
                    $dt = \DateTime::createFromFormat('Y-m-d', $status->getDeliveryDate());
                    $time = $dt->format('d/m/Y');
                    // delivered to service
                    if (in_array($ticket->getStatus()->getId(), $statusesFromMpo)) {
                        $delivered = $ticket->getFieldValue(Ticket::FIELD_DATE_FROM_MPO);
                        if ($delivered && strlen($delivered) > 0) {
                            continue;
                        }
                        $this->ticketService->deliveredToServiceLocation($ticket, $time);
                        $log = sprintf('Ticket %s delivered to service at %s', $ticket->getId(), $time);
                        $this->logger->log(Logger::INFO, $log);
                        $logData['success'][] = $log;
                        continue;
                    }

                    // delivered to mpo
                    if ($ticket->getStatus()->getId() == Ticket::STATUS_SERVICE_SENT_TO_MPO) {
                        $delivered = $ticket->getFieldValue(Ticket::FIELD_DATE_TO_MPO);
                        if ($delivered && strlen($delivered) > 0) {
                            continue;
                        }
                        $this->ticketService->deliveredToMpoLocation($ticket, $time);
                        $log = sprintf('Ticket %s delivered to MPO at %s', $ticket->getId(), $time);
                        $this->logger->log(Logger::INFO, $log);
                        $logData['success'][] = $log;

                        continue;
                    }
                }
            } catch (\Exception $e) {
                if (false !== strpos($e->getMessage(), 'Shipment not found')) {
                    $logData['errors'][] = sprintf('Ticket %s in status %s: could not be found @ courier service.', $ticket->getId(), $ticket->getStatus()->getTitle());
                } else {
                    $logData['errors'][] = sprintf('Ticket %s in status %s: could not be processed because: %s.', $ticket->getId(), $ticket->getStatus()->getTitle(), $e->getMessage());
                }
            }
            continue;
        }
        $timeEnd = microtime(true);
        $returnMessage = sprintf('Shipment status check complete and took %s mins.', ($timeEnd - $this->timer) / 60) . PHP_EOL;
        $returnMessage .= sprintf('Tickets qualified for verification %s out of %s', $totalQualifiedTickets, count($tickets));
        $this->logger->log(Logger::INFO, $returnMessage);

        $mailBody = $returnMessage . PHP_EOL;
        foreach ($logData['errors'] as $msg) {
            $this->logger->log(Logger::INFO, $msg);
        }
        $this->mail->sendLogReport($logData, $mailBody, $action);

        return $returnMessage;
    }

    /**
     * Catches more tickets than 'standard' ticket update process. Performs same actions,
     * and acts as a kind of a garbage collector for manual tickets, etc.
     *
     * @return \GuzzleHttp\Psr7\Response
     */
    public function fixDateAndStatusForTicketsSentToMpo()
    {
        $this->timer = microtime(true);
        /* @var \kyResultSet $tickets */
        $statusesFromMpo = [Ticket::STATUS_SERVICE_SENT_TO_MPO];
        $department = [Ticket::DEPARTMENT_RETAIL];
        $tickets = \kyTicket::getAll($department, $statusesFromMpo);
        $logData = ['errors' => [], 'success' => [], 'notify' => [], 'cancelled' => []];
        $timeEnd = microtime(true);
        $msg = 'starting fixDateAndStatusForTicketsSentToMpo operation for statuses [Poslat u mpo]:  - fetched %s tickets in %s:';
        $this->logger->log(Logger::INFO, sprintf($msg, count($tickets), ($timeEnd - $this->timer) / 60));
        $apiKeyParser = new ApiKeyParser($this->getConfig()->get('services')->city->apiKeys);
        foreach ($tickets as $key => $kyTicket) {
            try {
                $ticket = new \WeCare\Ticket\Entity\Ticket($kyTicket, $this->httpClient, $this->cityService, $this->logger, $this->getConfig());
                $apiKey = $apiKeyParser($ticket->getType()->getTitle());
                $status = $ticket->getShipmentStatus($apiKey);
                if ($status->isDelivered()) {
                    $dt = \DateTime::createFromFormat('Y-m-d', $status->getDeliveryDate());
                    $date = $dt->format('d/m/Y');
                    $delivered = $ticket->getFieldValue(Ticket::FIELD_DATE_TO_MPO);
                    if ($delivered !== $date) {
                        $ticket->setFieldValue(Ticket::FIELD_DATE_TO_MPO, $date, $delivered);
                        $logData['date'][] = $ticket->getId();
//                        $this->logger->debug('updated date for ' . $ticket->getId());
                    }
                    if ($ticket->getStatus() !== Ticket::STATUS_RETURNED_TO_MPO) {
                        $ticket->setStatus(Ticket::STATUS_RETURNED_TO_MPO);
                        $ticket->update();
                        $logData['status'][] = $ticket->getId();
//                        $this->logger->debug('updated status for ' . $ticket->getId());
                    }
                }
            } catch (\Exception $e) {
                if ($e->getMessage() === 'No shipmentId saved yet.') {
                    $logData['general'][] = $ticket->getId();
                } else {
                    $msg = sprintf('There was an error while trying to fix ticket %s date / status: %s', $ticket->getId(), $e->getMessage());
                    $logData['error'][] = $msg;
                    $this->logger->error($msg);
                }

                continue;
            }
        }

        $mailBody = 'Fix dates and statuses for tickets sent to MPO' . PHP_EOL;
        $this->mail->sendFixDateLogReport($logData, $mailBody);
//        $this->getResponse()->getBody()->write('done');

        return $this->getResponse();
    }

    /**
     * TODO on hold, not used yet, seems it does the job
     *
     * @return \GuzzleHttp\Psr7\Response
     */
    public function fixDateAndStatusForManualTicketsSentToES()
    {
        $this->timer = microtime(true);
        /* @var \kyResultSet $tickets */
        $statusesFromMpo = [Ticket::STATUS_IN_TRANSPORT_TO_ES];
        $department = [Ticket::DEPARTMENT_EXTERNAL_SERVICE];
        $tickets = \kyTicket::getAll($department, $statusesFromMpo, [], [], 5, 1);
        $logData = ['errors' => [], 'success' => [], 'notify' => [], 'cancelled' => []];
        $timeEnd = microtime(true);
        $msg = 'starting fixDateAndStatusForTicketsSentToMpo operation for statuses [Poslat u mpo]:  - fetched %s tickets in %s:';
        $this->logger->log(Logger::INFO, sprintf($msg, count($tickets), ($timeEnd - $this->timer) / 60));
        $apiKeyParser = new ApiKeyParser($this->getConfig()->get('services')->city->apiKeys);
        foreach ($tickets as $key => $kyTicket) {
            try {
                $ticket = new \WeCare\Ticket\Entity\Ticket($kyTicket, $this->httpClient, $this->cityService, $this->logger, $this->getConfig());
                $apiKey = $apiKeyParser($ticket->getType()->getTitle());
                $status = $ticket->getShipmentStatus($apiKey);

                var_dump($status);
                var_dump($status->getDeliveryDate());
                die();


                if ($status->isDelivered()) {
                    $dt = \DateTime::createFromFormat('Y-m-d', $status->getDeliveryDate());
                    $date = $dt->format('d/m/Y');
                    $ticket->setFieldValue(Ticket::FIELD_DATE_TO_EXTERNAL_SERVICE, $date, '');
                    $ticket->setStatus(Ticket::STATUS_DEVICE_IN_ES);
                    $ticket->update();

                    var_dump($ticket->getId());
                    die();

                    $logData['success'][] = $ticket->getId();
                }
            } catch (\Exception $e) {
                if ($e->getMessage() === 'No shipmentId saved yet.') {
                    $logData['general'][] = $ticket->getId();
                } else {
                    $msg = sprintf('There was an error while trying to fix ticket %s date / status: %s', $ticket->getId(), $e->getMessage());
                    $logData['error'][] = $msg;
                    $this->logger->error($msg);
                }

                continue;
            }
        }

        $mailBody = 'Fix dates and statuses for tickets sent to MPO' . PHP_EOL;
        $this->mail->sendFixDateLogReport($logData, $mailBody);
//        $this->getResponse()->getBody()->write('done');

        return $this->getResponse();
    }
}
