<?php
declare(strict_types=1);
namespace WeCare\Ticket\Model;


class Ticket
{
    const STATUS_MPO_RECEIVED = 1;
    const STATUS_MPO_SENT_TO_SERVICE = 190;
    const STATUS_READY_FOR_MPO = 252;
    const STATUS_SERVICE_SENT_TO_MPO = 191;
    const STATUS_DELIVERED_TO_SERVICE = 291;
    const STATUS_DEVICE_IN_ES = 214;
    const STATUS_IN_TRANSPORT_TO_ES = 213;
//    const STATUS_READY_SEND_TO_SERVICE = 395; // dev
    const STATUS_READY_SEND_TO_SERVICE = 419; // prod
    const STATUS_RETURNED_TO_MPO = 435;

//    const STATUS_DIAGNOSTIC_COMPLETE_ACCEPTED = 366;

    const DEPARTMENT_RETAIL = 45;
    const DEPARTMENT_LOGISTIC_DEVICE = 65;
    const DEPARTMENT_EXTERNAL_SERVICE = 59;

    const FIELD_DATE_FROM_MPO = 110;
    const FIELD_DATE_TO_MPO = 111;
    const FIELD_RECEIVED_SHIPMENT_CODE = 107;
    const FIELD_SENT_SHIPMENT_CODE = 109;
    const FIELD_SENT_SHIPMENT_CODE_TO_ES = 166;
    const FIELD_RECEIVING_LOCATION = 88;
    const FIELD_RECEIVING_TYPE = 106;  // prijem uredjaja
    const FIELD_SENDING_TYPE = 108;  // isporuka uredjaja
    const FIELD_RECEIVED_SHIPMENT_ID = 188;
    const FIELD_SENT_SHIPMENT_ID = 189;
    const FIELD_MANUFACTURER = 89;
    const FIELD_EXTERNAL_SERVICE = 135;
    const FIELD_DATE_TO_EXTERNAL_SERVICE = 136;

//    const FIELD_DIAGNOSTIC_COMPLETE_ACCEPTED = 252; // dev




//    const FIELD_DATE_FROM_MPO = 'syurmnnuryqi';
//    const FIELD_DATE_TO_MPO = 'u4n3mf4hfyha';
//    const FIELD_RECEIVED_SHIPMENT_CODE = 'ar65fcdxf2am';
//    const FIELD_SENT_SHIPMENT_CODE = 't7d1iu4nb0qu';
//    const FIELD_RECEIVING_LOCATION = '4x30sgxkr2nm';
//    const FIELD_RECEIVING_TYPE = '3qnk0banthtn';  // prijem uredjaja
//    const FIELD_SENDING_TYPE = 'itobqs9j9kh7';  // isporuka uredjaja
//    const FIELD_RECEIVED_SHIPMENT_ID = 'xrce1n4ln0p4';
//    const FIELD_SENT_SHIPMENT_ID = '61ieih29snmu';

}