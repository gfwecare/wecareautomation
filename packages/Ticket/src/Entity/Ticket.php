<?php

namespace WeCare\Ticket\Entity;

use Laminas\Config\Config;
use WeCare\CityExpress\Model\StatusList;
use WeCare\Ticket\Model\Ticket as TicketModel;
use WeCare\CityExpress\Service\Api as City;
use Monolog\Logger;

class Ticket
{

    /**
     * @var \kyTicket
     */
    private $ticket;

    /**
     * @var \GuzzleHttp\Client
     */
    private $httpClient;
    /**
     * @var City
     */
    private $cityService;

    private $logger;

    private $config;

    private $apiUrl;

    public function __construct(
        \kyTicket $ticket, \GuzzleHttp\Client $httpClient, City $cityService, Logger $logger, Config $config
    ) {
        $this->ticket = $ticket;
        $this->httpClient = $httpClient;
        $this->cityService = $cityService;
        $this->logger = $logger;
        $this->config = $config;
        $this->apiUrl = $this->config->services->miniApi;
    }

    public function saveBarcode(StatusList $status)
    {
        if ($this->ticket->getStatus()->getId() === TicketModel::STATUS_READY_SEND_TO_SERVICE) {
            $oldBarcode = $this->getFieldValue(TicketModel::FIELD_RECEIVED_SHIPMENT_CODE);
            if ($status->getBarcode() !== $oldBarcode) {
                $this->setFieldValue(TicketModel::FIELD_RECEIVED_SHIPMENT_CODE, $status->getBarcode(), $oldBarcode);
                $this->logger->log(Logger::INFO, 'updated barcode {from MPO} for ticket ' . $this->ticket->getId());
            }
//                    var_dump($status->getBarcode());
//                    var_dump($ticket->getCustomField('ar65fcdxf2am')->getRawValue());
//                    die();
        }
        if ($this->ticket->getStatus()->getId() === TicketModel::STATUS_READY_FOR_MPO) {
            $oldBarcode = $this->getFieldValue(TicketModel::FIELD_SENT_SHIPMENT_CODE);
            if ($status->getBarcode() !== $oldBarcode && $status->getBarcode() !== $this->getFieldValue(TicketModel::FIELD_RECEIVED_SHIPMENT_CODE)) {
                $this->setFieldValue(TicketModel::FIELD_SENT_SHIPMENT_CODE, $status->getBarcode(), $oldBarcode);
                $this->logger->log(Logger::INFO, 'updated barcode {to MPO} for ticket ' . $this->ticket->getId());
            }
        }
    }

    public function getShipmentStatus($apiKey)
    {
        if (in_array($this->ticket->getStatus()->getId(), [TicketModel::STATUS_READY_SEND_TO_SERVICE, TicketModel::STATUS_MPO_SENT_TO_SERVICE])) {
            $shipmentId = $this->getFieldValue(TicketModel::FIELD_RECEIVED_SHIPMENT_ID);
        } else if (in_array($this->ticket->getStatus()->getId(), [TicketModel::STATUS_READY_FOR_MPO, TicketModel::STATUS_SERVICE_SENT_TO_MPO])) {
            $shipmentId = $this->getFieldValue(TicketModel::FIELD_SENT_SHIPMENT_ID);
        } else {
            $shipmentId = $this->getFieldValue(TicketModel::FIELD_SENT_SHIPMENT_CODE_TO_ES);
            $shipmentId = substr($shipmentId, 6, -2);
        }

        // skip operation if shipment id not saved yet
        if (strlen($shipmentId) === 0) {
            throw new \Exception('No shipmentId saved yet.');
        }
        if (strlen($shipmentId) < 5) {
            throw new \Exception(sprintf('Weird shipmentId detected %s for ticket %s ', $shipmentId, $this->getId()));
        }
//        var_dump('shipmentId: ' . $shipmentId);
//                die();
//                $status = $this->cityService->getShipmentStatusByRef($ticket->getId(), $apiKey);
        return $this->cityService->getShipmentApiStatus($shipmentId, $apiKey);
    }

    public function getTicket()
    {
        return $this->ticket;
    }

    public function setFieldValue($fieldId, $value, $oldValue)
    {
        $url = sprintf($this->apiUrl . '/city/syncRemoteData.php?ticketId=%s&fieldId=%s&customAction=setFieldData&value=%s&departmentId=%s&oldValue=%s',
            $this->ticket->getId(), $fieldId, $value, $this->ticket->getDepartmentId(), $oldValue);
        try {
            $response = $this->httpClient->send(new \GuzzleHttp\Psr7\Request('GET', $url));
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            throw new \Exception('Failed to save custom field data: ' . $e->getMessage());
        }
        $data = json_decode($response->getBody()->getContents());
        if (!$data->status) {
            throw new \Exception('Failed to save custom field data:' . $data->value);
        }

        return $data->status;
    }

    public function getFieldValue($fieldId)
    {
        try {
            $url = $this->apiUrl . '/city/syncRemoteData.php?ticketId=%s&fieldId=%s&customAction=getFieldData';
            $response = $this->httpClient->send(
                new \GuzzleHttp\Psr7\Request('GET', sprintf($url, $this->ticket->getId(), $fieldId))
            );
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            throw new \Exception('Failed to get custom ticket data: ' . $e->getMessage());
        }

        $data = json_decode($response->getBody()->getContents());
        if (!$data->status) {
            throw new \Exception('Failed to get custom ticket data: ' . $data->value);
        }

        return $data->value;
    }

    /**
     * Using kayako api, not function properly
     * @deprecated
     *
     * @param $fieldName
     * @return \kyCustomField
     */
    public function getField($fieldName)
    {
        return $this->ticket->getCustomField($fieldName);
    }

    public function updateFields()
    {
        return $this->ticket->updateCustomFields();
    }

    public function update()
    {
        return $this->ticket->update();
    }

    public function getStatus()
    {
        return $this->ticket->getStatus();
    }

    public function setStatus($statusId)
    {
        return $this->ticket->setStatusId($statusId);
    }

    public function setDepartment($departmentId)
    {
        return $this->ticket->setDepartmentId($departmentId);
    }

    public function getType()
    {
        return $this->ticket->getType();
    }

    public function getId()
    {
        return $this->ticket->getId();
    }
}