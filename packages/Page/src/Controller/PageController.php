<?php
declare(strict_types=1);
namespace WeCare\Page\Controller;

use GuzzleHttp\Psr7\Response;
use WeCare\Page\Repository\PageRepository as PageRepo;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Controller\Controller;

class PageController extends Controller
{
    /**
     * @var PageRepo
     */
    private $pageRepo;

    /**
     * PageController constructor.
     * @param PageRepo $pageRepo
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param \Twig\Environment $twig
     */
    public function __construct(PageRepo $pageRepo, Session $session, Config $config, Flash $flash, \Twig\Environment $twig)
    {
        parent::__construct($twig, $config, $session, $flash);

        $this->pageRepo = $pageRepo;
    }

    /**
     * User list & view
     *
     * @return Response
     */
    public function view(): Response
    {
        $pages = $this->pageRepo->fetchAll();
        $this->setGlobalVariable('pageTitle', 'View Pages');
        return $this->respond('view', ['pages' => $pages]);
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('pageId');
        $page = false;
        $this->setGlobalVariable('pageTitle', 'Create page');
        if ($id) {
            $page = $this->pageRepo->getById($id);
            $this->setGlobalVariable('pageTitle', 'Edit page: ' . $page->getTitle());
        }
        return $this->respond('form', ['page' => $page]);
    }

    /**
     * @return Response
     */
    public function update(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        $data['pageId'] = $this->getRequest()->getAttribute('pageId');
        //@TODO validate data
        try {
            $this->pageRepo->update($data);
            $this->getFlash()->success('Page successfully updated');
        } catch (\Exception $e) {
            $this->getFlash()->error($e->getMessage());
            return $this->redirect('/admin/page/view/');
        }
        return $this->redirect('/admin/page/view/'.$data['pageId'].'/');
    }

    /**
     * @return Response
     */
    public function create(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        try {
            $this->pageRepo->create($data);
            $this->getFlash()->success('Page successfully created');
        } catch (\Exception $e) {
            $this->getFlash()->error($e->getMessage());
        }
        return $this->redirect('/admin/page/view/');
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function delete(): Response
    {
        $page = false;
        $id = $this->getRequest()->getAttribute('pageId');
        if (!empty($id)) {
            $page = $this->pageRepo->getById($id);
        }
        try {
            $this->pageRepo->delete($page);
            $this->getFlash()->success('Page successfully deleted');
        } catch (\Exception $e) {
            $this->getFlash()->error($e->getMessage());
            return $this->redirect('/admin/page/view/');
        }
        return $this->redirect('/admin/page/view/');
    }
}