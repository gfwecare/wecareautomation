<?php
declare(strict_types=1);

namespace WeCare\Page\Action;

use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;
use \Twig\Environment;
use GuzzleHttp\Psr7\Response;

class BaseAction
{
    /**
     * @var \Twig\Environment
     */
    private $template;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var Config
     */
    private $config;

    /**
     * ParseReportAction constructor.
     * @param Logger $logger
     * @param Config $config
     */
    public function __construct(
        Logger $logger, Config $config, Environment $template
    ) {
        $this->logger = $logger;
        $this->config = $config;
        $this->template = $template;
        $this->response = new Response();
    }

    public function respond($template, $data = []) : Response
    {
        $className = explode('\\', get_class($this));
        $action = strtolower(str_replace('Action', '', $className[count($className) - 1]));
        $template = '/'. $action .'/'. $template . '.twig';
        $this->response->getBody()->write($this->template->render($template, ['data' => $data]));
        $this->response->getBody()->rewind();

        return $this->response;
    }

}