<?php
declare(strict_types=1);

namespace WeCare\Page\Action;

use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;
use \Twig\Environment;
use WeCare\Page\Repository\PageRepository;

class PageAction extends BaseAction
{
    private $repository;

    /**
     * PageAction constructor.
     * @param Logger $logger
     * @param Config $config
     */
    public function __construct(
        Logger $logger, Config $config, Environment $template, PageRepository $repository
    ) {
        parent::__construct($logger, $config, $template);
        $this->repository = $repository;
    }

    /**
     * Parses data for provided merchantId
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request request
     * @param \Psr\Http\Message\ResponseInterface $response response
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function __invoke(
        \Psr\Http\Message\ServerRequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response
    ) {
        try {
            $page = $this->repository->getBySlug($request->getAttribute('slug'));
        } catch (\Exception $e) {
            // @TODO render 404 template
            echo '404';
            $page = false;
        }


        return $this->respond('page', ['page' => $page]);
    }
}