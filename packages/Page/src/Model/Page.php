<?php
declare(strict_types = 1);
namespace WeCare\Page\Model;

use Skeletor\Model\Model;
use Skeletor\Acl\AclInterface;


/**
 * Class User.
 * Base user model.
 *
 * @package SNF\User\Model
 */
class Page extends Model
{
    private $pageId;

    private $title;

    private $slug;

    private $content;

    private $imageSrc;

    private $isActive;

    /**
     * Page constructor.
     * @param $pageId
     * @param $title
     * @param $content
     * @param $imageSrc
     * @param $isActive
     */
    public function __construct($pageId, $title, $content, $imageSrc, $isActive, $createdAt, $updatedAt, $slug)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->pageId = $pageId;
        $this->title = $title;
        $this->slug = $slug;
        $this->content = $content;
        $this->imageSrc = $imageSrc;
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->pageId;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return mixed
     */
    public function getImageSrc()
    {
        return $this->imageSrc;
    }

    /**
     * @return mixed
     */
    public function isActive()
    {
        return $this->isActive;
    }
}
