<?php
declare(strict_types=1);

namespace WeCare\Page\Repository;

use Skeletor\Mapper\NotFoundException;
use WeCare\Page\Mapper\Page as Mapper;
use WeCare\Page\Model\Page;
use Laminas\Config\Config;

/**
 * Class PageRepository.
 *
 */
class PageRepository
{
    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * @var \DateTime
     */
    private $dt;

    /**
     * @var Config
     */
    private $config;

    /**
     * PageRepository constructor.
     *
     * @param Mapper $mapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(Mapper $mapper, \DateTime $dt, Config $config)
    {
        $this->mapper = $mapper;
        $this->dt = $dt;
        $this->config = $config;
    }

    /**
     * Fetches a list of UserEntity models.
     *
     * @param array $params
     *
     * @return array
     */
    public function fetchAll($params = array()): array
    {
        $pages = [];
        foreach ($this->mapper->fetchAll($params) as $data) {
            $pages[] = $this->make($data);
        }

        return $pages;
    }

    /**
     * @param $id
     * @return Page
     * @throws NotFoundException
     */
    public function getById($id): Page
    {
        return $this->make($this->mapper->fetchById($id));
    }

    /**
     * @param $slug
     *
     * @return Page
     * @throws \Exception
     */
    public function getBySlug($slug): Page
    {
        if ($slug === '') {
            throw new \InvalidArgumentException('Wrong param provided. ');
        }

        $data = $this->mapper->fetchAll(['slug' => $slug], 1);
        if (empty($data)) {
            throw new \Exception('Page entity not found. ' . $slug);
        }

        return $this->make($data[0]);
    }

    /**
     * Persists page model.
     *
     * @param array $data
     *
     * @return int
     * @throws \Exception
     */
    public function create($data): int
    {
        return $this->mapper->insert([
            'title' => $data['title'],
            'slug' => $data['slug'],
            'content' => $data['content'],
            'imageSrc' => (isset($data['imageSrc'])) ? $data['imageSrc'] : '',
            'isActive' => $data['isActive']
        ]);
    }

    /**
     * Updates page model.
     *
     * @param $data
     * @return Page
     * @throws \Exception
     *
     */
    public function update($data): Page
    {
        return $this->getById($this->mapper->update([
            'pageId' => $data['pageId'],
            'title' => $data['title'],
            'slug' => $data['slug'],
            'content' => $data['content'],
            'imageSrc' => (isset($data['imageSrc'])) ? $data['imageSrc'] : '',
            'isActive' => $data['isActive']
        ]));
    }

    /**
     * Factory method
     *
     * @param $userData
     * @return User
     */
    private function make($pageData): Page
    {
        $data = [];
        foreach ($pageData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }

        return new Page(
            $data['pageId'],
            $data['title'],
            $data['content'],
            $data['imageSrc'],
            (int) $data['isActive'],
            $data['createdAt'],
            $data['updatedAt'],
            $data['slug']
        );
    }

    /**
     * Deletes a single page entity.
     *
     * @param Page $page
     *
     * @return bool
     */
    public function delete(Page $page): bool
    {
        return $this->mapper->delete($page->getId());
    }

    public function slugExists($slug): bool
    {
        try {
            $this->getBySlug($slug);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }
}
