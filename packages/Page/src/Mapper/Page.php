<?php
declare(strict_types = 1);
namespace WeCare\Page\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;

class Page extends MysqlCrudMapper
{
    public function __construct(\PDO $pdo)
    {
        parent::__construct($pdo, 'page', 'pageId');
    }
}