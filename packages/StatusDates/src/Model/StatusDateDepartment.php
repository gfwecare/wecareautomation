<?php
declare(strict_types=1);
namespace WeCare\StatusDates\Model;

use Skeletor\Model\Model;

class StatusDateDepartment extends Model
{
    private $statusDatesDepartmentsId;

    private $departmentId;

    private $department;

    public function __construct($statusDatesDepartmentsId, $departmentId, $department, $createdAt, $updatedAt)
    {
        $this->statusDatesDepartmentsId = $statusDatesDepartmentsId;
        $this->departmentId = $departmentId;
        $this->department = $department;

        parent::__construct($createdAt, $updatedAt);
    }

    public function getId()
    {
        return (int) $this->statusDatesDepartmentsId;
    }

    /**
     * @return mixed
     */
    public function getStatusDatesDepartmentsId()
    {
        return (int) $this->statusDatesDepartmentsId;
    }

    /**
     * @return mixed
     */
    public function getDepartmentId()
    {
        return (int) $this->departmentId;
    }

    /**
     * @return mixed
     */
    public function getDepartment()
    {
        return $this->department;
    }
}