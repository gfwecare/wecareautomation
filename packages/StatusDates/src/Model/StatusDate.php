<?php
declare(strict_types=1);
namespace WeCare\StatusDates\Model;

use Skeletor\Model\Model;

class StatusDate extends Model
{
    private $statusDateId;

    private $targetStatusId;

    private $targetStatus;

    private $customFieldId;

    private $customField;

    public function __construct($statusDateId, $targetStatusId, $targetStatus, $customFieldId, $customField, $createdAt, $updatedAt)
    {
        $this->statusDateId = $statusDateId;
        $this->targetStatusId = $targetStatusId;
        $this->targetStatus = $targetStatus;
        $this->customFieldId = $customFieldId;
        $this->customField = $customField;

        parent::__construct($createdAt, $updatedAt);
    }

    public function getStatusDateId()
    {
        return (int) $this->statusDateId;
    }

    public function getId()
    {
        return (int) $this->statusDateId;
    }

    /**
     * @return mixed
     */
    public function getTargetStatusId()
    {
        return $this->targetStatusId;
    }

    /**
     * @return mixed
     */
    public function getCustomFieldId()
    {
        return $this->customFieldId;
    }

    /**
     * @return mixed
     */
    public function getTargetStatus()
    {
        return $this->targetStatus;
    }

    /**
     * @return mixed
     */
    public function getCustomField()
    {
        return $this->customField;
    }
}