<?php
declare(strict_types=1);

namespace WeCare\StatusDates\Repository;

use Skeletor\Mapper\NotFoundException;
use WeCare\StatusDates\Mapper\StatusDatesDepartments as Mapper;
use WeCare\StatusDates\Model\StatusDateDepartment as Model;
use WeCare\User\Service\User;
use Laminas\Config\Config;


class StatusDatesDepartmentRepository
{
    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * @var User
     */
    private $userService;

    /**
     * @var \DateTime
     */
    private $dt;

    /**
     * @var Config
     */
    private $config;

    /**
     * userRepository constructor.
     *
     * @param Mapper $userMapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(
        Mapper $mapper, User $userService, \DateTime $dt, Config $config
    ) {
        $this->mapper = $mapper;
        $this->userService = $userService;
        $this->dt = $dt;
        $this->config = $config;
    }

    /**
     * Fetches a list of UserEntity models.
     *
     * @param array $params
     *
     * @return array
     */
    public function fetchAll($params = array(), $limit = null, $order = null): array
    {
        $items = [];
        foreach ($this->mapper->fetchAll($params, $limit, $order) as $data) {
            $items[] = $this->make($data);
        }

        return $items;
    }

    /**
     * @param $userId
     * @return User
     * @throws NotFoundException
     */
    public function getById($id): Model
    {
        return $this->make($this->mapper->fetchById((int) $id));
    }

    /**
     * Updates user model.
     *
     * @param $data
     * @return User
     * @throws \Exception
     *
     */
    public function update($data): Model
    {
        $this->mapper->update($data);

        return $this->getById($data['statusDatesDepartmentsId']);
    }

    public function create($data): Model
    {
        return $this->getById($this->mapper->insert($data));
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    private function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }

        return new Model(
            $data['statusDatesDepartmentsId'],
            $data['departmentId'],
            $data['department'],
            $data['createdAt'],
            $data['updatedAt']
        );
    }

    public function deleteStatusDateDepartment($id): bool
    {
        return $this->mapper->delete($id);
    }
}
