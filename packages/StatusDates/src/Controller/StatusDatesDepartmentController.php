<?php
declare(strict_types=1);
namespace WeCare\StatusDates\Controller;

use GuzzleHttp\Psr7\Response;
use WeCare\StatusDates\Repository\StatusDatesDepartmentRepository;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use WeCare\User\Service\User;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Controller\Controller;
use Psr\Log\LoggerInterface as Logger;
use WeCare\Ticket\Service\Kayako;

class StatusDatesDepartmentController extends Controller
{
    private $departmentRepo;

    /**
     * @var User
     */
    private $userService;

    /**
     * @var Kayako
     */
    private $kayako;

    private $logger;

    /**
     * ArticleController constructor.
     * @param Article $articleService
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param \Twig\Environment $twig
     * @param User $userService
     */
    public function __construct(
        StatusDatesDepartmentRepository $departmentRepo, Session $session, Config $config, Flash $flash, \Twig\Environment $twig,
        User $userService, Logger $logger, Kayako $kayako
    ) {
        parent::__construct($twig, $config, $session, $flash);

        $this->departmentRepo = $departmentRepo;
        $this->userService = $userService;
        $this->logger = $logger;
        $this->kayako = $kayako;
    }

    public function index(): Response
    {
//        return $this->respond('index', ['items' => $this->serviceRepo->fetchAll()]);
    }

    public function create(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        try {
            $department = explode('#', $data['departmentId']);
            $this->departmentRepo->create([
                'statusDatesDepartmentsId' => $data['statusDatesDepartmentsId'],
                'departmentId' => $department[1],
                'department' => $department[0],
            ]);
            $this->getFlash()->success('Department successfully added.');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die;
        }
        return $this->redirect('/admin/department/view/');
    }

    public function update(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        //@TODO validate data
        try {
            $this->departmentRepo->update($data);
            $this->getFlash()->success('Rule updated.');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }

        return $this->redirect('/admin/department/view/');
    }

    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('id');
        $existingDeps = $this->departmentRepo->fetchAll();
        $departments = [];
        /* @var \kyDepartment $department */
        foreach ($this->kayako->getDepartments() as $department) {
            foreach ($existingDeps as $dep) {
                if ($dep->getDepartmentId() === $department->getId()) {
                    continue 2;
                }
            }
            $departments[$department->getId()] = $department->getTitle();
        }
        $department = false;
        $this->setGlobalVariable('pageTitle', 'Create department');
        if ($id) {
            $department = $this->departmentRepo->getById($id);
            $this->setGlobalVariable('pageTitle', 'Edit department: ' . $department->getid());
        }

        asort($departments, SORT_NATURAL);
        return $this->respondPartial('admin/statusdatesdepartment/form.twig', [
            'department' => $department,
            'departments' => $departments,
        ]);
    }

    /**
     * Articles list & view
     *
     * @return Response
     */
    public function view(): Response
    {
        $this->setGlobalVariable('pageTitle', 'View Rules');

        return $this->respond('view', [
            'departments' => $this->departmentRepo->fetchAll(),
        ]);
    }

    /**
     * @TODO make sure there are no relations before deleting
     *
     * @return Response
     */
    public function delete(): Response
    {
        try {
            $this->departmentRepo->deleteStatusDateDepartment((int) $this->getRequest()->getAttribute('id'));
            $this->getFlash()->success('Department successfully deleted');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        return $this->redirect('/admin/department/view/');
    }

}