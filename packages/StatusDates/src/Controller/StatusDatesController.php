<?php
declare(strict_types=1);
namespace WeCare\StatusDates\Controller;

use GuzzleHttp\Psr7\Response;
use WeCare\StatusDates\Model\StatusDate;
use WeCare\StatusDates\Repository\StatusDatesDepartmentRepository;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use WeCare\StatusDates\Service\StatusDates;
use WeCare\User\Service\User;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Controller\Controller;
use Psr\Log\LoggerInterface as Logger;
use WeCare\Ticket\Service\Kayako;
use WeCare\Ticket\Service\Mailer;

class StatusDatesController extends Controller
{
    private $departmentRepository;

    /**
     * @var User
     */
    private $userService;

    /**
     * @var Kayako
     */
    private $kayako;

    private $logger;

    /**
     * @var Mailer
     */
    private $mail;

    private $statusDateService;

    /**
     * ArticleController constructor.
     * @param Article $articleService
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param \Twig\Environment $twig
     * @param User $userService
     */
    public function __construct(
        StatusDatesDepartmentRepository $departmentRepository, Session $session, Config $config, Flash $flash, \Twig\Environment $twig,
        User $userService, Logger $logger, Kayako $kayako, StatusDates $statusDateService, Mailer $mail
    ) {
        parent::__construct($twig, $config, $session, $flash);

        $this->userService = $userService;
        $this->logger = $logger;
        $this->departmentRepository = $departmentRepository;
        $this->kayako = $kayako;
        $this->statusDateService = $statusDateService;
        $this->mail = $mail;
    }

    public function index(): Response
    {
//        return $this->respond('index', ['items' => $this->serviceRepo->fetchAll()]);
    }

    public function runDuplicatesCheck()
    {
        $data = $this->statusDateService->getDuplicates();
        $response = new Response();
        $data = (array) json_decode($data->getBody()->getContents());
        $response->getBody()->write('1');
        $response->getBody()->rewind();
        $this->mail->sendStatusDatesReport($data, 'Zavrsen proces upisa duplikata u cf.', 'Duplicate ticket write process report ' );

        return $response;
    }

    public function testProcess()
    {
        global $argv;

        $limit = null;
        if (isset($argv[3])) {
            $limit = $argv[3];
        }
        $messages = [
            'success' => [], 'errors' => [], 'multipleStatuses' => []
        ];
        /* @var StatusDate $rule */
        foreach ($this->statusDateService->getAll() as $key => $rule) {
            $response = $this->run($rule->getid());
            if ($response['status']) {
                if (isset($response['data']['multipleStatuses'])) {
                    $messages['multipleStatuses'][$rule->getCustomField()] = $response['data']['multipleStatuses'];
                }
                if (isset($response['data']['results']['success'])) {
                    $messages['success'][$rule->getCustomField()] = $response['data']['results']['success'];
                }
            } else {
                $messages['errors'][$rule->getCustomField()] = $response['data']['results']['error'];
                $this->logger->error('Failed to write CF for ticket ' . $response['data']['results']['error']);
            }
            if ($limit && $key == ($limit-1)) {
                echo sprintf('break after key %s', $key);
                break;
            }
        }

        if (!empty($messages['success']) || !empty($messages['multipleStatuses']) || !empty($messages['errors'])) {
//            $subject = sprintf('Datum statusa report - status %s i cf %s', $rule->getTargetStatus(), $rule->getCustomField());
            $subject = sprintf('Datum statusa report');
            $this->mail->sendStatusDatesReport($messages, 'Datumi statusa report.', $subject);
        }

        return $this->getResponse();
    }

    public function runProcess()
    {
        $this->run($this->getRequest()->getAttribute('id'));

        return $this->redirect('/admin/status-dates/view/');
    }

    private function run($id)
    {
        $response = $this->statusDateService->runProcess($this->statusDateService->getById($id));
        $message = '';
        $return = [
            'message' => 'Process failed to execute.',
            'status' => 0,
        ];
        if ($response) {
            $message = 'Process ran successfully.' . PHP_EOL;
            $return['message'] = $message;
            $return['status'] = 1;
            $return['data'] = json_decode($response->getBody()->getContents(), true);
            $message .= print_r($return['data'], true);
        }
        $this->getFlash()->success($message);

        return $return;
    }

    public function create(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        $status = explode('#', $data['targetStatusId']);
        $field = explode('#', $data['customFieldId']);
        try {
            $this->statusDateService->create([
                'statusDateId' => $data['statusDateId'],
                'targetStatusId' => $status[1],
                'targetStatus' => $status[0],
                'customFieldId' => $field[1],
                'customField' => $field[0],
            ]);
            $this->getFlash()->success('Rule successfully created.');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die;
        }
        return $this->redirect('/admin/status-dates/view/');
    }

    public function update(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        $status = explode('#', $data['targetStatusId']);
        $field = explode('#', $data['customFieldId']);
        //@TODO validate data
        try {
            $this->statusDateService->update([
                'statusDateId' => $data['statusDateId'],
                'targetStatusId' => $status[1],
                'targetStatus' => $status[0],
                'customFieldId' => $field[1],
                'customField' => $field[0],
            ]);
            $this->getFlash()->success('Rule updated.');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }

        return $this->redirect('/admin/status-dates/view/');
    }

    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('id');
        $rule = false;
        $this->setGlobalVariable('pageTitle', 'Create rule');
        if ($id) {
            $rule = $this->statusDateService->getById($id);
            $this->setGlobalVariable('pageTitle', 'Edit rule: ' . $rule->getid());
        }

//        $ticket = $this->kayako->getTicketById(92027369); // staging
        $ticket = $this->kayako->getTicketById(1210178341); // prod
        $existingFields = [];
        $existingRules = [];
        /* @var StatusDate $item */
        foreach ($this->statusDateService->getAll() as $item) {
            $existingFields[] = $item->getCustomFieldId();
            $existingRules[] = $item->getTargetStatusId();
        }
        $customFields = [];
        /* @var \kyTicketCustomFieldGroup $fieldGroup */
        foreach ($ticket->getCustomFieldGroups() as $fieldGroup) {
            if (false === strpos($fieldGroup->getTitle(), 'Datumi statusa')) {
//            if ($fieldGroup->getTitle() !== 'Datumi statusa (samo za Admine)') {
                continue;
            }
            foreach ($fieldGroup->getFields() as $field) {
                if (!in_array($field->getId(), $existingFields)) {
                    $customFields[$field->getId()] = $field->getTitle();
                }
            }
        }
        $statuses = [];
        // TODO filter existing statuses
//        $forDepartments = [45, 46, 48, 49, 50, 52, 53, 59, 65, 69, 70, 98];
        $forDepartments = [];
        foreach ($this->departmentRepository->fetchAll() as $dep) {
            $forDepartments[] = $dep->getDepartmentId();
        }
        foreach ($this->kayako->getStatusList()->filterBy('getDepartmentId', $forDepartments) as $status) {
            if (!in_array($status->getId(), $existingRules)) {
                $statuses[$status->getId()] = $status->getTitle();
            }
        }
        asort($statuses,  SORT_NATURAL);
        asort($customFields, SORT_NATURAL);
        return $this->respondPartial('admin/statusdates/form.twig', [
            'rule' => $rule,
            'statuses' => $statuses,
            'customFields' => $customFields,
        ]);
    }

    /**
     * Articles list & view
     *
     * @return Response
     */
    public function view(): Response
    {
        $this->setGlobalVariable('pageTitle', 'View Rules');

        return $this->respond('view', [
            'rules' => $this->statusDateService->getAll(),
        ]);
    }

    /**
     * @TODO make sure there are no relations before deleting
     *
     * @return Response
     */
    public function delete(): Response
    {
        try {
            $this->statusDateService->deleteStatusDate((int) $this->getRequest()->getAttribute('id'));
            $this->getFlash()->success('Rule successfully deleted');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        return $this->redirect('/admin/status-dates/view/');
    }

}