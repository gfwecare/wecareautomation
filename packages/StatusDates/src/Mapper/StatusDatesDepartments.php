<?php

namespace WeCare\StatusDates\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;

class StatusDatesDepartments extends MysqlCrudMapper
{

    /**
     * Manufacturer constructor.
     */
    public function __construct(\PDO $pdo)
    {
        parent::__construct($pdo, 'statusDatesDepartments', 'statusDatesDepartmentsId');
    }
}