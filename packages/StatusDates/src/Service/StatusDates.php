<?php
declare(strict_types=1);
namespace WeCare\StatusDates\Service;

use GuzzleHttp\Client;
use WeCare\StatusDates\Model\StatusDate;
use WeCare\StatusDates\Repository\StatusDatesRepository;
use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;

class StatusDates
{
    private $statusDateRepo;

    private $httpClient;

    private $logger;

    private $config;

    public function __construct(StatusDatesRepository $repository, Client $httpClient, Logger $logger, Config $config)
    {
        $this->httpClient = $httpClient;
        $this->statusDateRepo = $repository;
        $this->logger = $logger;
        $this->config = $config;
    }

    public function getDuplicates()
    {
        $url = sprintf($this->config->services->miniApi . '/city/scanTicketStatusChange.php?action=duplicate');
        $msg = sprintf('Running duplicates process.');
        $this->logger->debug($msg);
        try {
            $response = $this->httpClient->send(new \GuzzleHttp\Psr7\Request('GET', $url));
            $this->logger->debug("Duplicates process complete.");
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $this->logger->error($e->getMessage());

            return false;
        }

        return $response;
    }

    public function runProcess(StatusDate $rule)
    {
        $url = sprintf($this->config->services->miniApi . '/city/scanTicketStatusChange.php?statusId=%d&fieldId=%d',
            $rule->getTargetStatusId(), $rule->getCustomFieldId());
        $msg = sprintf('Running process for : %s and %s to url %s', $rule->getTargetStatusId(), $rule->getCustomFieldId(), $url);
        $this->logger->debug($msg);
        try {
            $response = $this->httpClient->send(new \GuzzleHttp\Psr7\Request('GET', $url));
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $this->logger->error($e->getMessage());

            return false;
        }

        return $response;
    }

    public function getAll($params = [])
    {
        return $this->statusDateRepo->fetchAll($params);
    }

    public function create($data)
    {
        return $this->statusDateRepo->create($data);
    }

    public function update($data)
    {
        return $this->statusDateRepo->update($data);
    }

    public function getById($id)
    {
        return $this->statusDateRepo->getById($id);
    }

    public function deleteStatusDate($id)
    {
        return $this->statusDateRepo->deleteStatusDate($id);
    }
}