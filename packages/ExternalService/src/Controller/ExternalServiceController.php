<?php
declare(strict_types=1);
namespace WeCare\ExternalService\Controller;

use GuzzleHttp\Psr7\Response;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use WeCare\ExternalService\Model\ExternalService;
use WeCare\ExternalService\Model\Manufacturer;
use WeCare\ExternalService\Repository\ExternalServiceRepository;
use WeCare\ExternalService\Repository\ManufacturerRepository;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use WeCare\User\Service\User;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Controller\Controller;
use Psr\Log\LoggerInterface as Logger;
use WeCare\Ticket\Service\Kayako;

//use SNF\Article\Model\DataTablesRequest;

class ExternalServiceController extends Controller
{
    private $serviceRepo;

    /**
     * @var User
     */
    private $userService;

    private $manufacturerRepo;

    /**
     * @var Kayako
     */
    private $kayako;

    private $logger;

    /**
     * ArticleController constructor.
     * @param Article $articleService
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param \Twig\Environment $twig
     * @param User $userService
     */
    public function __construct(
        ExternalServiceRepository $serviceRepo, Session $session, Config $config, Flash $flash, \Twig\Environment $twig,
        User $userService, Logger $logger, ManufacturerRepository $manufacturerRepository, Kayako $kayako
    ) {
        parent::__construct($twig, $config, $session, $flash);

        $this->serviceRepo = $serviceRepo;
        $this->userService = $userService;
        $this->manufacturerRepo = $manufacturerRepository;
        $this->logger = $logger;
        $this->kayako = $kayako;
    }

    public function index(): Response
    {
        return $this->respond('index', ['items' => $this->serviceRepo->fetchAll()]);
    }

    public function create(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        try {
            $this->serviceRepo->create($data);
            $this->getFlash()->success('ES successfully created.');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die;
        }
        return $this->redirect('/admin/external-service/view/');
    }

    public function update(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        //@TODO validate data
        try {
            $this->serviceRepo->update($data);
            $this->getFlash()->success('ES updated.');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        return $this->redirect('/admin/external-service/view/');
    }



    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('externalServiceId');
        $service = false;
        $this->setGlobalVariable('pageTitle', 'Create ES');
        if ($id) {
            $service = $this->serviceRepo->getById($id);
            $this->setGlobalVariable('pageTitle', 'Edit ES: ' . $service->getName());
        }

        return $this->respondPartial('admin/externalservice/form.twig', [
            'service' => $service,
            'types' => $this->manufacturerRepo->getTypes(),
            'rules' => $this->serviceRepo->getRules($id),
            'manufacturers' => $this->manufacturerRepo->fetchAll()
        ]);
    }

    public function export(): Response
    {
        $this->setGlobalVariable('pageTitle', 'View ES');
        $types = $this->manufacturerRepo->getTypes();
        $csv = 'name, address, phone, city, zip, kayakoId, manufacturers' . PHP_EOL;
        /* @var ExternalService $service */
        foreach ($this->serviceRepo->fetchAll() as $service) {
            $csv .= '"'. $service->getName() .'",';
            $csv .= '"'. $service->getAddress() .'",';
            $csv .= '"'. $service->getPhone() .'",';
            $csv .= '"'. $service->getCity() .'",';
            $csv .= '"'. $service->getZip() .'",';
            $csv .= '"'. $service->getKayakoId() .'",';
            $csv .= '"';
            /* @var ExternalService\Manufacturer $manufacturer */
            foreach ($service->getManufacturers() as $manufacturer) {
                $csv .= $manufacturer['manufacturer'];
                if (strlen($manufacturer['acceptTypeIds'])) {
                    $csv .= '|| accept';
                    foreach (explode(',', $manufacturer['acceptTypeIds']) as $id) {
                        if (isset($types[$id]['name'])) {
                            $csv .= ' ' . addslashes($types[$id]['name']) . ', ';
                        }
                    }
                }
                if (strlen($manufacturer['ignoredTypeIds'])) {
                    $csv .= '|| ignore';
                    foreach (explode(',', $manufacturer['ignoredTypeIds']) as $id) {
                        if (isset($types[$id]['name'])) {
                            $csv .= ' ' . addslashes($types[$id]['name']) . ', ';
                        }
                    }
                }
            }
            $csv .= '"' . PHP_EOL;
        }
        echo $csv;
        die();
    }

    /**
     * Articles list & view
     *
     * @return Response
     */
    public function view(): Response
    {
        $this->setGlobalVariable('pageTitle', 'View ES');

        return $this->respond('view', [
            'services' => $this->serviceRepo->fetchAll(),
            'types' => $this->manufacturerRepo->getTypes(),
        ]);
    }

    /**
     * @TODO make sure there are no relations before deleting
     *
     * @return Response
     */
    public function delete(): Response
    {
        try {
            $this->serviceRepo->deleteService((int) $this->getRequest()->getAttribute('externalServiceId'));
            $this->getFlash()->success('ES successfully deleted');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        return $this->redirect('/admin/external-service/view/');
    }

    public function fixServices()
    {
        /* @var \kyCustomFieldSelect $field */
        $field = $this->kayako->getTicketById(102012265)->getCustomField('pschykwzeg00');
        foreach ($field->getDefinition()->getOptions()->getRawArray() as $key => $option) {
            if ($key === 0) {
                continue;
            }
            $data = $this->serviceRepo->fetchAll(['name' => trim($option->value)])[0];
            $data = $data->toArray();
            $data['kayakoId'] = $option->getId();
            try {
                $this->serviceRepo->update($data);
            } catch (\Exception $e) {
                if (strpos($e->getMessage(), 'Duplicate entry') === false) {
                    var_dump($e->getMessage());
                } else {
                    $service = $this->serviceRepo->fetchAll(['name' => $data['name']])[0];
                    $data = $service->toArray();
                    $data['kayakoId'] = $option->id;
                    $this->serviceRepo->update($data);
                }
            }
        }
        $this->getFlash()->success('ES successfully synchronized with kayako service.');
        return $this->redirect('/admin/external-service/view/');
    }

    public function fetchServices()
    {
        /* @var \kyCustomFieldSelect $field */
        $field = $this->kayako->getTicketById(102012265)->getCustomField('pschykwzeg00');
        foreach ($field->getDefinition()->getOptions()->getRawArray() as $key => $option) {
            if ($key === 0) {
                continue;
            }
            if (count($this->serviceRepo->fetchAll(['kayakoId' => $option->id])) === 1) {
                continue;
            }
            if (!(int) $option->id) {
                continue;
            }
            $data = [
                'externalServiceId' => null,
                'name' => trim($option->value),
                'address' => '',
                'city' => '',
                'zip' => '',
                'phone' => '',
                'kayakoId' => $option->id,
            ];
            try {
                $this->serviceRepo->create($data);
            } catch (\Exception $e) {
                if (strpos($e->getMessage(), 'Duplicate entry') === false) {
                    var_dump($e->getMessage());
                    die();
                } else {
                    $service = $this->serviceRepo->fetchAll(['name' => $data['name']])[0];
                    $data = $service->toArray();
                    $data['kayakoId'] = $option->id;
                    $this->serviceRepo->update($data);
                }
            }
        }
        $this->getFlash()->success('ES successfully synchronized with kayako service.');
        return $this->redirect('/admin/external-service/view/');
    }

    /**
     * NOT USED.
     * used initialy to import data
     */
//    public function enterDataFromXls()
//    {
//        $xlsx = new Xlsx();
//        $sheet = $xlsx->load(DATA_PATH . '/ex.xlsx');
//        $worksheet = $sheet->getActiveSheet();
//        foreach ($worksheet->getRowIterator() as $row) {
//            $data = [];
//            if ($row->getRowIndex() < 2) {
//                continue;
//            }
//            $data['kayakoId'] = 0;
//            foreach ($row->getCellIterator() as $cell) {
//                if ($cell->getColumn() === 'A') {
//                    $data['name'] = trim($cell->getValue());
//                }
//                if ($cell->getColumn() === 'B') {
//                    $data['address'] = trim((string) $cell->getValue());
//                }
//                if ($cell->getColumn() === 'C') {
//                    $data['city'] = trim((string) $cell->getValue());
//                }
//                if ($cell->getColumn() === 'D') {
//                    $data['zip'] = trim((string) $cell->getValue());
//                }
//                if ($cell->getColumn() === 'E') {
//                    $data['phone'] = $cell->getValue();
//                }
//                if ($cell->getColumn() === 'F') {
//                    $manufacturers = trim((string) $cell->getValue());
//                }
//            }
//            $es = $this->serviceRepo->create($data);
////            $es = $this->serviceRepo->fetchAll(['name' => $data['name']])[0];
//            foreach (explode(',', $manufacturers) as $value) {
//                $value = trim($value);
//                $manufacturer = $this->manufacturerRepo->fetchAll(['name' => $value]);
//                if (!isset($manufacturer[0])) {
//                    continue;
//                }
//                if (is_object($manufacturer[0])) {
//                    $this->serviceRepo->createRule($es->getId(), $manufacturer[0]->getId(), $value);
//                }
//            }
//        }
//        echo 'done';
//        die();
//    }
}