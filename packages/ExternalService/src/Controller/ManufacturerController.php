<?php
declare(strict_types=1);
namespace WeCare\ExternalService\Controller;

use GuzzleHttp\Psr7\Response;
use WeCare\ExternalService\Repository\ManufacturerRepository;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use WeCare\User\Service\User;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Controller\Controller;
use Psr\Log\LoggerInterface as Logger;
use WeCare\Ticket\Service\Kayako;

//use SNF\Article\Model\DataTablesRequest;

class ManufacturerController extends Controller
{
    private $manufacturerRepo;

    /**
     * @var User
     */
    private $userService;

    /**
     * @var Kayako
     */
    private $kayako;

    private $logger;

    /**
     * ArticleController constructor.
     * @param Article $articleService
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param \Twig\Environment $twig
     * @param User $userService
     */
    public function __construct(
        ManufacturerRepository $manufacturerRepo, Session $session, Config $config, Flash $flash, \Twig\Environment $twig,
        User $userService, Logger $logger, Kayako $kayako
    ) {
        parent::__construct($twig, $config, $session, $flash);

        $this->manufacturerRepo = $manufacturerRepo;
        $this->userService = $userService;
        $this->logger = $logger;
        $this->kayako = $kayako;
    }

    public function index(): Response
    {
        return $this->respond('index', ['items' => $this->manufacturerRepo->fetchAll()]);
    }

    public function viewTypes()
    {
        $this->setGlobalVariable('pageTitle', 'View types');

        return $this->respond('viewTypes', [
            'types' => $this->manufacturerRepo->getTypes()
        ]);
    }

    /**
     * Fetch ticket types from kayako
     * @TODO check existing items before writing, should be faster
     *
     * @return Response
     */
    public function fetchTypes()
    {
        foreach (\kyTicketType::getAll() as $key => $type) {
            if ($key === 0) {
                continue;
            }
            $data = [
                'ticketTypeId' => null,
                'name' => trim($type->title),
                'kayakoId' => $type->getId()
            ];
            try {
                $this->manufacturerRepo->createType($data);
            } catch (\Exception $e) {
                if (strpos($e->getMessage(), 'Duplicate entry') === false) {
                    var_dump($e->getMessage());
                }
            }
        }
        $this->getFlash()->success('Ticket types successfully synchronized with kayako service.');
        return $this->redirect('/admin/manufacturer/viewTypes/');
    }

    /**
     * Sync manufacturers from kayako
     * @TODO check existing items before writing, should be faster
     *
     * @return Response
     * @throws \Exception
     */
    public function fetchManufacturers()
    {
        /* @var \kyCustomFieldSelect $test */
        $test = $this->kayako->getTicketById(102012265)->getCustomField('zcut43ahiib9');
        foreach ($test->getDefinition()->getOptions()->getRawArray() as $key => $option) {
            if ($key === 0) {
                continue;
            }

            $data = [
                'manufacturerId' => null,
                'name' => trim($option->value),
                'kayakoId' => $option->getId()
            ];
            try {
                $this->manufacturerRepo->create($data);
            } catch (\Exception $e) {
                if (strpos($e->getMessage(), 'Duplicate entry') === false) {
                    var_dump($e->getMessage());
                }
            }
        }
        $this->getFlash()->success('Manufacturers successfully synchronized with kayako service.');
        return $this->redirect('/admin/manufacturer/view/');
    }

    public function create(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        try {
            $this->manufacturerRepo->create($data);
            $this->getFlash()->success('Manufacturer successfully created.');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die;
        }
        return $this->redirect('/admin/manufacturer/view/');
    }

    public function update(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        //@TODO validate data
        try {
            $this->manufacturerRepo->update($data);
            $this->getFlash()->success('Manufacturer updated.');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        return $this->redirect('/admin/manufacturer/view/');
    }

    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('manufacturerId');
        $manufacturer = false;
        $this->setGlobalVariable('pageTitle', 'Create manufacturer');
        if ($id) {
            $manufacturer = $this->manufacturerRepo->getById($id);
            $this->setGlobalVariable('pageTitle', 'Edit manufacturer: ' . $manufacturer->getName());
        }

        return $this->respondPartial('manufacturer/form.twig', [
            'manufacturer' => $manufacturer
        ]);
    }

    /**
     * Articles list & view
     *
     * @return Response
     */
    public function view(): Response
    {
        $this->setGlobalVariable('pageTitle', 'View manufacturers');

        return $this->respond('view', ['manufacturers' => $this->manufacturerRepo->fetchAll([], null, ['orderBy' => 'name', 'dir' => 'ASC'])]);
    }

    /**
     * @TODO make sure there are no relations before deleting
     *
     * @return Response
     */
    public function delete(): Response
    {
        try {
            $this->manufacturerRepo->deleteManufacturer((int) $this->getRequest()->getAttribute('manufacturerId'));
            $this->getFlash()->success('Manufacturer successfully deleted');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        return $this->redirect('/admin/manufacturer/view/');
    }

}