<?php
declare(strict_types=1);

namespace WeCare\ExternalService\Repository;

use Skeletor\Mapper\NotFoundException;
use WeCare\ExternalService\Mapper\Manufacturer as Mapper;
use WeCare\ExternalService\Model\Manufacturer as Model;
use WeCare\ExternalService\Mapper\TicketType;
use WeCare\User\Service\User;
use Laminas\Config\Config;


class ManufacturerRepository
{
    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * @var User
     */
    private $userService;

    /**
     * @var TicketType
     */
    private $ticketTypeMapper;

    /**
     * @var \DateTime
     */
    private $dt;

    /**
     * @var Config
     */
    private $config;

    /**
     * userRepository constructor.
     *
     * @param Mapper $userMapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(
        Mapper $manufacturerMapper, User $userService, \DateTime $dt, Config $config, TicketType $ticketTypeMapper
    ) {
        $this->mapper = $manufacturerMapper;
        $this->ticketTypeMapper = $ticketTypeMapper;
        $this->userService = $userService;
        $this->dt = $dt;
        $this->config = $config;
    }

    /**
     * Fetches a list of UserEntity models.
     *
     * @param array $params
     *
     * @return array
     */
    public function fetchAll($params = array(), $limit = null, $order = null): array
    {
        $items = [];
        foreach ($this->mapper->fetchAll($params, $limit, $order) as $data) {
            $items[] = $this->make($data);
        }

        return $items;
    }

    /**
     * @param $userId
     * @return User
     * @throws NotFoundException
     */
    public function getById($id): Model
    {
        return $this->make($this->mapper->fetchById((int) $id));
    }

    /**
     * Updates user model.
     *
     * @param $data
     * @return User
     * @throws \Exception
     *
     */
    public function update($data): Model
    {
        $this->mapper->update($data);

        return $this->getById($data['manufacturerId']);
    }

    public function getTypes()
    {
        $items = [];
        foreach ($this->ticketTypeMapper->fetchAll() as $item) {
            $items[$item['kayakoId']] = $item;
        }

        return $items;
    }

    public function createType($data)
    {
        return $this->ticketTypeMapper->insert($data);
    }

    public function create($data): Model
    {
        return $this->getById($this->mapper->insert($data));
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    private function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }

        return new Model(
            $data['manufacturerId'],
            $data['name'],
            $data['kayakoId'],
            $data['createdAt'],
            $data['updatedAt']
        );
    }

    public function deleteManufacturer($id): bool
    {
        return $this->mapper->delete($id);
    }
}
