<?php
declare(strict_types=1);

namespace WeCare\ExternalService\Repository;

use Skeletor\Mapper\NotFoundException;
use WeCare\ExternalService\Mapper\ExternalService as Mapper;
use WeCare\ExternalService\Model\ExternalService as Model;
use WeCare\ExternalService\Mapper\Manufacturer;
use WeCare\User\Service\User;
use Laminas\Config\Config;
use Tamtamchik\SimpleFlash\Flash;


class ExternalServiceRepository
{
    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * @var User
     */
    private $userService;

    private $manufacturerRepo;

    /**
     * @var \DateTime
     */
    private $dt;

    private $flash;

    /**
     * @var Config
     */
    private $config;

    /**
     * userRepository constructor.
     *
     * @param Mapper $userMapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(Mapper $serviceMapper, Manufacturer $manufacturerRepo, User $userService, \DateTime $dt, Config $config, Flash $flash)
    {
        $this->mapper = $serviceMapper;
        $this->userService = $userService;
        $this->manufacturerRepo = $manufacturerRepo;
        $this->dt = $dt;
        $this->flash = $flash;
        $this->config = $config;
    }

    public function findEsForFixedRule($manufacturer, $typeId)
    {
        return $this->mapper->findEsForFixedRule($manufacturer, $typeId);
    }

    public function findEsForGeneralRule($manufacturer, $typeId)
    {
        return $this->mapper->findEsForGeneralRule($manufacturer, $typeId);
    }

    /**
     * Fetches a list of UserEntity models.
     *
     * @param array $params
     *
     * @return array
     */
    public function fetchAll($params = array()): array
    {
        $items = [];
        foreach ($this->mapper->fetchAll($params) as $data) {
            $model = $this->make($data);
//            $model->setManufacturers($this->mapper->getRules($model->getId()));
            $items[] = $model;
        }

        return $items;
    }

    /**
     * @param $userId
     * @return User
     * @throws NotFoundException
     */
    public function getById($id): Model
    {
        return $this->make($this->mapper->fetchById((int) $id));
    }

    public function getByKayakoId($id): Model
    {
        return $this->make($this->mapper->fetchByKayakoId((int) $id));
    }

    /**
     * Updates user model.
     *
     * @param $data
     * @return User
     * @throws \Exception
     *
     */
    public function update($data): Model
    {
        if (isset($data['manufacturerId'])) {
            $data = $this->updateRules($data);
        }
        $this->mapper->update($data);

        return $this->getById($data['externalServiceId']);
    }

    public function getRules(int $serviceId)
    {
        return $this->mapper->getRules($serviceId);
    }

    /**
     * Updates rules, and removes params from data.
     *
     * @param $data
     * @return mixed
     * @throws NotFoundException
     * @throws \Exception
     */
    private function updateRules($data)
    {
        $this->mapper->deleteRules($data['externalServiceId']);
        foreach ($data['manufacturerId'] as $i => $datum) {
            if ((int) $data['manufacturerId'][$i] === 0) {
                break;
            }
            $acceptTypes = '';
            if (isset($data['acceptedTypes'][$i])) {
                $acceptTypes = implode(',', $data['acceptedTypes'][$i]);
            }
            $ignoredTypes = '';
            if (isset($data['ignoredTypes'][$i])) {
                $ignoredTypes = implode(',', $data['ignoredTypes'][$i]);
            }
            $manufacturer = $this->manufacturerRepo->fetchById((int) $data['manufacturerId'][$i]);
            if (!$this->createRule($data['externalServiceId'], $data['manufacturerId'][$i], $manufacturer['name'],
                $acceptTypes, $ignoredTypes)) {
                $this->flash::error(sprintf('Rule for %s and %s already exists.', $manufacturer['name'], $acceptTypes . $ignoredTypes));
            }
        }
        unset($data['manufacturerId']);
        unset($data['acceptedTypes']);
        unset($data['ignoredTypes']);

        return $data;
    }

    public function create($data): Model
    {
        if (!isset($data['kayakoId'])) {
            $data['kayakoId'] = 0;
        }
        $rulesData = $data;
        unset($data['manufacturerId']);
        unset($data['acceptedTypes']);
        unset($data['ignoredTypes']);
        $model = $this->getById($this->mapper->insert($data));
        $rulesData['externalServiceId'] = $model->getId();
        if (isset($rulesData['manufacturerId'])) {
            $this->updateRules($rulesData);
        }

        return $model;
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    private function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }
        if (!isset($data['manufacturers'])) {
            $data['manufacturers'] = '';
            if (isset($data['externalServiceId'])) {
                $data['manufacturers'] = $this->mapper->getManufacturers($data['externalServiceId']);
            }
        }

        return new Model(
            $data['externalServiceId'],
            $data['name'],
            $data['address'],
            $data['city'],
            $data['zip'],
            $data['phone'],
            $data['kayakoId'],
            $data['manufacturers'],
            $data['createdAt'],
            $data['updatedAt']
        );
    }

    public function createRule($serviceId, $manufacturerId, $manufacturer, $acceptTypes = null, $ignoredTypes = null)
    {
        return $this->mapper->createRule($serviceId, $manufacturerId, $manufacturer, $acceptTypes, $ignoredTypes);
    }

    public function deleteService($id): bool
    {
        return $this->mapper->delete($id);
    }
}
