<?php

namespace WeCare\ExternalService\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;

class ExternalService extends MysqlCrudMapper
{

    /**
     * Manufacturer constructor.
     */
    public function __construct(\PDO $pdo)
    {
        parent::__construct($pdo, 'externalService');
    }

    public function findEsForGeneralRule($manufacturer, $typeId)
    {
        $sql = "SELECT * FROM `externalServiceManufacturer` WHERE `manufacturer` = '{$manufacturer}' AND 
          `acceptTypeIds` = '' AND (`ignoredTypeIds` NOT LIKE '%{$typeId},%' OR 
          `ignoredTypeIds` NOT LIKE '%,{$typeId}%' OR `ignoredTypeIds` NOT LIKE '{$typeId}'
          )";
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function findEsForFixedRule($manufacturer, $typeId)
    {
        $sql = "SELECT * FROM `externalServiceManufacturer` WHERE `manufacturer` = '{$manufacturer}' AND 
          `acceptTypeIds` LIKE '%{$typeId}%' AND (`ignoredTypeIds` NOT LIKE '%{$typeId}%' OR `ignoredTypeIds` = '' OR `ignoredTypeIds` IS NULL)";
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getRules(int $serviceId)
    {
        $sql = "SELECT * FROM `externalServiceManufacturer` WHERE `{$this->primaryKeyName}` = $serviceId";
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function createRule($sId, $mId, $manufacturer, $acceptTypes = null, $ignoredTypes = null)
    {
        $fields = 'externalServiceId, manufacturerId, manufacturer';
        $values = "{$sId}, {$mId}, '{$manufacturer}'";
        if ($acceptTypes) {
            $fields .= ',acceptTypeIds';
            $values .= ",'{$acceptTypes}'";
        }
        if ($ignoredTypes) {
            $fields .= ',ignoredTypeIds';
            $values .= ",'{$ignoredTypes}'";
        }
        $sql = "INSERT INTO `externalServiceManufacturer` ({$fields}) VALUES($values)";
        $statement = $this->driver->prepare($sql);
        if (!$statement->execute()) {
            if (strpos($statement->errorInfo()[2], 'Duplicate entry') !== false) {
                return false;
            }
            throw new \Exception('Sql: ' . $sql . ' generated error:' . print_r($statement->errorInfo(), true));
        }
        return true;
    }

    public function deleteRules($serviceId)
    {
        $sql = "DELETE FROM `externalServiceManufacturer` WHERE externalServiceId = {$serviceId}";
        $statement = $this->driver->prepare($sql);
        if (!$statement->execute()) {
            throw new \Exception('Sql: ' . $sql . ' generated error:' . print_r($statement->errorInfo(), true));
        }
    }

    public function getManufacturers($serviceId)
    {
        $sql = "SELECT * FROM `externalServiceManufacturer` WHERE externalServiceId = {$serviceId}";
        $statement = $this->driver->prepare($sql);
        $statement->execute();

        return $statement->fetchAll();
    }

    public function fetchByKayakoId(int $id): array
    {
        $sql = "SELECT * FROM `{$this->tableName}` WHERE `kayakoId` = $id";
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();
        $item = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (!$item) {
            throw new NotFoundException('Entity not found, kayakoID: ' . $id);
        }

        return $item;
    }
}