<?php

namespace WeCare\ExternalService\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;

class TicketType extends MysqlCrudMapper
{

    /**
     * Manufacturer constructor.
     */
    public function __construct(\PDO $pdo)
    {
        parent::__construct($pdo, 'ticketType');
    }
}