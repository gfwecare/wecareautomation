<?php

namespace WeCare\ExternalService\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;

class Manufacturer extends MysqlCrudMapper
{

    /**
     * Manufacturer constructor.
     */
    public function __construct(\PDO $pdo)
    {
        parent::__construct($pdo, 'manufacturer');
    }
}