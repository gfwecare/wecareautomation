<?php

namespace WeCare\ExternalService\Model;

use Skeletor\Model\Model;

class Manufacturer extends Model
{
    private $manufacturerId;

    private $name;

    private $kayakoId;

    /**
     * Manufacturer constructor.
     * @param $manufacturerId
     * @param $name
     * @param $acceptTypes
     * @param $ignoreTypes
     */
    public function __construct($manufacturerId, $name, $kayakoId, $createdAt, $updatedAt)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->manufacturerId = $manufacturerId;
        $this->name = $name;
        $this->kayakoId = $kayakoId;
    }

    public function getId()
    {
        return $this->manufacturerId;
    }

    /**
     * @return mixed
     */
    public function getManufacturerId()
    {
        return $this->manufacturerId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getKayakoId()
    {
        return $this->kayakoId;
    }
}