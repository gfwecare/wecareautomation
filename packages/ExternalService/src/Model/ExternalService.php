<?php

namespace WeCare\ExternalService\Model;

use Skeletor\Model\Model;

class ExternalService extends Model
{
    protected $externalServiceId;

    protected $name;

    protected $address;

    protected $city;

    protected $zip;

    protected $phone;

    protected $kayakoId;

    private $manufacturers;

    /**
     * ExternalService constructor.
     * @param $name
     * @param $address
     * @param $city
     * @param $zip
     * @param $phone
     * @param $kayakoId
     * @param $manufacturers
     */
    public function __construct(
        $externalServiceId, $name, $address, $city, $zip, $phone, $kayakoId, $manufacturers, $createdAt, $updatedAt
    ) {
        parent::__construct($createdAt, $updatedAt);
        $this->externalServiceId = $externalServiceId;
        $this->name = $name;
        $this->address = $address;
        $this->city = $city;
        $this->zip = $zip;
        $this->phone = $phone;
        $this->kayakoId = $kayakoId;
        $this->manufacturers = $manufacturers;
    }

    public function setManufacturers($manufacturers)
    {
        $this->manufacturers = $manufacturers;
    }

    public function getRules()
    {
        return $this->manufacturers;
    }

    public function getId()
    {
        return (int) $this->externalServiceId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getKayakoId()
    {
        return $this->kayakoId;
    }

    /**
     * @return mixed
     */
    public function getManufacturers()
    {
        return $this->manufacturers;
    }



}