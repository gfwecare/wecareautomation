<?php

namespace WeCare\ExternalService\Model\ExternalService;

use Skeletor\Model\Model;

class Manufacturer //extends Model
{
    private $manufacturerId;

    private $name;

    private $acceptTypes;

    private $ignoreTypes;

    /**
     * Manufacturer constructor.
     * @param $manufacturerId
     * @param $name
     * @param $acceptTypes
     * @param $ignoreTypes
     */
//    public function __construct($manufacturerId, $name, $acceptTypes, $ignoreTypes, $createdAt, $updatedAt)
    public function __construct($manufacturerId, $name, $acceptTypes, $ignoreTypes)
    {
//        parent::__construct($createdAt, $updatedAt);
        $this->manufacturerId = $manufacturerId;
        $this->name = $name;
        $this->acceptTypes = $acceptTypes;
        $this->ignoreTypes = $ignoreTypes;
    }

    public function getId()
    {
        return $this->manufacturerId;
    }

    /**
     * @return mixed
     */
    public function getManufacturerId()
    {
        return $this->manufacturerId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAcceptTypes()
    {
        return $this->acceptTypes;
    }

    /**
     * @return mixed
     */
    public function getIgnoreTypes()
    {
        return $this->ignoreTypes;
    }
}