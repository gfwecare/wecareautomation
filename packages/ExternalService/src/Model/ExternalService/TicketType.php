<?php

namespace WeCare\ExternalService\Model\ExternalService;

use Skeletor\Model\Model;

class TicketType extends Model
{
    private $ticketTypeId;

    private $name;

    private $kayakoId;

    /**
     * Manufacturer constructor.
     * @param $manufacturerId
     * @param $name
     * @param $acceptTypes
     * @param $ignoreTypes
     */
    public function __construct($ticketTypeId, $name, $kayakoId, $createdAt, $updatedAt)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->ticketTypeId = $ticketTypeId;
        $this->name = $name;
        $this->kayakoId = $kayakoId;
    }

    public function getId()
    {
        return $this->ticketTypeId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getKayakoId()
    {
        return $this->kayakoId;
    }
}