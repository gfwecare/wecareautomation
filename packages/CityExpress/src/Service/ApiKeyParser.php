<?php
/**
 * Created by PhpStorm.
 * User: djavolak
 * Date: 5/30/2020
 * Time: 1:34 PM
 */

namespace WeCare\CityExpress\Service;


class ApiKeyParser
{
    private $keys;

    private $useTestService;

    public function __construct($keys, $useTestService = null)
    {
        $this->keys = $keys;
        $this->useTestService = $useTestService;
    }

    public function __invoke($value)
    {
        if ($this->useTestService) {
            return $this->getTestKey();
        }
        return $this->getKeyFor($value);
    }

    private function getTestKey()
    {
        return '62f6d812-cb6c-4a08-9489-06fe140b1357';
    }

    private function getKeyFor($value)
    {
        foreach ($this->keys as $type => $keyData) {
            if (isset($keyData->mapping)) {
                foreach ($keyData->mapping as $mapping) {
                    if ($mapping === 'MKA') {
                        if (false !== strpos($value, $mapping)) {
                            return $keyData->apiKey;
                        }
                    } else {
                        if ($value === $mapping) {
                            return $keyData->apiKey;
                        }
                    }
                }
            } else {
                //should be ostalo
                return $keyData->apiKey;
            }
        }
    }
}