<?php
namespace WeCare\CityExpress\Service;

use WeCare\CityExpress\Model\CollieShipment;
use WeCare\CityExpress\Model\CreateShipmentRequest;
use WeCare\CityExpress\Model\CreateShipmentPlainRequest;
use WeCare\CityExpress\Model\Response;
use WeCare\CityExpress\Model\Status;
use WeCare\CityExpress\Model\StatusList;
use Psr\Log\LoggerInterface as Logger;

class Api
{
    /**
     * @var GuzzleHttp\Client
     */
    private $httpClient;

    private $logger;


    /**
     * CityExpert_Api constructor.
     * @param \GuzzleHttp\Client $httpClient
     */
    public function __construct(\GuzzleHttp\Client $httpClient, Logger $logger)
    {
        $this->httpClient = $httpClient;
        $this->logger = $logger;
    }

    public function createShipment($apiKey, $ticketId, $ticketType, $target, $vendor)
    {
        $ref1 = $ticketId;
        $ref2 = $ticketType;
        $ref3 = $vendor;

        $senderName = 'CARE & REPAIR DOO BEOGRAD';
        $senderZip = 11000;
        $senderStreet = 'Autoput za Zagreb 41i';
        $senderCity = 'Beograd';
        //test data
        if ($apiKey === '62f6d812-cb6c-4a08-9489-06fe140b1357') {
            $senderName = 'TEST API';
            $senderStreet = 'TESTNA ADRESA BB';
        }

        $ConsigneeName = $target['name'];
        $ConsigneePostalCode = $target['zip'];
        $ConsigneeStreet  = $target['address'];
        $ConsigneeCity = $target['city'];
        $ConsigneeGsmNumber = $target['phone'];
        $ConsigneeEmailAddress = $target['email'];
        $collie = new CollieShipment($ref1, $ref2, $ref3);

//        var_dump('would create shipment from');
//        var_dump($senderName);
//        var_dump('to');
//        var_dump($ConsigneeName, $ConsigneePostalCode, $ConsigneeStreet, $ConsigneeCity);
//        die();

        $createShipmentRequest = new CreateShipmentRequest(
            $apiKey,
            [$collie],
            $ref1,
            $ref2,
            $ConsigneeName,
            $ConsigneePostalCode,
            $ConsigneeStreet,
            $ConsigneeCity,
            $ConsigneeGsmNumber,
            $ConsigneeEmailAddress,
            $senderName,
            $senderZip,
            $senderStreet,
            $senderCity,
            $ref3
        );

        $log = 'sent data to city for ' . $ref1 . PHP_EOL. PHP_EOL;
        $log .= $createShipmentRequest->toJson() . PHP_EOL. PHP_EOL;
//        echo $log;

        $response = $this->send(
            new \GuzzleHttp\Psr7\Request(
                'POST',
                'CreateShipment',
                ['Content-type' => 'application/json'],
                $createShipmentRequest->toJson()
            )
        );

        $log = 'received response for ' . $ref1 . PHP_EOL. PHP_EOL;
        $log .= $response;
//        echo $log;

        return Response::fromString($response);
    }

    public function createShipmentPlain($apiKey, $ticketId, $ticketType, $sender, $vendor, $externalService = null)
    {
        $ref1 = $ticketId;
        $ref2 = $ticketType;
        $ref3 = $vendor;
        $senderName = $sender['name'];
        $senderZip = $sender['zip'];
        $senderStreet = $sender['address'];
        $senderCity = $sender['city'];

        $ConsigneeName = 'CARE & REPAIR DOO BEOGRAD';
        $ConsigneePostalCode = 11000;
        $ConsigneeStreet  = 'Autoput za Zagreb 41i';
        $ConsigneeCity = 'Beograd';
        $ConsigneeGsmNumber = null;
        $ConsigneeEmailAddress = null;
        if ($externalService) {
            $ConsigneeName = $externalService['name'];
            $ConsigneePostalCode = $externalService['zip'];
            $ConsigneeStreet  = $externalService['address'];
            $ConsigneeCity = $externalService['city'];
        }
//        var_dump('would create shipment from');
//        var_dump($sender);
//        var_dump('to');
//        var_dump($ConsigneeName, $ConsigneePostalCode, $ConsigneeStreet, $ConsigneeCity);
//        die();

        $createShipmentRequest = new CreateShipmentPlainRequest(
            $apiKey,
            1,
            $ref1,
            $ref2,
            $ConsigneeName,
            $ConsigneePostalCode,
            $ConsigneeStreet,
            $ConsigneeCity,
            $ConsigneeGsmNumber,
            $ConsigneeEmailAddress,
            'RS',
            $senderName,
            $senderZip,
            $senderStreet,
            $senderCity,
            $ref3
        );

        $log = 'sent data to city for ' . $ref1 . PHP_EOL. PHP_EOL;
        $log .= $createShipmentRequest->toJson() . PHP_EOL. PHP_EOL;
//        echo $log;

        $response = $this->send(
            new \GuzzleHttp\Psr7\Request(
                'POST',
                'CreateShipmentPlain',
                ['Content-type' => 'application/json'],
                $createShipmentRequest->toJson()
            )
        );

        $log = 'received response for ' . $ref1 . PHP_EOL. PHP_EOL;
        $log .= $response;
//        echo $log;

        return Response::fromString($response);
    }

    public function getAvailableTimeSlots($pickupZip, $deliveryZip, $apiKey)
    {
        $uriPattern = 'GetPossibleDateTimesForShipments?ApiKey=%s&PickupPostalCode=%s&DeliveryPostalCode=%s';
        $uri = sprintf($uriPattern, $apiKey, $pickupZip, $deliveryZip);

        return $this->send(new \GuzzleHttp\Psr7\Request('GET', $uri));
    }

    public function searchLocation($string, $apiKey)
    {
        $uriPattern = 'GetLocations?ApiKey=%s&PostalCodeOrName=%s';
        $uri = sprintf($uriPattern, $apiKey, $string);

        return $this->send(new \GuzzleHttp\Psr7\Request('GET', $uri));
    }

    public function requestPickup($apiKey, $shipmentId = null)
    {
        $data = ['ApiKey' => $apiKey];
        if ($shipmentId) {
            $uri = 'RequestPickupForShipments';
            $data['ShipmentIds'] = [$shipmentId];
        } else {
            $uri = 'RequestPickup';
        }
        $headers = ['Content-type' => 'application/json'];

        return json_decode($this->send(new \GuzzleHttp\Psr7\Request('POST', $uri, $headers, json_encode($data))));
    }

    public function getShippingLabels($apiKey, $shipmentId = null)
    {
        $uri = sprintf('GetShippingLabelsForSingleShipment?ApiKey=%s&ShipmentId=%s', $apiKey, $shipmentId);
        if (!$shipmentId) { // activates all shipments
            $uri = sprintf('GetShippingLabelsForAllShipments?ApiKey=%s', $apiKey);
        }

        return json_decode($this->send(new \GuzzleHttp\Psr7\Request('POST', $uri, ['Content-type' => 'application/json'])));
    }

    public function getActiveShipments($apiKey)
    {
        return $this->send(
            new \GuzzleHttp\Psr7\Request('GET', sprintf('GetActiveShipments?ApiKey=%s', $apiKey))
        );
    }

    /**
     * @return StatusList
     */
    public function getShipmentStatusByRef($ref, $apiKey, $shipmentCode = false)
    {
        $uriPattern = 'GetShipmentStatusByAnyRefAndPickupDateSimple?ApiKey=%s&RefType=1&refValue=%s&PickupDate=%s';
        $dt = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));
        $dt->modify('-30 day');
        $offset = 40;
        $currentDay = (int) $dt->format('j');
        $ids = [];
        for ($i=$currentDay; $i<($currentDay+$offset); $i++) {
            $date = $dt->format('Y-m-d');
//            echo $date . PHP_EOL;
            $uri = sprintf($uriPattern, $apiKey, urlencode($ref), urlencode($date."T00:00:00"));
            try {
                $response = json_decode($this->send(new \GuzzleHttp\Psr7\Request('GET', $uri)));
            } catch (\Exception $e) {
//                var_dump($e->getMessage());
//                die();
            }
            if (isset($response->Shipment->CargoNetShipmentId)) {
                if ($shipmentCode && $shipmentCode == $response->Shipment->CargoNetShipmentId) {
                    return $this->parseStatuses($response, $uri, @$response->Shipment->DateCreated);
                }
                if (!in_array($response->Shipment->CargoNetShipmentId, $ids)) {
                    $ids[] = $response->Shipment->CargoNetShipmentId;
                }
//                var_dump($response->Shipment->CargoNetShipmentId);
//                die();
//                return $this->parseStatuses($response, $uri, @$response->Shipment->DateCreated);
            }
            $dt->modify('+1 day');
        }
        if (count($ids) === 0) {
            throw new \Exception('not found 30+' . $ref);
            var_dump('status not found: ' . $ref);
            die();
        }
//        var_dump($ids[count($ids) - 1]);
//        var_dump($response->Shipment->CargoNetShipmentId);
        return $this->parseStatuses($response, $uri, @$response->Shipment->DateCreated);

//        $uri = 'GetShipmentStatusByAnyRefAndPickupDateSimple?ApiKey=%s&RefType=1&refValue=%s';
//        $uri = 'GetShipmentStatusByAnyRef?ApiKey=%s&RefType=1&refValue=%s';
//        echo $uri = sprintf($uri, $apiKey, urlencode($ref));
//        try {
//            $response = json_decode($this->send(new \GuzzleHttp\Psr7\Request('GET', $uri), $debug));
//        } catch (\Exception $e) {
//            if ($e->getMessage() === 'Unknown error') {
//                $uri = 'GetShipmentStatusByAnyRefAndPickupDateSimple?ApiKey=%s&RefType=1&refValue=%s';
//                $uri = sprintf($uri, $apiKey, urlencode($ref . '#1'));
//                $response = json_decode($this->send(new \GuzzleHttp\Psr7\Request('GET', $uri), $debug));
//            } else {
//                throw $e;
//            }
//        }
//        var_dump($response->Shipment);
//        var_dump($response);
//        die();
//
//        return $this->parseStatuses($response, $uri, $response->Shipment->DateCreated);
    }

    /**
     * @param $response
     * @param $debug string Used to get additional data during status inspection
     * @return StatusList
     */
    private function parseStatuses($response, $debug, $dateCreated)
    {
        return new StatusList($response->Shipment->Traces, $response->Shipment->CargoNetShipmentId, $debug, $dateCreated);
    }

    public function getShipmentApiStatus($shipmentId, $apiKey)
    {
        $uri = sprintf('GetShipmentStatusByShipmentId?ApiKey=%s&ShipmentId=%s', $apiKey, $shipmentId);

        $response =  json_decode(
            $this->send(new \GuzzleHttp\Psr7\Request('GET', $uri))
        );
//        var_dump($response->Shipment->CargoNetShipmentId);
//        var_dump($response->Shipment->Collies);
//        var_dump($response->Shipment);
//        var_dump($response->Shipment->Traces);
//        die();

        return $this->parseStatuses($response, $uri, $response->Shipment->DateCreated);
    }

    private function send(\GuzzleHttp\Psr7\Request $request, $debug = false)
    {
//        $debug = true;
        if ($debug) {
            echo 'sending request:' . PHP_EOL;
            echo $request->getUri() . PHP_EOL;
            echo $request->getBody()->getContents() . PHP_EOL;
        }
        $msg = 'sending request:' . PHP_EOL;
        $msg .= $request->getUri() . PHP_EOL;
        $msg .= $request->getBody()->getContents() . PHP_EOL;
        $this->logger->debug($msg);

        try {
            $response = $this->httpClient->send($request);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $r = $e->getResponse()->getBody()->getContents();
            $this->logger->debug('RESPONSE: ' . $r);
            return $this->handleInvalidResponse(json_decode($r), $request, $e->getMessage());
        }

        $response = $response->getBody()->getContents();
        $this->logger->debug('RESPONSE: ' . $response);
        $res = json_decode($response);
        if (!$res->IsValid) { // should not be after implementation ?
            return $this->handleInvalidResponse($res, $request, '');
        }
        if (is_object($response)) {
            var_dump('invalid response would be passed:');
            var_dump($response);
            die();
        }

        return $response;
    }

    private function handleInvalidResponse($response, \GuzzleHttp\Psr7\Request $request, $errorMessage)
    {
        if (isset($response->ValidationErrors[0]) && $response->ValidationErrors[0] === 10010) {
            $request->getBody()->rewind();
            $pattern = 'Order %s has shipments already created.';
            throw new \Exception(sprintf($pattern, json_decode($request->getBody()->getContents())->Ref1));
        }
        if (isset($response->ValidationErrors[0]) && $response->ValidationErrors[0] === 10008) {
            $request->getBody()->rewind();
            $pattern = 'Order %s has invalid address combination.';
            throw new \Exception(sprintf($pattern, json_decode($request->getBody()->getContents())->Ref1));
        }
        if (isset($response->ValidationErrors[0]) && $response->ValidationErrors[0] === 10009) {
            $request->getBody()->rewind();
            $pattern = 'SenderPostalCodeAndCityInvalidCombination';
            throw new \Exception($pattern);
        }
        if (isset($response->ValidationErrors[0]) && $response->ValidationErrors[0] === 20000) {
            var_dump($response);
            $request->getBody()->rewind();
            $pattern = 'Unknown error';
            throw new \Exception($pattern);
        }
        if (isset($response->ValidationErrors[0]) && $response->ValidationErrors[0] === 10020) {
            var_dump($response);
            $request->getBody()->rewind();
            $pattern = 'Unknown error';
            throw new \Exception($pattern);
        }
        if (isset($response->ValidationErrors[0]) && $response->ValidationErrors[0] === 10019) {
            $request->getBody()->rewind();
            $pattern = 'NoShipmentsForPickupFound';
            throw new \Exception($pattern);
        }
        if (isset($response->ValidationErrors[0]) && $response->ValidationErrors[0] === 10032) {
            $request->getBody()->rewind();
            $pattern = 'NoPostalLocationsFound';
            throw new \Exception($pattern);
        }
        if (isset($response->ValidationErrors[0]) && $response->ValidationErrors[0] === 10007) {
            $msg = sprintf('Shipment not found. Additional data: %s', $request->getUri()->getQuery());
            throw new \Exception($msg);
        }

        echo 'Service returned errors: ' . PHP_EOL;
        echo $errorMessage . PHP_EOL;
        var_dump($response->ValidationErrors);
        var_dump($response->ModelErrors);
        var_dump($response->Warnings);

        die();
//        die();

        return $response;
    }
}