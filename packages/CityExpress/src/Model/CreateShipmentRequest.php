<?php

namespace WeCare\CityExpress\Model;


class CreateShipmentRequest
{
    private $ApiKey;

    /**
     * @var array List of collies contained in this shipment.
     */
    private $Collies;

    /**
     * Value of this field must be unique on a single date.
     * Max length 35.
     *
     * @var string will be printed on shipping label
     */
    private $Ref1;

    /**
     * Max length 35.
     *
     * @var string will be printed on shipping label
     */
    private $Ref2;

    /**
     * Max length 35.
     *
     * @var string will be printed on shipping label
     */
    private $Ref3;

    /**
     * Max length 35.
     *
     * @var string
     */
    private $ConsigneeName;

    /**
     * Max length 10.
     *
     * @var string
     */
    private $ConsigneePostalCode;

    /**
     * Max length 70.
     *
     * @var string
     */
    private $ConsigneeStreet;

    /**
     * Max length 35.
     *
     * @var string
     */
    private $ConsigneeCity;

    /**
     * Max length 3.
     *
     * @var string
     *
     **/
    private $ConsigneeCountryPrefix = 'RS';

    /**
     * @var bool
     */
    private $AllowSaturdayDelivery;

    /**
     * Indicates until when the shipment should be delivered. See ExpressTypeEnum for allowed values.
     * 21 - Delivery until 10:30; 22 - Delivery until 8:30, 100 - Delivery until end of business day
     *
     * @var int
     */
    private $ExpressType;

    /**
     * 0 - none; 1 - auto email, 2 - auto sms, 3 both
     *
     * @var int
     */
    private $NotificationType;

    /**
     * Description of delivery usually printed on shipping label.
     *
     * @var string
     */
    private $RemarkDelivery;

    /**
     * Description of pickup usually printed on shipping label.
     *
     * @var string
     */
    private $RemarkPickup;

    /**
     * Flag indicating if the shipment is consisted of pallets instead of boxes.
     *
     * @var bool
     */
    private $IsCargo;

    /**
     * Flag indicating whether return document exists that is collected upon delivery.
     *
     * @var bool
     */
    private $ReturnDocument;



    /**
     * PDF document that is merged with the shipping label PDF as a new page.
     * When shipping labels are fetched with later API calls this document is returned as a part of the shipping label.
     *
     * @var string base64 ?
     */
    private $ShipmentDocumentData;

    /**
     * Extension of the document. For now only "PDF" value is allowed. Must be "PDF" if ShipmentDocumentData is not null.
     *
     * @var string
     */
    private $ShipmentDocumentExtension;

    /**
     * Cash on delivery amount.
     *
     * @var double
     */
    private $CodAmount;

    /**
     * 0 - local, 1- euro
     *
     * @var int
     */
    private $CodCurrency;

    /**
     * If shipment is to be picked up at Parcel shop, Parcel shop id is passed in this field.
     *
     * @var string
     */
    private $DropOffParcelShopId;

    /**
     * If shipment is to be delivered to Parcel shop, Parcel shop id is passed in this field.
     *
     * @var string
     */
    private $DeliveryParcelShopId;

    /**
     * Null value means that the sender pays for shipping. See ExWorksTypeEnum for allowed values.
     * 4 - Consignee pays for the shipping; 44 - Shipping fee is prepaid     *
     *
     * @var int
     */
    private $ExWorksType;

    /**
     * Max length 30.
     *
     * @var string
     */
    private $ConsigneeTelephoneNumber;

    /**
     * Max length 30.
     *
     * @var string
     */
    private $ConsigneeGsmNumber;

    /**
     * Max length 70.
     *
     * @var string
     */
    private $ConsigneeEmailAddress;

    /**
     * @var string Description of goods being sent.
     */
    private $RemarkGoods;

    /**
     * @var double Weight of the whole shipment in kilograms.
     */
    private $ShipmentWeight;

    private $PickupStart;
    private $PickupEnd;

    private $DeliveryStart;
    private $DeliveryEnd;

    private $SenderName;

    private $SenderPostalCode;

    private $SenderStreet;

    private $SenderCity;

    private $SenderCountryPrefix;


    private $ShipperPrintsLabels;

    /**
     * CityExpert_CreateShipment_Request constructor.
     * @param string $ApiKey
     * @param array $Collies
     * @param string $Ref1
     * @param string $Ref2
     * @param string $Ref3
     * @param string $ConsigneeName
     * @param string $ConsigneePostalCode
     * @param string $ConsigneeStreet
     * @param string $ConsigneeCity
     * @param string $ConsigneeCountryPrefix
     * @param bool $AllowSaturdayDelivery
     * @param int $ExpressType
     * @param int $NotificationType
     * @param string $RemarkDelivery
     * @param string $RemarkPickup
     * @param bool $IsCargo
     *
     * @param bool $ReturnDocument
     * @param string $ShipmentDocumentData
     * @param string $ShipmentDocumentExtension
     * @param float $CodAmount
     * @param int $CodCurrency
     * @param string $DropOffParcelShopId
     * @param string $DeliveryParcelShopId
     * @param int $ExWorksType
     * @param string $ConsigneeTelephoneNumber
     * @param string $ConsigneeGsmNumber
     * @param string $ConsigneeEmailAddress
     * @param string $RemarkGoods
     * @param float $ShipmentWeight
     * @param $PickupStart
     * @param $PickupEnd
     * @param $DeliveryStart
     * @param $DeliveryEnd
     */
    public function __construct(
        $ApiKey, array $Collies, $Ref1, $Ref2, $ConsigneeName, $ConsigneePostalCode, $ConsigneeStreet, $ConsigneeCity,
        $ConsigneeGsmNumber = null, $ConsigneeEmailAddress = null,
        $SenderName = '', $SenderPostalCode = '', $SenderStreet = '', $SenderCity = '', $Ref3 = null,
        $ConsigneeCountryPrefix = 'RS', $SenderCountryPrefix = 'RS', $RemarkDelivery = null, $RemarkPickup = null,
        bool $AllowSaturdayDelivery = false, $ExpressType = 100, $NotificationType = 3, $IsCargo = false,
        $ReturnDocument = false, $ShipmentDocumentData = null, $ShipmentDocumentExtension = null, $CodAmount = null,
        $CodCurrency = null, $DropOffParcelShopId = null, $DeliveryParcelShopId = null, $ExWorksType = null,
        $ConsigneeTelephoneNumber = null, $RemarkGoods = null, $ShipmentWeight = null, $PickupStart = null,
        $PickupEnd = null, $DeliveryStart = null, $DeliveryEnd = null, $ShipperPrintsLabels = true)
    {
        $this->ApiKey = $ApiKey;
        $this->Collies = $Collies;
        $this->Ref1 = $Ref1;
        $this->Ref2 = $Ref2;
        $this->Ref3 = $Ref3;
        $this->ConsigneeName = $ConsigneeName;
        $this->ConsigneePostalCode = $ConsigneePostalCode;
        $this->ConsigneeStreet = $ConsigneeStreet;
        $this->ConsigneeCity = $ConsigneeCity;
        $this->ConsigneeCountryPrefix = $ConsigneeCountryPrefix;
        $this->SenderCity = $SenderCity;
        $this->SenderName = $SenderName;
        $this->SenderCountryPrefix = $SenderCountryPrefix;
        $this->SenderPostalCode = $SenderPostalCode;
        $this->SenderStreet = $SenderStreet;
        $this->AllowSaturdayDelivery = $AllowSaturdayDelivery;
        $this->ExpressType = $ExpressType;
        $this->NotificationType = $NotificationType;
        $this->RemarkDelivery = $RemarkDelivery;
        $this->RemarkPickup = $RemarkPickup;
        $this->IsCargo = $IsCargo;
        $this->ReturnDocument = $ReturnDocument;
        $this->ShipmentDocumentData = $ShipmentDocumentData;
        $this->ShipmentDocumentExtension = $ShipmentDocumentExtension;
        $this->CodAmount = $CodAmount;
        $this->CodCurrency = $CodCurrency;
        $this->DropOffParcelShopId = $DropOffParcelShopId;
        $this->DeliveryParcelShopId = $DeliveryParcelShopId;
        $this->ExWorksType = $ExWorksType;
        $this->ConsigneeTelephoneNumber = $ConsigneeTelephoneNumber;
        $this->ConsigneeGsmNumber = $ConsigneeGsmNumber;
        $this->ConsigneeEmailAddress = $ConsigneeEmailAddress;
        $this->RemarkGoods = $RemarkGoods;
        $this->ShipmentWeight = $ShipmentWeight;
        $this->PickupStart = $PickupStart;
        $this->PickupEnd = $PickupEnd;
        $this->DeliveryStart = $DeliveryStart;
        $this->DeliveryEnd = $DeliveryEnd;
        $this->ShipperPrintsLabels = false;
    }

    public function toArray()
    {
        $data = [];
        foreach (get_object_vars($this) as $prop => $value) {
            if ($prop === 'Collies') {
                /* @var CollieShipment $propObject */
                foreach ($value as $key => $propObject) {
                    foreach ($propObject->toArray() as $subProp => $subValue) {
                        $data[$prop][$key][$subProp] = $subValue;
                    }
                }
            } else {
                $data[$prop] = $value;
            }
        }

        return $data;
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }
}