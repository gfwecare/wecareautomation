<?php

namespace WeCare\CityExpress\Model;


class StatusList
{
    // @TODO add these to mail notice
    const STATUS_LOG_NOTICE = [134, 120, 110, 77];
    const STATUS_DELIVERED = [40, 70, 134]; // 134 Dostavljena povratna pošiljka
    const STATUS_CANCELLED = [76, 120, 78, 100, 80, 140]; // 120 Mesto preuzimanja zatvoreno, 78 Nepoznata adresa pošiljaoca, 100 odbijeno, 80 nepoznata adresa, 140 Godišnji odmor
    const STATUS_IN_TRANSIT = [44, 30, 10, 20, 60, 65, 130, 50, 90, 131]; // 130 odustan primalac, 50 Vraceno u lokalni centar, 90 nedostatak vremena, 131 odlozena isporuka (primalac)
    const STATUS_IGNORE = [260, 0, 37]; //0 Kreirano preuzimanje na drugoj adresi, 37 Kreirano preuzimanje na drugoj adresi u toku dana

    private $statuses = [];

    private $inTransit = false;

    private $delivered = false;

    private $cancelled = false;

    private $logNotice = false;

    private $deliveryDate = false;

    private $createDate = false;

    private $cargoNetShipmentId;

    private $tmpKnownStatuses = [44, 10, 30, 40, 260, 20, 60, 70, 65, 76, 130, 50, 120, 134, 110, 77, 78, 100, 140, 80, 0, 37];

    public function __construct($statuses, $cargoNetShipmentId, $debug, $dateCreated = null)
    {
        $this->parseStatuses($statuses, $debug);
//        if (substr($cargoNetShipmentId, 0, 2) !== "00") {
//            $cargoNetShipmentId = substr($cargoNetShipmentId, 6);
//        }
        if (strlen($cargoNetShipmentId) < 6 || $cargoNetShipmentId == '00000000') {
//            var_dump($statuses);
//            var_dump($cargoNetShipmentId);
//            die();
            throw new \Exception('strange barcode: ' . $cargoNetShipmentId);
        }
        if (strlen($cargoNetShipmentId) > 8) {
            $cargoNetShipmentId = str_replace('688001', '', $cargoNetShipmentId);
        }
        $this->cargoNetShipmentId = $cargoNetShipmentId;
        $this->createDate = $dateCreated;
        $this->checkNoStatuses();
    }

    /**
     * If city has not updated status for two days, shipment is considered cancelled
     */
    private function checkNoStatuses()
    {
        if (count($this->statuses) === 0) {
            $dt = \DateTime::createFromFormat('Y-m-d', explode('T', $this->createDate)[0]);
            $now = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));
            if ($now->diff($dt)->invert === 1 && $now->diff($dt)->d > 2) {
                $this->cancelled = true;
            }
        }
    }

    private function parseStatuses($statuses, $debug)
    {
        foreach ($statuses as $data) {
            if (!in_array((int) $data->StatusNumber, $this->tmpKnownStatuses)) {
                throw new \Exception('New status: ' . serialize($data));
            }
            if (!in_array($data->StatusNumber, self::STATUS_IGNORE) && in_array($data->StatusNumber, self::STATUS_CANCELLED)) {
                $this->cancelled = true;
            }
            if (!in_array($data->StatusNumber, self::STATUS_IGNORE) && in_array($data->StatusNumber, self::STATUS_LOG_NOTICE)) {
                $this->logNotice = true;
            }
            if (!in_array($data->StatusNumber, self::STATUS_IGNORE) && in_array($data->StatusNumber, self::STATUS_IN_TRANSIT)) {
                $this->inTransit = true;
            }
            if (!in_array($data->StatusNumber, self::STATUS_IGNORE) && in_array($data->StatusNumber, self::STATUS_DELIVERED)) {
                $this->delivered = true;
                $this->inTransit = true;
                $this->deliveryDate = explode('T', $data->ScanDate)[0];
            }
            $this->statuses[] = new Status($data->StatusNumber, $data->StatusDescription, $data->ScanDate);
        }
    }

    /**
     * In transit stays forever when picked up by courier
     *
     * @return mixed
     */
    public function isInTransit()
    {
        return $this->inTransit;
    }

    public function getStatusList()
    {
        return $this->statuses;
    }

    /**
     * True when api returns proper status
     *
     * @return bool
     */
    public function isDelivered()
    {
        return $this->delivered;
    }

    public function shouldLog()
    {
        return $this->logNotice;
    }

    public function isCancelled()
    {
        return $this->cancelled;
    }

    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    public function getBarcode()
    {
        return $this->cargoNetShipmentId;
    }

    public function hasStatuses()
    {
        return count($this->statuses) > 0;
    }
}