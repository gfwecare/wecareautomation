<?php

namespace WeCare\CityExpress\Model;


class Response
{
    private $CreatedShipmentId;
    private $Collies;
    private $PickupStart;
    private $PickupEnd;
    private $DeliveryStart;
    private $DeliveryEnd;
    /*
     * not used
     * @var array [net, gross]
     */
    private $PriceInformation;
    private $ExpectedSenderAddress;
    private $IsValid;
    private $ModelErrors;
    private $ValidationErrors;

    /**
     * CityExpert_CreateShipmentResponse constructor.
     * @param $CreatedShipmentId
     * @param $Collies
     * @param $PickupStart
     * @param $PickupEnd
     * @param $DeliveryStart
     * @param $DeliveryEnd
     * @param $PriceInformation
     * @param $ExpectedSenderAddress
     * @param $IsValid
     * @param $ModelErrors
     * @param $ValidationErrors
     */
    public function __construct(
        $CreatedShipmentId,
        $Collies,
        $PickupStart,
        $PickupEnd,
        $DeliveryStart,
        $DeliveryEnd,
        $PriceInformation,
        $ExpectedSenderAddress,
        $IsValid,
        $ModelErrors,
        $ValidationErrors
    ) {
        $this->CreatedShipmentId = $CreatedShipmentId;
        $this->Collies = $Collies;
        $this->PickupStart = $PickupStart;
        $this->PickupEnd = $PickupEnd;
        $this->DeliveryStart = $DeliveryStart;
        $this->DeliveryEnd = $DeliveryEnd;
        $this->PriceInformation = $PriceInformation;
        $this->ExpectedSenderAddress = $ExpectedSenderAddress;
        $this->IsValid = $IsValid;
        $this->ModelErrors = $ModelErrors;
        $this->ValidationErrors = $ValidationErrors;
    }

    public static function fromString($response)
    {
        if (!is_object($response)) {
            $response = json_decode($response);
        }
        // $response = json_decode($response);
        return new self(
            $response->CreatedShipmentId,
            $response->Collies,
            $response->PickupStart,
            $response->PickupEnd,
            $response->DeliveryStart,
            $response->DeliveryEnd,
            $response->PriceInformation,
            $response->ExpectedSenderAddress,
            $response->IsValid,
            $response->ModelErrors,
            $response->ValidationErrors
        );
    }

    /**
     * @return mixed
     */
    public function getCreatedShipmentId()
    {
        return $this->CreatedShipmentId;
    }

    /**
     * @return mixed
     */
    public function getCollies()
    {
        return $this->Collies;
    }

    /**
     * @return mixed
     */
    public function getPickupStart()
    {
        return $this->PickupStart;
    }

    /**
     * @return mixed
     */
    public function getPickupEnd()
    {
        return $this->PickupEnd;
    }

    /**
     * @return mixed
     */
    public function getDeliveryStart()
    {
        return $this->DeliveryStart;
    }

    /**
     * @return mixed
     */
    public function getDeliveryEnd()
    {
        return $this->DeliveryEnd;
    }

    /**
     * @return mixed
     */
    public function getPriceInformation()
    {
        return $this->PriceInformation;
    }

    /**
     * @return mixed
     */
    public function getExpectedSenderAddress()
    {
        return $this->ExpectedSenderAddress;
    }

    /**
     * @return mixed
     */
    public function getisValid()
    {
        return $this->IsValid;
    }

    /**
     * @return mixed
     */
    public function getModelErrors()
    {
        return $this->ModelErrors;
    }

    /**
     * @return mixed
     */
    public function getValidationErrors()
    {
        return $this->ValidationErrors;
    }
}