<?php

namespace WeCare\CityExpress\Model;


class StatusListStub
{
    private $inTransit;

    private $delivered;

    private $cancelled;

    private $dt;

    public function __construct($delivered, $inTransit, $cancelled, \DateTime $dt)
    {
        $this->delivered = $delivered;
        $this->inTransit = $inTransit;
        $this->cancelled = $cancelled;
        $this->dt = $dt;
    }

    /**
     * basically we should be checking when not delivered, but checkout city service behavior
     *
     * @return mixed
     */
    public function isInTransit()
    {
        return $this->inTransit;
    }

    public function shouldLog()
    {
        return false;
    }

    public function isDelivered()
    {
        return $this->delivered;
    }

    public function isCancelled()
    {
        return $this->cancelled;
    }

    public function getDeliveryDate()
    {
        return $this->dt->format('Y-m-d');
    }
}