<?php

namespace WeCare\CityExpress\Model;


class Status
{
    private $id;

    private $name;

    private $date;

    /**
     * Status constructor.
     * @param $id
     * @param $name
     * @param $date
     */
    public function __construct($id, $name, $date)
    {

        $this->id = $id;
        $this->name = $name;
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }
}