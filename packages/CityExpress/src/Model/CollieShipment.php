<?php

namespace WeCare\CityExpress\Model;


class CollieShipment
{
    /**
     * Unique Colli identifier should be passed in this field. It is later printed on the shipping label
     *
     * @var string
     */
    private $Ref1;

    /**
     * @var string
     */
    private $Ref2;

    /**
     * @var string
     */
    private $Ref3;

    /**
     * Description of goods being sent
     *
     * @var string
     */
    private $RemarkGoods;


    /**
     * @var Double
     */
    private $ColliWeight;

    /**
     * @var string
     */
    private $BarcodeCustomer;

    /**
     * base64 encoded pdf document
     *
     * @var string
     */
    private $ColliDocumentData;

    /**
     * @var string
     */
    private $ColliDocumentExtension;

    /**
     * CityExpert_Collie_Shipment constructor.
     * @param string $Ref1
     * @param string $Ref2
     * @param string $Ref3
     * @param string $RemarkGoods
     * @param float $ColliWeight
     * @param string $BarcodeCustomer
     * @param string $ColliDocumentData
     * @param string $ColliDocumentExtension
     * @param bool $IsReturnColli
     */
    public function __construct(string $Ref1, string $Ref2, $Ref3 = null, $RemarkGoods = null, $ColliWeight = null, $BarcodeCustomer = null,
                $ColliDocumentData = null, $ColliDocumentExtension = null, $IsReturnColli = null
    ) {
        $this->Ref1 = $Ref1;
        $this->Ref2 = $Ref2;
        $this->Ref3 = $Ref3;
        $this->RemarkGoods = $RemarkGoods;
        $this->ColliWeight = $ColliWeight;
        $this->BarcodeCustomer = $BarcodeCustomer;
        $this->ColliDocumentData = $ColliDocumentData;
        $this->ColliDocumentExtension = $ColliDocumentExtension;
    }

    public function toArray()
    {
        $data = [];
        foreach (get_object_vars($this) as $prop => $value) {
            $data[$prop] = $value;
        }

        return $data;
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }
}