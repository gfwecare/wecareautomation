// js for frontend
// Define Hamburger Bars, Mobile menu(overlay),X(close menu) and the body element
// const hamburgerBars = document.querySelector('.headerHamburgerMenu');
// const hamburgerOverlay = document.querySelector('.headerHamburgerMenuOverlay');
const closeMenu = document.querySelector('.exitMenu');
const body = document.getElementsByTagName("body")[0];
const headerTop = document.querySelector('.headerTop');


/* By clicking on the hamburger bars, the menu toggles the inactive class off (shows the menu), and hides the bars,
unscrollable On, remove header margin */

// hamburgerBars.onclick = function() {
//     hamburgerOverlay.classList.toggle('inactive');
//     hamburgerBars.classList.add('inactive');
//     body.classList.add('stopScrolling');
//     headerTop.style.marginTop = '0px';
//
// }

/* By clicking on the X(close menu), the inactive class is toggled on (hides the menu) and shows the bars,
unscrollable Off, add header margin */
// closeMenu.onclick = function() {
//     hamburgerOverlay.classList.toggle('inactive');
//     hamburgerBars.classList.remove('inactive');
//     body.classList.remove('stopScrolling');
//     headerTop.style.marginTop = '26px';
// }

$(document).ready(function() {
    let userId = $('#userId').attr('data-userId');

    //multilevel menu
    // $(function(){
    //     $(".dropdown-menu > li > a.trigger").on("click",function(e){
    //         var current=$(this).next();
    //         var grandparent=$(this).parent().parent();
    //         if($(this).hasClass('left-caret')||$(this).hasClass('right-caret'))
    //             $(this).toggleClass('right-caret left-caret');
    //         grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
    //         grandparent.find(".sub-menu:visible").not(current).hide();
    //         current.toggle();
    //         e.stopPropagation();
    //     });
    //     $(".dropdown-menu > li > a:not(.trigger)").on("click",function(){
    //         var root=$(this).closest('.dropdown');
    //         root.find('.left-caret').toggleClass('right-caret left-caret');
    //         root.find('.sub-menu:visible').hide();
    //     });
    // });

    //check all
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });

    // extend original append event
    (function($) {
        var origAppend = $.fn.append;

        $.fn.append = function () {
            return origAppend.apply(this, arguments).trigger("append");
        };
    })(jQuery);

    //moment.js initialize
    // moment().format();


    //default data table

    //Users
    $('#usersTable').DataTable();

    //Banks
    $('#banksTable').DataTable();

    //Report
   $('#reportTable').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url':'/report/ajaxHandle/',
            "type": "POST"
        },
        'columns': [
            { data: 'bankId' },
            { data: 'pointOfSaleId' },
            { data: 'transactionDate' },
            { data: 'value' },
            { data: 'netValue' },
            { data: 'provision' },
            { data: 'additionalProvision' },
            { data: 'installments' },
            { data: 'reportDate' },
        ]
    });
    //Pos
    $('#posTableView').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url':'/pos/ajaxHandle/'+userId+'/',
            "type": "POST"
        },
        'columns': [
            { data: 'name' },
            { data: 'address' },
            { data: 'isActive' },
            { data: 'createdAt' },
            { data: 'updatedAt' },
            { data: 'action' },
        ],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]
    });
    $('#posTableConfigure').DataTable({
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });


    $('.datepicker').datepicker({
        dateFormat: 'd/m/yy'
    });

    // new js
    $('.addManufacturerTemplate').click(function(e) {
        e.preventDefault();
        itemCount++;
        console.log(itemCount);
        $("#manufacturers").append($('#manufacturerTemplate').html().replaceAll('#count', itemCount));
    });
    $('#manufacturers').on('click', '.removeManufacturerTemplate', function(e) {
        e.preventDefault();
        $(this).parent().remove();
    });

});

