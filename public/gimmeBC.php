<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

include("../vendor/autoload.php");

if (!isset($_GET{"valueBC"})) {
    echo 'forgot something ?';
    exit();
}

use Laminas\Barcode\Barcode;

$barcodeOptions = [
    'text' => $_GET{"valueBC"},
    'fontSize' => 10,
    'barHeight' => 30,
    'factor' => 2,
//    'barThinWidth' => 1,
//    'barThickWidth' => 3,
];
$rendererOptions = [];
$renderer = Barcode::factory(
    'code128',
    'image',
    $barcodeOptions,
    $rendererOptions
);
$renderer->render();
