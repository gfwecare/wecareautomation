<?php

use Psr\Log\LoggerInterface;
use Skeletor\App\WebSkeletor;

define('APP_PATH', __DIR__ . '/..');
define('DATA_PATH', __DIR__ . '/../data');

error_reporting(E_ALL);
ini_set('display_errors', 1);

include("../vendor/autoload.php");

try {
    /* @var \DI\Container $container */
    $container = require APP_PATH . '/config/bootstrap.php';
    $app = new WebSkeletor($container, $container->get(LoggerInterface::class));
    $app->respond();
} catch (\Exception $e) {
    if ($app) {
        $app->handleErrors($e);
        exit();
    }
    // @TODO handle better
    var_dump($e->getTrace());
    var_dump('There was an unknown error with application. More info: ' . $e->getMessage());
    exit();
}

