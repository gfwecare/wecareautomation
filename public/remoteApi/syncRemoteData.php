<?php

ini_set('display_errors', '1');
error_reporting(E_ALL);

if (!isset($_GET['customAction'])) {
    die();
}

$dbHost = 'localhost';
$dbName = 'korisnickapodrska';
$dbUser = 'root';
$dbPass = 'OvoJ3NoviP@ss!';

$action = $_GET['customAction'];
$data = array(
    'status' => false,
    'value' => false,
);

try {
    $dsn = "mysql:host={$dbHost};dbname={$dbName}";
    $pdo = new \PDO($dsn, $dbUser, $dbPass, [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']);

    switch ($action) {
        case 'getFieldData':

            $data = array(
                'status' => true,
                'value' => getCustomFieldValue((int) $_GET['ticketId'], (int) $_GET['fieldId']),
            );

            break;

        case 'setFieldData':
            $logData = array(
                'departmentId' => (int) $_GET['departmentId'],
                'ticketId' => (int) $_GET['ticketId'],
                'fieldId' => (int) $_GET['fieldId'],
                'value' => $_GET['value'],
                'oldValue' => $_GET['oldValue']
            );
            $data['status'] = setCustomFieldValue((int) $_GET['ticketId'], (int) $_GET['fieldId'], $_GET['value']) &&
                writeAuditLog($logData);

            break;

        default:
            //uncomment after tested
//        die();

            break;
    }
} catch (\Exception $e) {
    $data['value'] = $e->getMessage();
}

echo json_encode($data);
exit();




function getCustomFieldValue($ticketId, $fieldId) {
    global $pdo;

    $statement = $pdo->prepare("SELECT fieldvalue FROM `swcustomfieldvalues` WHERE `typeid` = :typeid AND 
      `customfieldid` = :customfieldid");
    $statement->bindValue(':typeid', $ticketId);
    $statement->bindValue(':customfieldid', $fieldId);
    if (!$statement->execute()) {
        throw new \Exception(sprintf('could not get value for field %s and ticket %s', $fieldId, $ticketId));
    }
    $data = $statement->fetch(\PDO::FETCH_ASSOC);

    return $data['fieldvalue'];
}

function setCustomFieldValue($ticketId, $fieldId, $value) {
    global $pdo;

    $pdo->beginTransaction();
    if (strlen($value) === 0) {
        throw new \Exception(sprintf(
            'Tried to save empty value for ticket %s and custom fieldid %s.', $ticketId, $fieldId
        ));
    }
    setFieldValue($ticketId, $fieldId, $value);

    return true;
}

function setFieldValue($ticketId, $fieldId, $value) {
    global $pdo;

    $sql = " WHERE `typeid` = :typeid AND `customfieldid` = :customfieldid";
    $statement = $pdo->prepare("SELECT * FROM `swcustomfieldvalues` " . $sql);
    $statement->bindValue(':typeid', $ticketId);
    $statement->bindValue(':customfieldid', $fieldId);
    $statement->execute();
    if ($statement->fetch(\PDO::FETCH_ASSOC)) {
        $statement = $pdo->prepare("UPDATE `swcustomfieldvalues` SET `fieldvalue` = :value, `lastupdated` = unix_timestamp() " . $sql);
    } else {
        $statement = $pdo->prepare("INSERT INTO `swcustomfieldvalues` (`typeid`, `customfieldid`, `fieldvalue`, `uniquehash`) 
VALUES (:typeid, :customfieldid, :value, MD5(unix_timestamp()))");
    }
    $statement->bindValue(':typeid', $ticketId);
    $statement->bindValue(':customfieldid', $fieldId);
    $statement->bindValue(':value', $value);
    $status = true;
    if (!$statement->execute()) {
        $status = false;
        mylog($sql .' - failed update sql, field, value : '. $ticketId .' - '. $fieldId .' - '. $value, $status);
        throw new \Exception(sprintf('could not save value %s for field %s and ticket %s', $value, $fieldId, $ticketId));
    }
    mylog($sql .' - success update sql, field, value : '. $ticketId .' - '. $fieldId .' - '. $value, $status);
}

function writeAuditLog($data) {
    global $pdo;

    $sql = "INSERT INTO `swticketauditlogs` 
(ticketid, departmentid, dateline, creatortype, creatorid, creatorfullname, actiontype, actionmsg, valuetype, oldvalueid, newvalueid) 
VALUES (:ticketId,
 :departmentId, unix_timestamp(), 3, 0, 'automation script', 
18, :actionMsg, 
:valueType, unix_timestamp(), unix_timestamp())";
    $statement = $pdo->prepare($sql);

    $statement->bindValue(':ticketId', $data['ticketId']);
    $statement->bindValue(':valueType', $data['fieldId']);
    $statement->bindValue(':departmentId', $data['departmentId']);
    $statement->bindValue(':actionMsg', sprintf('Custom field %s set from %s to %s', $data['ticketId'], $data['oldValue'], $data['value']));
//    $statement->bindValue(':actionMsg', sprintf('Custom field %s to %s', $data['ticketId'], $data['value']));
    $status = true;
    if (!$statement->execute()) {
        var_dump($statement->errorInfo());
        $status = true;
        mylog($sql .' - failed log sql, field, value : '. print_r($data, true), $status);
        mylog(print_r($statement->errorInfo(), true), 0);
        throw new \Exception(sprintf('could not save log for ticket %s and field %s', $data['ticketId'], $data['fieldId']));
    }
    mylog($sql .' - success log sql, field, value : '. print_r($data, true), $status);
    $pdo->commit();

    return true;
}

function mylog($sql, $status) {
    $fileName = sprintf('%s.log', date('Y-m-d'));
    file_put_contents($fileName, sprintf('%s: query: %s, status %s',  date('Y-m-d H:i:s'), $sql, $status) . PHP_EOL, FILE_APPEND);
}
