<?php

ini_set('display_errors', '1');
error_reporting(E_ALL);

$dbHost = 'localhost';
$dbName = 'korisnickapodrska';
//$dbName = 'bgd_korisnickapodrska';
$dbUser = 'root';
$dbPass = 'OvoJ3NoviP@ss!';


// @TODO change test params

$data = array(
    'status' => false,
    'value' => false,
);

try {
    $dsn = "mysql:host={$dbHost};dbname={$dbName}";
    $pdo = new \PDO($dsn, $dbUser, $dbPass, [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']);

    maybeFireTrigger();
} catch (\Exception $e) {
    $data['value'] = $e->getMessage();
}

function maybeFireTrigger() {
    global $pdo;

    $conditions = "(ticketstatustitle = 'Primljen u MPO' OR ticketstatustitle = 'Pripremljen za slanje u servis') AND departmenttitle = 'Retail'";
    $statement = $pdo->prepare("SELECT MAX(ticketid) as id FROM `swtickets` WHERE {$conditions}");
    $statement->execute();
    $id = $statement->fetch(\PDO::FETCH_ASSOC);
    if (!$id) {
        // log
        return;
    }
    $storedId = getIdFromStorage();
    if ($storedId < (int) $id['id']) {
        $sql = "SELECT ticketid as id FROM `swtickets` WHERE {$conditions} AND ticketid > {$storedId}";
        $statement = $pdo->prepare($sql);
        $statement->execute();
        foreach ($statement->fetchAll(\PDO::FETCH_ASSOC) as $data) {
            var_dump($data);
            $ticketId = (int) $data['id'];
            setIdToStorage($ticketId);
            triggerNewTicketEvent($ticketId);
        }
    }
}

function triggerNewTicketEvent($id) {
    // trigger saving ES custom field
    echo sendRequest('http://wecareportal.djavolak.info/ticket/writeServiceDataToTicket/?id=' . $id);
}

function setIdToStorage($id) {
    $key = 'gf#lastTriggeredId';
    file_put_contents( 'lastTriggeredId', $id);
}

function getIdFromStorage() {
    $key = 'gf#lastTriggeredId';

    return (int) file_get_contents(__DIR__ . '/lastTriggeredId');
}

function sendRequest($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);
    curl_close($ch);

    return $output;
}