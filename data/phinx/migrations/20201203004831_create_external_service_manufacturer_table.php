<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateExternalServiceManufacturerTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `externalServiceManufacturer` (
  `externalServiceManufacturerId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `externalServiceId` int(10) unsigned NOT NULL,
  `manufacturerId` int(10) unsigned NOT NULL,
  `manufacturer` varchar(256) NOT NULL,
  `ignoredTypeIds` varchar(128) DEFAULT '',
  `acceptTypeIds` varchar(128) DEFAULT '',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`externalServiceManufacturerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
        $this->query("ALTER TABLE `externalServiceManufacturer` 
ADD UNIQUE INDEX `UNIQUE_RULE_ACCEPT` USING BTREE (`acceptTypeIds` ASC, `manufacturerId` ASC, `externalServiceId` ASC);");
        $this->query("ALTER TABLE `externalServiceManufacturer` 
ADD UNIQUE INDEX `UNIQUE_RULE_IGNORE` USING BTREE (`ignoredTypeIds` ASC, `manufacturerId` ASC, `externalServiceId` ASC);");
    }

    public function down()
    {
        $this->query("DROP TABLE `externalServiceManufacturer`");
    }
}
