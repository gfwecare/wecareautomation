<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateExternalServiceTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `externalService` (
  `externalServiceId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `address` varchar(255),
  `city` varchar(64),
  `zip` varchar(8),
  `phone` varchar(16),
  `kayakoId` INT (8),
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`externalServiceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `externalService`");
    }
}
