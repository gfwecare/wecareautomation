<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateStatusDatesDepartmentsTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `statusDatesDepartments` (
  `statusDatesDepartmentsId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `departmentId` int(32) NOT NULL,
  `department` varchar(256) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`statusDatesDepartmentsId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `statusDatesDepartments`");
    }
}
