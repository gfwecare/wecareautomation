<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTypeTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `ticketType` (
  `ticketTypeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ticketTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
        $this->query("ALTER TABLE `ticketType` ADD UNIQUE INDEX `type_name_UNIQUE` (`name` ASC);");
    }

    public function down()
    {
        $this->query("DROP TABLE `ticketType`");
    }
}
