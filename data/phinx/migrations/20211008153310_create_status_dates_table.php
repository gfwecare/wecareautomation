<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateStatusDatesTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `statusDates` (
  `statusDateId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `targetStatusId` int(32) NOT NULL,
  `targetStatus` varchar(256) NOT NULL,
  `customField` varchar(256) NOT NULL,
  `customFieldId` int(32) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`statusDateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
        $this->query("ALTER TABLE `statusDates` ADD UNIQUE INDEX `status_field_UNIQUE` (`targetStatusId`, `customFieldId` ASC);");
    }

    public function down()
    {
        $this->query("DROP TABLE `statusDates`");
    }
}
