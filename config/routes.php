<?php

/**
 * Define routes here.
 *
 * Routes follow this format:
 *
 * [METHOD, ROUTE, CALLABLE] or
 * [METHOD, ROUTE, [Class => method]]
 *
 * When controller is used without method (as string), it needs to have a magic __invoke method defined.
 *
 * Routes can use optional segments and regular expressions. See nikic/fastroute
 */

return [
//    ['GET', '/git/bb-hook', 'Bipsys\Controller\GitController'],
    // frontend
//    [['GET'], '/page/{slug}/', \WeCare\Page\Action\PageAction::class],
//    [['GET'], '/', \WeCare\Page\Action\IndexAction::class],


    // backend
//    [['GET', 'POST'], '/admin/page/{action}/[{pageId}/]', \WeCare\Page\Controller\PageController::class],
    [['GET', 'POST'], '/admin/user/{action}/[{userId}/]', \WeCare\User\Controller\UserController::class],
    [['GET'], '/', \WeCare\Admin\Action\IndexAction::class],

    [['GET', 'POST'], '/admin/manufacturer/{action}/[{manufacturerId}/]', \WeCare\ExternalService\Controller\ManufacturerController::class],
    [['GET', 'POST'], '/admin/external-service/{action}/[{externalServiceId}/]', \WeCare\ExternalService\Controller\ExternalServiceController::class],
    [['GET', 'POST'], '/admin/status-dates/{action}/[{id}/]', \WeCare\StatusDates\Controller\StatusDatesController::class],
    [['GET', 'POST'], '/admin/department/{action}/[{id}/]', \WeCare\StatusDates\Controller\StatusDatesDepartmentController::class],


//    [['GET'], '/ticket/shipmentsTest/', [\WeCare\Ticket\Controller\TicketController::class => 'shipmentsTest']],

    // cron / cli
    [['GET', 'POST'], '/ticket/{action}/', \WeCare\Ticket\Controller\TicketController::class],
//    [['GET'], '/ticket/getReportsFromCity/', [\WeCare\Ticket\Controller\TicketController::class => 'getReportsFromCity']],
//    [['GET'], '/ticket/writeServiceDataToTicket/', [\WeCare\Ticket\Controller\TicketController::class => 'writeServiceDataToTicket']],
    [['GET'], '/notification/testAkton/', [\WeCare\Notification\Controller\NotificationController::class => 'testAkton']]
];
