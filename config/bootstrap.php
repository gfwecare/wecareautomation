<?php

use WeCare\User\Repository\UserRepository;
use Laminas\Session\SessionManager;
use Laminas\Session\ManagerInterface;
use Laminas\Session\Config\SessionConfig;
use Monolog\ErrorHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Acl\Acl;
use WeCare\Ticket\Service\XlsParser;

class GF_Mail_Log_Handler extends \Monolog\Handler\AbstractHandler
{
    /**
     * @var \WeCare\Ticket\Service\Mailer
     */
    private $mail;

    public function __construct($level = \Monolog\Logger::ERROR, bool $bubble = true, $mail)
    {
        parent::__construct($level, $bubble);
        $this->mail = $mail;
    }

    public function handle(array $record): bool
    {
        if ($record['level'] === \Monolog\Logger::ERROR) {
            $this->mail->handleApplicationError($record);
        }

        return false;
    }
}


$containerBuilder = new \DI\ContainerBuilder;
/* @var \DI\Container $container */
$container = $containerBuilder
//    ->addDefinitions(require_once __DIR__ . '/config_web.php')
    ->build();

$container->set(\FastRoute\Dispatcher::class, function() {
    $routeList = require APP_PATH.'/config/routes.php';

    /** @var \FastRoute\Dispatcher $dispatcher */
    return FastRoute\simpleDispatcher(
        function (\FastRoute\RouteCollector $r) use ($routeList) {
            foreach ($routeList as $routeDef) {
                $r->addRoute($routeDef[0], $routeDef[1], $routeDef[2]);
            }
        }
    );
});

$container->set(Acl::class, function() use ($container) {
    return new Acl(
        $container->get(ManagerInterface::class),
        $container->get(Config::class),
        require APP_PATH . '/config/acl.php');
});

$container->set(Skeletor\Middleware\MiddlewareInterface::class, function() use ($container) {
    return new \WeCare\Admin\Middleware\AuthMiddleware(
        $container->get(ManagerInterface::class),
        $container->get(Config::class),
        $container->get(Flash::class),
        $container->get(UserRepository::class),
        $container->get(Acl::class)
    );
});

$container->set(Config::class, function() {
    $params = include(APP_PATH . "/config/config.php");
    $config = new \Laminas\Config\Config($params);
    $config = $config->merge(new \Laminas\Config\Config(include(APP_PATH . "/config/config-local.php")));

    return $config;
});

$container->set(ManagerInterface::class, function() {
    $sessionConfig = new SessionConfig();
    $sessionConfig->setOptions([
        'remember_me_seconds' => 2592000, //2592000, // 30 * 24 * 60 * 60 = 30 days
        'use_cookies'         => true,
        'cookie_httponly'     => true,
        'name'                => 'wecarerutiranje',
        'cookie_lifetime'     => 60 * 60 * 2,
    ]);

    $session = new SessionManager($sessionConfig);
    $session->start();

    return $session;
});

$container->set(\Monolog\Logger::class, function() {
    $logger = new \Monolog\Logger('wecare.cron.log');
    $date = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));
    $logFile = APP_PATH . '/data/logs/' . $date->format('y-m-d') . '-cron.log';
    // create dir or file if needed
    if (!file_exists($logFile)) {
        touch($logFile);
    }
    $logger->pushHandler(
        new StreamHandler($logFile, \Monolog\Logger::INFO)
    );

    return $logger;
});

$container->set(Logger::class, function() use ($container) {
    $logger = new \Monolog\Logger('wecare');
    $date = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));
    $logDir = APP_PATH . '/data/logs/' . $date->format('Y') . '-' . $date->format('m');
    $logFile = $logDir . '/' . gethostname() . '-' . $date->format('d') . '.log';
    $debugLog = APP_PATH . '/data/logs/debug.log';
    // create dir or file if needed
    if (!is_dir($logDir)) {
        mkdir($logDir);
    }
    if (!file_exists($logFile)) {
        touch($logFile);
    }

    $logger->pushHandler(
        new StreamHandler($logFile, \Monolog\Logger::ERROR)
    );
    $logger->pushHandler(
        new GF_Mail_Log_Handler(\Monolog\Logger::ERROR, true, $container->get(\WeCare\Ticket\Service\Mailer::class))
    );
    $logger->pushHandler(
        new StreamHandler($debugLog,\Monolog\Logger::DEBUG)
    );
    if (strtolower(getenv('APPLICATION_ENV')) !== 'production') {
        $logger->pushHandler(new BrowserConsoleHandler());
    }
    ErrorHandler::register($logger);

    return $logger;
});

$container->set(\PDO::class, function() use ($container) {
    $config = $container->get(Config::class);
    $dsn = "mysql:host={$config->db->host};dbname={$config->db->name}";
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );
    return new \PDO($dsn, $config->db->user, $config->db->pass, $options);
});

$container->set(\Redis::class, function() use ($container) {
    $config = $container->get(Config::class);
    $redis = new \Redis();
    $redis->connect($config->redis->host);

    return $redis;
});

$container->set(\DateTime::class, function() use ($container) {
    $dt = new \DateTime('now', new \DateTimeZone($container->get(Config::class)->offsetGet('timezone')));
    return $dt;
});

$container->set(\Twig\Environment::class, function() use ($container) {
    $loader = new \Twig\Loader\FilesystemLoader(
        APP_PATH . '/themes/sbadmin/'
    );
    $te = new \Twig\Environment($loader, array(
        'debug' => true,
//        'cache' => '/path/to/compilation_cache',
    ));
    $te->addExtension(new Twig\Extension\DebugExtension());
    $te->addExtension(new Twig\Extensions\I18nExtension());

    return $te;
});

$container->set(Flash::class, function () use ($container) {
    //session needs to be started for flash
    $container->get(ManagerInterface::class);

    return new Flash();
});

$container->set(XlsParser::class, function () use ($container) {
    return new XlsParser(
        new \PhpOffice\PhpSpreadsheet\Reader\Xlsx(),
        $container->get(Config::class)->fileStorage->mpoSource,
        $container->get(Config::class)->fileStorage->externalSource
    );
});

$container->set(kyConfig::class, function () use ($container) {
    return new \kyConfig(
        $container->get(Config::class)->services->kayako->endpoint,
        $container->get(Config::class)->services->kayako->apiKey,
        $container->get(Config::class)->services->kayako->secret
    );
});

$container->set(\GuzzleHttp\Client::class, function() use ($container) {
    return new \GuzzleHttp\Client([
        'base_uri' => $container->get(Config::class)->services->akton->endpoint,
        'verify' => false
    ]);
});

$container->set(\WeCare\CityExpress\Service\Api::class, function() use ($container) {
    return new \WeCare\CityExpress\Service\Api(
        new \GuzzleHttp\Client([
            'base_uri' => $container->get(Config::class)->services->city->endpoint
        ]), $container->get(Logger::class)
    );
});

$container->set(\Laminas\Mail\Transport\TransportInterface::class, function() use ($container) {
    $transport = new \Laminas\Mail\Transport\Smtp();
    $options = new \Laminas\Mail\Transport\SmtpOptions($container->get(Config::class)->mailServer->toArray());
    $transport->setOptions($options);

    return $transport;
});


return $container;

