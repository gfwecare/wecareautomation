<?php

date_default_timezone_set('Europe/Belgrade');

return array(
    'baseUrl' => 'http://wecareportal.djavolak.info',
    'redirectUri' => '/user/index',
    'timezone' => 'Europe/Belgrade',
    'cliMap' => [
        'Ticket' => \WeCare\Ticket\Controller\TicketController::class,
        'Notification' => \WeCare\Notification\Controller\NotificationController::class,
        'StatusDates' => \WeCare\StatusDates\Controller\StatusDatesController::class,
    ]
);