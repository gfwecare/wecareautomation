<?php

$guest = [
    '/',
    '/page/*',
    '/admin/user/loginForm/',
    '/admin/user/login/',
    '/admin/user/resetPassword/',
    '/git/bbHook',
    '/cron/*',
    '/ticket/getReportsFromCity/',
    '/admin/external-service/fixServices/',
    '/ticket/writeServiceDataToTicket/',
    '/ticket/writeStatusUpdateToCustomField/',
];

$level2 = [
    '/admin/user/form/*',
    '/admin/user/update/*',
    '/admin/user/logout/',
    '/admin/manufacturer/*',
    '/admin/external-service/*',
    '/admin/status-dates/*',
    '/admin/department/*',
];

$level1 = [
    '/admin/user/*',
    '/admin/page/*',
];

//can also see everything level2 can see
$level1 = array_merge($level2, $level1);

return [
    0 => $guest,
    1 => $level1,
    2 => $level2
];